import requests
import json as j
import time


def request_token(env):
    url = 'https://login.microsoftonline.com/06b476ce-d8bc-4355-a477-0392dd2dc025/oauth2/v2.0/token'
    hder = {'Content-Type': "application/x-www-form-urlencoded"}
    body = None
    if env == 'uat':
        body = {
            "client_id": "0be775d0-2094-4b4e-886d-11241c01000b",
            "client_secret": "3TCk}fW#1OP0^uy",
            "scope": "d6e405e5-cf8a-4c11-833f-0903623f8ef0/.default",
            "grant_type": "client_credentials"
        }
    if env == 'tst':
        body = {
            "client_id": "be385a58-2671-4066-a772-15666329f83b",
            "client_secret": "3TCk}fW#1OP0^uy",
            "scope": "00db6d8a-63c0-4b55-b453-32016f1d5af2/.default",
            "grant_type": "client_credentials"
        }

    api_response = requests.post(url, body, hder)
    resp_dict = j.loads(api_response.content)
    return resp_dict["access_token"]


def request_token_inbound_message(env):
    url = 'https://login.microsoftonline.com/06b476ce-d8bc-4355-a477-0392dd2dc025/oauth2/v2.0/token'
    hder = {'Content-Type': "application/x-www-form-urlencoded"}
    body = None
    if env == 'uat':
        body = {
            "client_id": "0be775d0-2094-4b4e-886d-11241c01000b",
            "client_secret": "3TCk}fW#1OP0^uy",
            "scope": "f8b78373-0620-4602-84d2-29bb5ef3d482/.default",
            "grant_type": "client_credentials"
        }

    if env == 'tst':
        body = {
            "client_id": "be385a58-2671-4066-a772-15666329f83b",
            "client_secret": "3TCk}fW#1OP0^uy",
            "scope": "3fa434cc-7863-45eb-8e3a-6a2dd41335a7/.default",
            "grant_type": "client_credentials"
        }

    api_response = requests.post(url, body, hder)
    resp_dict = j.loads(api_response.content)
    return resp_dict["access_token"]

def send_sc_message(body, env, tenant):
    username = None
    password = None
    response = None
    message = None
    if tenant == 1 and env == 'tst':
        username = 'sound.caller'
        password = 'b2v#ikO45!123456@#KL'
        #username = 'Mercury.Caller'
        #password = 'kH7s!#hd!uL'
    elif tenant == 1 and env == 'uat':
        username = 'sound.caller'
        password = 'b2v#ikO45!123456@#KL'
    elif tenant == 2 and env == 'tst':
        username = 'Mercury.Caller'
        password = 'kH7s!#hd!u'
    elif tenant == 2 and env == 'uat':
        username = 'Mercury.Caller'
        password = 'kH7s!#hd!v1'
    auth_str = '%s:%s' % (username, password)
    header = {'Authorization': 'Basic %s' % auth_str}

    if env == 'tst':
        env = 'test'
    try:
        url = 'https://sound' + env + '.appiancloud.com/suite/webapi/post-event'
        print(url)
        response = requests.post(url, body, header, auth=(username, password))
    except message as e:
        print(e)
    return response


def arbor_inbound_episode(body, env, header):
    response = None
    message = None

    try:
        url = 'https://php-' + env + '-functionapp-inboundmessage.azurewebsites.net/api/post-arbor-episode'
        print(url)
        response = requests.post(url, body, headers=header)
    except message as e:
        print(e)
    return response


def send_cfim_message(body, env):
    header = {}
    response = None
    message = None
    url = None
    if env == 'tst':
        url = 'https://php-tst-functionapp-inboundqueue.azurewebsites.net/api/post-event?code=U15kBXfAr693P5YVwlt2rCCPflba2blXY6Z1oSHgc5LbuaKTSBzI/g=='
    elif env == 'uat':
        url = 'https://php-uat-functionapp-inboundqueue.azurewebsites.net/api/post-event?code=ByJSQWaSS1jLYwqpkvhUawlH1QZv39Hl9B1Im94DMwFi3LEdWzFKtQ=='

    try:
        print(url)
        response = requests.post(url, body, header)

    except message as e:
        print(e)

    return response


def send_sc_message_second_tenant(body, env):
    header = {}
    response = None
    message = None

    try:
        url = 'https://php-' + env + '-functionapp-inboundqueue.azurewebsites.net/api/post-event?code=xKsAEKIsfxZD7yAa7/JLupXv/4pJ2Q6BUpttYpkX8Vqsm5XBKhYp2A=='
        print(url)
        response = requests.post(url, body, header)

    except message as e:
        print(e)

    return response


def arbor_readmission_risk(body, env):
    header = {"Appian-API-Key": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJlZGNlMTMzYS02ZjFmLTRmYmEtYjA0ZC05NGIyMDRmMmQ2NzIifQ.uLzNLdiX8w1_i3mwormlLRQCs18Sy_oSXILowMmviY8"}
    if env == 'uat':
        header = {"Appian-API-Key": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2ZWQwMTViOC0xYTdiLTQ0MWEtYjFkZS01MzQwMGViZTAwOWMifQ.Yj0GsgtQpKIjLGYv1AKsJsexutMnMUAit9cSGmTY524"}
    response = None
    message = None
    if env == 'tst':
        env = 'test'
    try:
        url = 'https://sound' + env + '.appiancloud.com/suite/webapi/post-arbor'
        print(url)
        response = requests.post(url, body, headers=header)
    except message as e:
        print(e)
    return response


def update_episode_status(tenant, env, external_visit_id, status, header):
    url = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/' + tenant + '/patientvisits/' + str(external_visit_id) + '/episodeStatus/update/' + str(status) + '?u=ceurom'

    print(url)
    payload = {}
    body = j.dumps(payload)
    api_response = requests.put(url, body, headers=header)
    #resp_dict = j.loads(api_response.content)
    return api_response#, resp_dict


def get_readmission_risk_survey_list(env, facility_id, header):
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/acutesites/' + str(facility_id) +'/interventions/readmissionrisk?u=jebishop&maxAge=8'
    api_response = requests.get(uri, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def get_readmission_risk_survey(env, external_visit_id, header):
    uri = 'https://php-' + env +'-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/patientvisits/' + str(external_visit_id) + '/interventions/readmissionrisk?u=jebishop'
    api_response = requests.get(uri, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def get_readmission_risk_flag(env, external_visit_id, header):
    uri = 'https://php-' + env +'-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/patientvisits/' + str(external_visit_id) + '/interventions/isatreadmissionrisk?u=jebishop'
    api_response = requests.get(uri, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def save_readmission_risk_survey(env, external_visit_id, header, ans0, ans1, ans2, ans3, ans4, ans5):
    payload = {
        "schedulePCPFollowUp": ans0,
        "communicationToPCP": ans1,
        "dischargeSummaryComplete": ans2,
        "medReconciliation": ans3,
        "medsToBed": ans4,
        "teachBackDischargeInstructions": ans5
    }
    body = j.dumps(payload)
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/patientvisits/' + str(external_visit_id) + '/interventions/readmissionrisk?u=jebishop'
    api_response = requests.put(uri, body, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def get_dismissal_survey(env, external_visit_id, header):
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/patientvisits/' + str(external_visit_id) + '/interventions/dismissal?u=sbering'
    api_response = requests.get(uri, headers=header)
    return api_response


def save_dismissal_survey(env, external_visit_id, header):
    payload = {"adime": ["attending", "diagnosis", "inpatient", "membership", "exclusion"], "notes": "Testing note"}
    body = j.dumps(payload)
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/patientvisits/' + str(external_visit_id) + '/interventions/dismissal?u=sbering'
    api_response = requests.put(uri, body,  headers=header)
    return api_response


def get_pcp_appointment_list(env, facility_id, header):
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/acutesites/' + str(facility_id) +'/interventions/pcpappointment?u=sbering&maxAge=6'
    api_response = requests.get(uri, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def get_pcp_survey(env, external_visit_id, header):
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/patientvisits/' + str(external_visit_id) + '/interventions/pcpappointment?u=sbering'
    api_response = requests.get(uri, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def save_pcp_survey(env, external_visit_id, header, appt_status=2, appt_date='2020-08-25', note='test', pcp='Test, PCP'):
    payload = {"pcpName": pcp,
               "appointmentStatus": appt_status,
               "appointmentDate": appt_date,
               "appointmentNotes": note
               }
    body = j.dumps(payload)
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/patientvisits/' + str(external_visit_id) + '/interventions/pcpappointment?u=sbering'
    api_response = requests.put(uri, body, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def get_hcc_list(env, facility_id, token):
    header = {'Content-Type': "application/json", 'Authorization': 'bearer ' + token}
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/acutesites/' + str(facility_id) + '/interventions/hcc?u=ceurom&maxAge=0'
    api_response = requests.get(uri, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def get_hcc_visit(env, external_visit_id, token):
    header = {'Content-Type': "application/json", 'Authorization': 'bearer ' + token}
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/patientvisits/' + str(external_visit_id) + '/interventions/hcc?u=ceurom'
    api_response = requests.get(uri, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def save_hcc_visit(env, external_visit_id, token, appt_status=2, appt_date='2020-08-25' ):
    header = {'Content-Type': "application/json", 'Authorization': 'bearer ' + token}
    payload = {
                "ResponseStatus": "2",
                "MemberId": "987654",
                "Hccs": [
                    {
                        "hccId": "189",
                        "statusId": "-1",
                        "previousDiagnosisText": " M35.4 TEST TEXT TEST TEXT TEST TEXT"
                    },
                    {
                        "hccId": "84",
                        "statusId": "1",
                        "previousDiagnosisText": "M35.5 TEST TEXT TEST TEXT TEST TEXT"
                    },
                    {
                        "hccId": "137",
                        "statusId": "2",
                        "previousDiagnosisText": "M35.6 TEST TEXT TEST TEXT TEST TEXT"
                    }
                ]
            }
    body = j.dumps(payload)
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/patientvisits/' + str(external_visit_id) + '/interventions/hcc?u=ceurom'
    api_response = requests.put(uri, body, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def get_hcc_attestation(env, external_visit_id, token):
    header = {'Content-Type': "application/json", 'Authorization': 'bearer ' + token}
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/patientvisits/' + str(external_visit_id) + '/interventions/hccAttestation?u=ceurom'
    api_response = requests.get(uri, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def get_visit_discharge_planning(env, external_visit_id, header):
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/patientvisits/' + str(external_visit_id) + '/interventions/dischargePlanning?u=ceurom'
    api_response = requests.get(uri, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def save_visit_discharge_planning(env, external_visit_id, header):
    payload = {
        "actualNSOC": "30",
        "caseManagementDesiredNSOC": "41",
        "goalsOfCareDiscussedOrAdvCarePlanningCompleted": "false",
        "hospiceConsultOrdered": "false",
        "isFocusPatient": "true",
        "noChangeToPlanOfCareReason": "2",
        "palliativeCareConsultOrdered": "false",
        "patientDesiredNSOC": "5",
        "providerRecommendedNSOC": "30",
        "surpriseQuestion": "false"
    }
    body = j.dumps(payload)
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/patientvisits/' + str(external_visit_id) + '/interventions/dischargePlanning?u=ceurom'
    api_response = requests.put(uri, body, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def get_visit_focus_patient(env, external_visit_id, header):
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/patientvisits/' + str(external_visit_id) + '/interventions/focusPatient?u=ceurom'
    api_response = requests.get(uri, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def save_visit_focus_patient(env, external_visit_id, header):
    payload = {
        "homeHealthAssessmentCompleted": "false",
        "surpriseQuestion": "false",
        "patientBaselineFunctionId": "4",
        "hasHelpAvailableHome": "true",
        "cpnRoundingComplete": "false",
        "goalsOfCareDiscussedOrAdvCarePlanningCompleted": "false",
        "palliativeCareConsultOrdered": "false",
        "hospiceConsultOrdered": "false",
        "noChangeToPlanOfCareReason": "9",
        "noChangeToPlanOfCareReasonNote": "test",
        "homeHealthAssessmentNotCompletedReason": "9",
        "homeHealthAssessmentNotCompletedReasonNote": "test"
    }
    body = j.dumps(payload)
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/patientvisits/' + str(external_visit_id) + '/interventions/focusPatient?u=ceurom'
    api_response = requests.put(uri, body, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def get_avoidable_day(env, external_visit_id, header):
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/patientVisits/' + str(external_visit_id) + '/interventions/avoidableDay?u=ceurom'

    api_response = requests.get(uri, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def save_avoidable_day(env, external_visit_id, date1, date2, header):
    payload = [
            {
                "dateOfService": date1,
                "isAvoidable": True,
                "avoidableDayParentTypeId": 1,
                "avoidableDayParentTypeName": "Attributable to Patient/Family",
                "avoidableDayTypeId": 11,
                "avoidableDayTypeName": "Patient/family refusing test and/or procedure"
            },
            {
                "dateOfService": date2,
                "isAvoidable": False,
                "avoidableDayParentTypeId": None,
                "avoidableDayTypeId": None
            }
        ]
    body = j.dumps(payload)
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/patientVisits/' + str(external_visit_id) + '/interventions/avoidableDay?u=ceurom'
    api_response = requests.put(uri, body, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def get_avoidable_day_summary(env, external_site_id, date1, date2, header):
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/acuteSites/' + str(external_site_id) + '/avoidableDays/summary?u=ceurom&fromDate=' + date1 + '&toDate=' + date2

    api_response = requests.get(uri, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def get_avoidable_day_roll_up(env, external_site_id, date1, date2,  header):
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/acuteSites/' + str(external_site_id) + '/avoidableDays/rollUp?u=ceurom&fromDate=' + date1 + '&toDate=' + date2

    api_response = requests.get(uri, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def get_episodes(env, facility_id, header):
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/acuteSites/' + str(facility_id) + '/episodes?u=jebishop@soundphysicians.com'

    api_response = requests.get(uri, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict


def get_episodes_sync(env, facility_id, header):
    uri = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4CAB-B7BD-8D257CF70DFF/acuteSites/' + str(facility_id) + '/episodes/sync?u=jebishop@soundphysicians.com'

    api_response = requests.get(uri, headers=header)
    resp_dict = j.loads(api_response.content)
    return api_response, resp_dict
