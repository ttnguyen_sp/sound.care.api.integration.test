from sc_util import api_helper
from Helpers import utilities
from config import env


def get_sc_token():
    header = {'Content-Type': "application/x-www-form-urlencoded"}
    request_body = {
        "grant_type": "password",
        "username": "dtrainer",
        "password": "fake",
        "client_id": "brio",
        "client_secret": "notreallysecret",
        "scope": "api offline_access"}
    uri = "https://auth" + env + ".soundphysicians.com/connect/token"

    response, message = api_helper.api_post(uri, request_body, header)
    print(response.text)
    access_token = response.json().get('access_token')
    return access_token


def get_uhc_token():
    header = {'Content-Type': "application/x-www-form-urlencoded"}
    request_body = {
        "username": "",
        "password": "",
        "client_secret": "e4a67ad987cc4740bacf4d218386ce67",
        "grant_type": "client_credentials",
        "client_id": "l7xx2e626a18108a47899b15cd0f6dfa1dfe",
    }
    uri = "https://api8.optum.com:8445/auth/oauth/v2/token"

    response, message = api_helper.api_post(uri, request_body, header)
    print(response.text)
    access_token = response.json().get('access_token')
    return access_token


tcma_client_id = 'soundconnect-svc'
tcma_client_secret = 'ED3DCA02-9D6C-4EAB-82AD-705F6F304F85'
tcma_payload = {'grant_type': 'client_credentials', 'scope': 'api'}
tcma_headers = {"Content-Type": "application/x-www-form-urlencoded"}


def get_soundcare_token():
    url = 'https://login.microsoftonline.com/06b476ce-d8bc-4355-a477-0392dd2dc025/oauth2/token'
    hder = {'Content-Type': "application/x-www-form-urlencoded"}
    body = {
        "client_secret": "yU.bZ7qR^X>:XUMk",
        "grant_type": "client_credentials",
        "client_id": "be385a58-2671-4066-a772-15666329f83b",
        "resource": "00db6d8a-63c0-4b55-b453-32016f1d5af2"
    }
    response, json_response = utilities.execute_api_post(url, body, hder)
    # return json_response["access_token"].encode("utf-8")
    return json_response["access_token"]
