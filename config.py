import warnings
# from requests.packages.urllib3.exceptions import InsecureRequestWarning
from urllib3.exceptions import InsecureRequestWarning

warnings.simplefilter('ignore', InsecureRequestWarning)

env = 'tst'
#env = 'uat'

tenant1 = 1
tenant1_guid = '88592232-0585-4CAB-B7BD-8D257CF70DFF'
tenant2 = 2
tenant2_guid = '756DFB9C-7CD7-430C-824C-1FCDE6B72207'

report_dir = "C:\\test_result\\"

base_uri = 'https://mobile' + env + '.soundphysicians.com'


# -----------------------------------------------------------------------------------------------------------------
# Target process resources
# -----------------------------------------------------------------------------------------------------------------
run_target_process = False

tp_tag = ''
suite_name = ''
test_plan_run_id = ''

child_test_plan_run_id = 0
errors = ""
passed_test_cases = 0
failed_test_cases = 0
total_test_cases = 0
test_case_name = None
test_cases = None


# -----------------------------------------------------------------------------------------------------------------
# TCMA Api Info
# -----------------------------------------------------------------------------------------------------------------
tcma_client_id = 'soundconnect-svc'
tcma_client_secret = 'ED3DCA02-9D6C-4EAB-82AD-705F6F304F85'
tcma_payload = {'grant_type': 'client_credentials', 'scope': 'api'}
tcma_headers = {"Content-Type": "application/x-www-form-urlencoded"}


# -----------------------------------------------------------------------------------------------------------------
# Covered Lives Api Info
# -----------------------------------------------------------------------------------------------------------------
coveredlives_base_url = 'https://php-tst-appservice-coveredlives.azurewebsites.net/api/'


# -----------------------------------------------------------------------------------------------------------------
# Sound Connect Api Info
# -----------------------------------------------------------------------------------------------------------------
sc_client_id = 'soundconnect-proxy'
sc_client_secret = 'not-a-secret'
sc_payload = {'grant_type': 'password', 'scope': 'api', 'username': 'dtrainer', 'password': '*hoodUnit'}
sc_headers = {"Content-Type": "application/x-www-form-urlencoded"}


