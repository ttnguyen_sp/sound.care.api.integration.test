def find_between(st, first, last):
    try:
        start = st.index(first) + len(first)
        end = st.index(last, start)
        return st[start:end]
    except ValueError:
        return ""


def is_sub_string_existed(string, sub_string):
    if sub_string not in string:
        return False
    elif sub_string in string:
        return True
