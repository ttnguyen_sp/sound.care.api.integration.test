import time
from config import env as env
#from sc_util import database_helper as dh
from Utilities import database_util as dh
from Helpers import episode_management_helper as e
from datetime import date, timedelta


# --------------------------------------------------------------------------------------------------------------- #
# ---------------------------------------------- Insert Queries ------------------------------------------------- #
# --------------------------------------------------------------------------------------------------------------- #
def create_patient_clinician_record(visit_id):
    sql1 = 'INSERT INTO Patient.Clinician (VisitId, Role, FirstName, LastName, NPI) ' \
           'VALUES (%s, \'Attending\', \'William\', \'Ciesla Jr\', \'1265526578\')' % visit_id
    sql2 = 'INSERT INTO Patient.Clinician (VisitId, Role, FirstName, LastName, NPI) ' \
           'VALUES (%s, \'Admitting\', \'Joseph\', \'Tan\', \'1922070515\')' % visit_id
    sql3 = 'INSERT INTO Patient.Clinician (VisitId, Role, FirstName, LastName, NPI) ' \
           'VALUES (%s, \'Discharging\', \'William\', \'Ciesla Jr\', \'1265526578\')' % visit_id
    dh.execute_prince_query_command(env, sql1, None)
    dh.execute_prince_query_command(env, sql2, None)
    dh.execute_prince_query_command(env, sql3, None)


def create_patient_diagnosis_record(visit_id):
    sql1 = 'INSERT INTO Patient.Diagnosis (VisitId, Code, CodeType, CodingMethod, SequenceNumber) ' \
           'VALUES (%s, \'R55\', \'F\', \'ICD10\',	1)' % visit_id
    sql2 = 'INSERT INTO Patient.Diagnosis (VisitId, Code, CodeType, CodingMethod, SequenceNumber) ' \
           'VALUES (%s, \'I10\', \'F\', \'ICD10\',	2)' % visit_id
    sql3 = 'INSERT INTO Patient.Diagnosis (VisitId, Code, CodeType, CodingMethod, SequenceNumber) ' \
           'VALUES (%s, \'E11.9\', \'F\', \'ICD10\', 3)' % visit_id
    sql4 = 'INSERT INTO Patient.Diagnosis (VisitId, Code, CodeType, CodingMethod, SequenceNumber) ' \
           'VALUES (%s, \'F10.10\', \'F\', \'ICD10\', 4)' % visit_id
    dh.execute_prince_query_command(env, sql1, None)
    dh.execute_prince_query_command(env, sql2, None)
    dh.execute_prince_query_command(env, sql3, None)
    dh.execute_prince_query_command(env, sql4, None)


def create_patient_procedure_record(visit_id):
    sql1 = 'INSERT INTO Patient.[Procedure] (VisitId, ProcedureDate, ProcedureCode) ' \
           'VALUES (%s, \'2020-06-08\', \'99220\')' % visit_id
    sql2 = 'INSERT INTO Patient.[Procedure] (VisitId, ProcedureDate, ProcedureCode) ' \
           'VALUES (%s, \'2020-06-08\', \'G8427\')' % visit_id
    sql3 = 'INSERT INTO Patient.[Procedure] (VisitId, ProcedureDate, ProcedureCode) ' \
           'VALUES (%s, \'2020-06-09\', \'99217\')' % visit_id
    dh.execute_prince_query_command(env, sql1, None)
    dh.execute_prince_query_command(env, sql2, None)
    dh.execute_prince_query_command(env, sql3, None)


def create_patient_insurance_record(visit_id):
    sql1 = 'INSERT INTO Patient.Insurance (VisitId,	SequenceNumber,	PlanId,	CompanyId, CompanyName,	CompanyAddress,	' \
           'CompanyCity, CompanyState, CompanyZip, CompanyPhone, CompanyGroupNum, CompanyGrpName, PolicyNumber,	' \
           'PlanEffectiveDate, PlanExpirationDate, InsuredLastName, InsuredFirstName, InsuredMiddleName, ' \
           'InsuredsRelToPatient, InsuredsDob, InsuredsAddress, InsuredsCity, InsuredsState, InsuredsZip) ' \
           'VALUES (%s,	1, \'318910\', \'3189\', \'Sound Insurance\', \'PO BOX 1234\', \'TACOMA\', \'AZ\', ' \
           '\'98400\', \'253-565-5576\', \'\', \'\', \'352721602013\',	NULL, NULL, \'\', \'\', \'\', \'Se\', ' \
           '\'1970-02-02\', \'\', \'\', \'\', \'\')' % visit_id
    sql2 = 'INSERT INTO Patient.Insurance (VisitId,	SequenceNumber,	PlanId,	CompanyId, CompanyName,	CompanyAddress,	' \
           'CompanyCity, CompanyState, CompanyZip, CompanyPhone, CompanyGroupNum, CompanyGrpName, PolicyNumber,	' \
           'PlanEffectiveDate, PlanExpirationDate, InsuredLastName, InsuredFirstName, InsuredMiddleName, ' \
           'InsuredsRelToPatient, InsuredsDob, InsuredsAddress, InsuredsCity, InsuredsState, InsuredsZip) ' \
           'VALUES (%s,	2, \'318911\', \'3289\', \'Sound Insurance\', \'PO BOX 1235\', \'TACOMA\', \'AZ\', ' \
           '\'98400\', \'253-565-5576\', \'\', \'\', \'352721602013\',	NULL, NULL, \'\', \'\', \'\', \'Se\', ' \
           '\'1970-02-02\', \'\', \'\', \'\', \'\')' % visit_id
    sql3 = 'INSERT INTO Patient.Insurance (VisitId,	SequenceNumber,	PlanId,	CompanyId, CompanyName,	CompanyAddress,	' \
           'CompanyCity, CompanyState, CompanyZip, CompanyPhone, CompanyGroupNum, CompanyGrpName, PolicyNumber,	' \
           'PlanEffectiveDate, PlanExpirationDate, InsuredLastName, InsuredFirstName, InsuredMiddleName, ' \
           'InsuredsRelToPatient, InsuredsDob, InsuredsAddress, InsuredsCity, InsuredsState, InsuredsZip) ' \
           'VALUES (%s,	3, \'318912\', \'3389\', \'Sound Insurance\', \'PO BOX 1236\', \'TACOMA\', \'AZ\', ' \
           '\'98400\', \'253-565-5576\', \'\', \'\', \'352721602013\',	NULL, NULL, \'\', \'\', \'\', \'Se\', ' \
           '\'1970-02-02\', \'\', \'\', \'\', \'\')' % visit_id
    dh.execute_prince_query_command(env, sql1, None)
    dh.execute_prince_query_command(env, sql2, None)
    dh.execute_prince_query_command(env, sql3, None)


def create_patient_discharge_plan_record(visit_id):
    sql = 'INSERT INTO Patient.DischargePlan (VisitId, DischargeDispositionActual, DischargeDispositionProvider, ' \
          'DischargeDispositionCareTeam, DischargeDispositionPatient, AnticipatedDischargeDate, DischargeLocationId, ' \
          'ReadmissionRiskScore, Note, SurpriseQuestion, DateCreated, CreatedBy) ' \
          'VALUES (%s, 1, NULL, NULL, NULL, NULL, 50, \'Some Notes\', NULL, GETDATE(), \'sound.caller\')' % visit_id
    dh.execute_prince_query_command(env, sql, None)


def create_patient_avoidable_day_record(visit_id, service_date1, service_date2):
    sql1 = 'INSERT INTO Patient.AvoidableDay (VisitId, DateOfService, AvoidableDayTypeId, Notes, DateCreated, CreatedBy)' \
           'VALUES (%s, \'%s\', 14, \'test123\', GETDATE(), \'epimgmt-api-user-tst\')' % (visit_id, service_date1)
    sql2 = 'INSERT INTO Patient.AvoidableDay (VisitId, DateOfService, AvoidableDayTypeId, Notes, DateCreated, CreatedBy)' \
           'VALUES (%s, \'%s\', 17, \'test456\', GETDATE(), \'epimgmt-api-user-tst\')' % (visit_id, service_date2)
    dh.execute_prince_query_command(env, sql1, None)
    dh.execute_prince_query_command(env, sql2, None)


def insert_patient_visit(acute_site_id, ext_visit_id, first_name, last_name, dob, admit_date):
    sql = 'INSERT INTO Patient.PatientVisit (AcuteSiteId, ExternalVisitId, FacilityAccountNumber, ' \
          'MedicalRecordNumber, FirstName, LastName, DateOfBirth, AdmitDate, DischargeDate, IsOneTimeCharge, ' \
          'PlaceOfServiceTypeId, RoomNumber, DateCreated, CreatedBy, ClinicalSummary, FollowUpNotes, ' \
          'DateDeleted, Sex, PatientAggregateId) ' \
          'VALUES (%s, \'%s\', \'\', \'\', \'%s\', \'%s\', \'%s\', \'%s\', NULL, 0, 21, \'123\', GETDATE(), ' \
          '\'admtnhuyen\', \'Clinical Summary Note\', \'Followup Note\', NULL, \'M\', \'AKJHBNSY0123D\')' % \
          (acute_site_id, ext_visit_id, first_name, last_name, dob, admit_date)
    dh.execute_prince_query_command(env, sql, None)
    p_visit_id = get_patient_visit_id(ext_visit_id)
    return p_visit_id


def insert_patient_discharge_plan(p_visit_id):
    sql = 'INSERT INTO Patient.DischargePlan (VisitId, DischargeDispositionActual, DischargeDispositionProvider, ' \
          'DischargeDispositionCareTeam, DischargeDispositionPatient, DischargeLocationId, ReadmissionRiskScore, ' \
          'AnticipatedDischargeDate, Note, SurpriseQuestion, DateCreated, CreatedBy) ' \
          'VALUES (%s, NULL, NULL, NULL, NULL, NULL, NULL, NULL, \'Some Note\', 0, GETDATE(), \'admtnhuyen\')' % p_visit_id
    dh.execute_prince_query_command(env, sql, None)


def insert_episode(p_id, e_id, ext_visit_id, EpisodePurposeId=3):
    sql = None
    if e_id is None:
        sql = 'INSERT INTO Episode.Episode (ProgramPayerId, EligibilityEngineId, EpisodePurposeId, ReferenceId, ' \
              'UniquePatientId, EpisodeStatusTypeId,  DateCreated, CreatedBy) ' \
              'VALUES (%s, NULL, %s, NEWID(), \'%s\', 2, GETDATE(), \'sound.caller\')' % \
              (p_id, EpisodePurposeId, ext_visit_id)
    else:
        sql = 'INSERT INTO Episode.Episode (ProgramPayerId, EligibilityEngineId, EpisodePurposeId, ReferenceId, ' \
              'UniquePatientId, EpisodeStatusTypeId,  DateCreated, CreatedBy) ' \
              'VALUES (%s, %s, %s, NEWID(), \'%s\', 2, GETDATE(), \'sound.caller\')' % \
              (p_id, e_id, EpisodePurposeId, ext_visit_id)

    dh.execute_prince_query_command(env, sql, None)
    episode_id = get_latest_episode_id()
    return episode_id


def insert_episode_patient_visit(p_visit_id, episode_id):
    sql = 'INSERT INTO Episode.EpisodePatientVisit (PatientVisitId,EpisodeId,EpisodeStatusTypeId,' \
          'Notes,IsHidden, DateCreated,CreatedBy, DateStatusTypeIdUpdated, StatusTypeIdUpdatedBy) ' \
          'VALUES (%s, %s, NULL, \'\', 0, GETDATE(), \'sound.caller\', NULL, NULL)' % \
          (p_visit_id, episode_id)
    dh.execute_prince_query_command(env, sql, None)


def create_subsequent_episode_using_query(acute_site_id, ext_visit_id, episode_type):
    patient_id, episode_id = create_episode_using_query(acute_site_id, ext_visit_id, episode_type, nsoc=1)


def create_episode_using_query(acute_site_id, ext_visit_id, episode_type, nsoc=1):
    p_id, e_id = e.program_payer_id_and_eligibility_engine_id(episode_type)
    first_name = e.generate_random_name('Patient')
    admit_date = date.today() - timedelta(days=5)

    p_visit_id = insert_patient_visit(acute_site_id, ext_visit_id, first_name, episode_type, '1980-01-01', admit_date)
    insert_patient_discharge_plan(p_visit_id)
    episode_id = insert_episode(p_id, e_id, ext_visit_id)
    insert_episode_patient_visit(p_visit_id, episode_id)
    return p_visit_id, episode_id


def create_episode_using_query_test(acute_site_id, ext_visit_id, episode_type, dob, fname, lname, admit_date, nsoc=1):
    p_id, e_id = e.program_payer_id_and_eligibility_engine_id(episode_type)

    p_visit_id = insert_patient_visit(acute_site_id, ext_visit_id, fname, lname, dob, admit_date)
    insert_patient_discharge_plan(p_visit_id)
    episode_id = insert_episode(p_id, e_id, ext_visit_id)
    insert_episode_patient_visit(p_visit_id, episode_id)
    return p_visit_id, episode_id


def insert_acute_site_program (acute_site_id, program_id):
    sql = "INSERT INTO Config.AcuteSiteProgram (ReferenceId,TenantId,AcuteSiteId,ProgramId,TIN,ProgramStartDate," \
          "ProgramEndDate,RiskDate,DateCreated,CreatedBy) VALUES (NEWID(), 1, %s, %s, NULL, \'2020-04-01\', NULL, " \
          "\'2020-04-01\', GETDATE(), \'admtnguyens@soundphysicians.com\')" % (acute_site_id, program_id)
    dh.execute_prince_query_command(env, sql, None)


def setup_acute_site_programs(acute_site_id):
    delete_all_acute_site_programs(acute_site_id)
    insert_acute_site_program(acute_site_id, 4)
    insert_acute_site_program(acute_site_id, 5)
    insert_acute_site_program(acute_site_id, 6)
    insert_acute_site_program(acute_site_id, 10)
    insert_acute_site_program(acute_site_id, 15)
    insert_acute_site_program(acute_site_id, 16)
    insert_acute_site_program(acute_site_id, 17)
    insert_acute_site_program(acute_site_id, 18)


def insert_intervention_response(patient_visit_id, type):
    sql = 'INSERT INTO Intervention.Response(TypeId,PatientVisitId,DateCreated,CreatedBy) ' \
          'VALUES (%s, %s, GETDATE(), \'admtnguyen\')' % (type, patient_visit_id)
    dh.execute_prince_query_command(env, sql, None)
    response_id = get_intervention_response_id(patient_visit_id, type)
    return response_id


def insert_intervention_response_answer(response_id, question_key, answer):
    sql = 'INSERT INTO Intervention.ResponseAnswer(ResponseId,QuestionKey,Answer) ' \
           'VALUES (%s, \'%s\', \'%s\')' % (response_id, question_key, answer)
    dh.execute_prince_query_command(env, sql, None)


def insert_intervention_response_hcc(response_id, hcc_id, status_id, pre_diagnosis, attestation):
    sql = 'INSERT INTO Intervention.ResponseHcc (ResponseId, HccId, StatusId, PreviousDiagnosisText, Attestation, ' \
          'AttestationBy, AttestationDateModified, IsPotentialNew) ' \
          'VALUES (%s, %s, %s, \'%s\', %s, \'ttnguyen\', GETDATE(), NULL)' % (response_id, hcc_id, status_id, pre_diagnosis, attestation)
    dh.execute_prince_query_command(env, sql, None)


def create_hcc_intervention_response_answer(response_id, resp_status, member_id, attestation_finalized):
    insert_intervention_response_answer(response_id, 'responseStatus', resp_status)
    insert_intervention_response_answer(response_id, 'memberId', member_id)
    if attestation_finalized is True:
        insert_intervention_response_answer(response_id, 'attestationFinalizedBy', 'ttnguyen')


def create_hcc_intervention_response_hcc(response_id, hcc_id, status_id, pre_diagnosis, attestation):
    insert_intervention_response_hcc(response_id, hcc_id, status_id, pre_diagnosis, attestation)


def create_hcc_record(patient_visit_id, resp_status, member_id, attestation_finalized, hcc_id, status_id,
                      pre_diagnosis, attestation):
    response_id = insert_intervention_response(patient_visit_id, 2)

    create_hcc_intervention_response_answer(response_id, resp_status, member_id, attestation_finalized)
    create_hcc_intervention_response_hcc(response_id, hcc_id, status_id, pre_diagnosis, attestation)


def create_readmission_risk_survey_record(patient_visit_id, ans0, ans1, ans2, ans3, ans4, ans5):
    response_id = insert_intervention_response(patient_visit_id, 8)
    insert_intervention_response_answer(response_id, 'schedulePCPFollowUp', ans0)
    insert_intervention_response_answer(response_id, 'communicationToPCP', ans1)
    insert_intervention_response_answer(response_id, 'dischargeSummaryComplete', ans2)
    insert_intervention_response_answer(response_id, 'medReconciliation', ans3)
    insert_intervention_response_answer(response_id, 'medsToBed', ans4)
    insert_intervention_response_answer(response_id, 'teachBackDischargeInstructions', ans5)
    return response_id


def create_pcp_appt_record(patient_visit_id, appt_date, appt_status):
    response_id = insert_intervention_response(patient_visit_id, 1)
    insert_intervention_response_answer(response_id, 'appointmentDate', appt_date)
    insert_intervention_response_answer(response_id, 'appointmentNotes', 'Here are some new notes')
    insert_intervention_response_answer(response_id, 'appointmentStatus', appt_status)
    insert_intervention_response_answer(response_id, 'pcpName', 'pcpName')
    return response_id


# --------------------------------------------------------------------------------------------------------------- #
# ---------------------------------------------- Update Query --------------------------------------------------- #
# --------------------------------------------------------------------------------------------------------------- #
def update_intervention_response_answer(response_id, question_key, answer):
    sql = 'UPDATE Intervention.ResponseAnswer SET Answer = %s ' \
          'WHERE ResponseId = %s AND QuestionKey = \'%s\'' % (answer, response_id, question_key )
    dh.execute_prince_query_command(env, sql, None)


def update_post_acute_care_type(nsoc, p_visit_id):
    sql = 'UPDATE Patient.DischargePlan SET DischargeDispositionActual = %s WHERE VisitId = %s' % (nsoc, p_visit_id)
    if nsoc == 0:
        sql = 'UPDATE Episode.PatientVisit SET PostAcuteCareTypeId = NULL WHERE VisitId = %s' % p_visit_id
    dh.execute_prince_query_command(env, sql, None)


def update_discharge_date(ext_visit_id, discharge_date):
    sql = 'UPDATE Patient.PatientVisit SET DischargeDate = \'%s\' WHERE ExternalVisitId = \'%s\'' % (discharge_date, ext_visit_id )
    dh.execute_prince_query_command(env, sql, None)


def update_readmission_risk_score(p_visit_id, score):
    sql = 'UPDATE Patient.DischargePlan SET ReadmissionRiskScore = %s  WHERE VisitId = %s' % (score, p_visit_id)
    if score == 0:
        sql = 'UPDATE Patient.DischargePlan SET ReadmissionRiskScore = NULL  WHERE VisitId = %s' % p_visit_id
    dh.execute_prince_query_command(env, sql, None)


def update_episode_is_hidden_status(episode_id, is_hidden):
    sql = 'UPDATE Episode.EpisodePatientVisit SET IsHidden = %s  WHERE EpisodeId = %s' % (is_hidden, episode_id)
    dh.execute_prince_query_command(env, sql, None)


def update_episode_status(episode_id, status):
    sql = 'UPDATE Episode.Episode SET EpisodeStatusTypeId = %s  WHERE Id = %s' % (status, episode_id)
    dh.execute_prince_query_command(env, sql, None)


def update_episode_patient_visit_status(episode_id, status):
    sql = 'UPDATE Episode.EpisodePatientVisit SET EpisodeStatusTypeId = %s  WHERE EpisodeId = %s' % (status, episode_id)
    if status == 0:
        sql = 'UPDATE Episode.EpisodePatientVisit SET EpisodeStatusTypeId = NULL WHERE EpisodeId = %s' % episode_id
    dh.execute_prince_query_command(env, sql, None)


def update_episode_patient_visit(episode_id, status, is_hidden=0):
    sql = 'UPDATE Episode.EpisodePatientVisit SET EpisodeStatusTypeId = %s, isHidden = %s WHERE EpisodeId = %s' % (status, is_hidden, episode_id)
    dh.execute_prince_query_command(env, sql, None)


# --------------------------------------------------------------------------------------------------------------- #
# ---------------------------------------------- Delete Query --------------------------------------------------- #
# --------------------------------------------------------------------------------------------------------------- #
def delete_pcp_appt_record(response_id):
    sql1 = 'DELETE FROM Intervention.ResponseAnswer WHERE ResponseId = %s' % response_id
    sql2 = 'DELETE FROM Intervention.Response WHERE Id = %s' % response_id
    dh.execute_prince_query_command(env, sql1, None)
    dh.execute_prince_query_command(env, sql2, None)


def delete_intervention_survey_records(p_visit_id, intervention_type):
    response_id = get_intervention_response_id(p_visit_id, intervention_type)
    delete_intervention_response_answer_records(response_id)
    delete_intervention_response_hcc_records(response_id)
    delete_intervention_response(response_id)


def delete_intervention_response_answer_records(response_id):
    sql = 'DELETE FROM Intervention.ResponseAnswer WHERE ResponseId = %s' % response_id
    dh.execute_prince_query_command(env, sql, None)


def delete_intervention_response_hcc_records(response_id):
    sql = 'DELETE FROM Intervention.ResponseHCC WHERE ResponseId = %s' % response_id
    dh.execute_prince_query_command(env, sql, None)


def delete_intervention_response_answer(response_id, key):
    sql = 'DELETE FROM Intervention.ResponseAnswer WHERE ResponseId = %s AND QuestionKey = \'%s\'' % (response_id, key)
    dh.execute_prince_query_command(env, sql, None)


def delete_intervention_response(response_id):
    sql = 'DELETE FROM Intervention.Response WHERE Id = %s' % response_id
    dh.execute_prince_query_command(env, sql, None)



def delete_episode_records(external_visit_id, episode_id):
    delete_episode_patient_visit(episode_id)
    delete_episode(episode_id)
    delete_patient_visit(external_visit_id)


def delete_patient_visit(external_visit_id):
    sql = 'DELETE FROM Patient.PatientVisit WHERE ExternalVisitId = \'%s\'' % external_visit_id
    res = dh.execute_prince_query_command(env, sql, None)


def delete_episode(episode_id):
    sql = 'DELETE FROM Episode.Episode where Id = %s' % episode_id
    res = dh.execute_prince_query_command(env, sql, None)


def delete_episode_patient_visit(episode_id):
    sql = 'DELETE FROM Episode.EpisodePatientVisit where EpisodeId = %s' % episode_id
    res = dh.execute_prince_query_command(env, sql, None)


def delete_all_acute_site_programs(acute_site_id):
    sql = 'DELETE FROM Config.AcuteSiteProgram WHERE AcuteSiteId = %s' % acute_site_id
    res = dh.execute_prince_query_command(env, sql, None)


# --------------------------------------------------------------------------------------------------------------- #
# ---------------------------------------------- Select Query --------------------------------------------------- #
# --------------------------------------------------------------------------------------------------------------- #
def get_intervention_response_answers(patient_visit_id, type_id):
    sql = 'SELECT r.PatientVisitId, ra.ResponseId, ra.QuestionKey, ra.Answer FROM Intervention.Response r ' \
          'JOIN Intervention.ResponseAnswer ra ON r.Id = ra.ResponseId ' \
          'WHERE r.PatientVisitId = %s AND r.TypeId = %s ORDER BY ra.Id' % (patient_visit_id, type_id)

    time.sleep(3)
    res = dh.execute_prince_query(env, sql, None)
    if res[0].__len__() != 0:
        return res
    else:
        return None


def get_intervention_response_id(patient_visit_id, type):
    sql = 'SELECT Id FROM Intervention.Response WHERE PatientVisitId = %s AND TypeId = %s' % (patient_visit_id, type)
    time.sleep(1)
    res = dh.execute_prince_query(env, sql, None)
    if res[0].__len__() != 0:
        return res[0][0].Id
    else:
        return None


def get_patient_visit_id(ext_visit_id):
    sql = 'SELECT Id FROM Patient.PatientVisit WHERE ExternalVisitId = \'%s\'' % ext_visit_id
    time.sleep(3)
    res = dh.execute_prince_query(env, sql, None)
    if res[0].__len__() != 0:
        return res[0][0].Id
    else:
        return None


def get_latest_episode_id():
    sql = 'SELECT TOP(1) Id FROM Episode.Episode ORDER BY Id DESC'
    res = dh.execute_prince_query(env, sql, None)
    if res[0].__len__() != 0:
        return res[0][0].Id
    else:
        return None


def get_bpcia_dismissal_answer(external_visit_id):
    sql = 'SELECT ir.PatientVisitId, p.ExternalVisitId, ir.TypeId, ra.ResponseId, ra.QuestionKey, ra.Answer ' \
          'FROM Patient.PatientVisit p	' \
          'JOIN Intervention.Response ir ON ir.PatientVisitId = p.Id ' \
          'JOIN Intervention.ResponseAnswer ra ON ra.ResponseId = ir.Id ' \
          'WHERE p.ExternalVisitId = \'%s\'' % external_visit_id

    time.sleep(15)
    result = None
    for x in range(1, 7):
        result = dh.execute_prince_query(env, sql, None)
        if result[0].__len__() != 0:
            break
        time.sleep(3)
    if result[0].__len__() > 0:
        return result
    else:
        return None


def get_readmission_score(p_visit_id, wait=10):
    sql = 'SELECT ReadmissionRiskScore FROM Patient.DischargePlan WHERE VisitId = %s' % p_visit_id

    time.sleep(wait)
    result = dh.execute_prince_query(env, sql, None)
    return result


def get_episode(external_visit_id, wait=40):
    # p.AnchorBundleScore, p.SubsequentBundleScore
    # e.EpisodeTypeId
    result = None
    sql = 'SELECT p.AcuteSiteId, p.ExternalVisitId, ep.EpisodeId, ep.PatientVisitId, ' \
          'p.FacilityAccountNumber, p.MedicalRecordNumber, p.FirstName, ' \
          'p.LastName, p.DateOfBirth, p.AdmitDate, p.DischargeDate, p.IsOneTimeCharge, p.PlaceOfServiceTypeId, ' \
          'p.RoomNumber, e.ProgramPayerId, e.EligibilityEngineId, e.EpisodePurposeId, e.EpisodeStatusTypeId, ' \
          'e.IsManualEpisode ' \
          'FROM Patient.PatientVisit p ' \
          'JOIN Episode.EpisodePatientVisit ep ON p.Id = ep.PatientVisitId ' \
          'JOIN Episode.Episode e ON ep.EpisodeId = e.Id WHERE p.ExternalVisitId = \'%s\'' % external_visit_id
    # result = dh.execute_prince_query(env, sql, None)

    time.sleep(wait)
    for x in range(1, 7):
        result = dh.execute_prince_query(env, sql, None)
        if result[0].__len__() != 0:
            break
        time.sleep(3)
    if result[0].__len__() > 0:
        return result
    else:
        return None


def get_episode_by_episode_id(episode_id, wait=40):
    # p.AnchorBundleScore, p.SubsequentBundleScore
    result = None
    sql = 'SELECT p.AcuteSiteId, p.ExternalVisitId, p.FacilityAccountNumber, p.MedicalRecordNumber, p.FirstName, ' \
          'p.LastName, p.DateOfBirth, p.AdmitDate, p.DischargeDate, p.IsOneTimeCharge, p.PlaceOfServiceTypeId, ' \
          'p.RoomNumber, e.ProgramPayerId, e.EligibilityEngineId, e.EpisodeTypeId, e.EpisodeStatusTypeId, ' \
          'e.IsManualEpisode ' \
          'FROM Patient.PatientVisit p ' \
          'JOIN Episode.EpisodePatientVisit ep ON p.Id = ep.PatientVisitId ' \
          'JOIN Episode.Episode e ON ep.EpisodeId = e.Id WHERE e.Id = %s' % episode_id
    # result = dh.execute_prince_query(env, sql, None)

    time.sleep(wait)
    for x in range(1, 7):
        result = dh.execute_prince_query(env, sql, None)
        if result[0].__len__() != 0:
            break
        time.sleep(3)
    if result[0].__len__() > 0:
        return result
    else:
        return None


def get_udf_find_episodes_for_visit(tenant_id, ext_visit_id, wait=1):
    sql = 'SELECT ExternalVisitId, AnchorVisitId, PatientVisitPkId, AnchorPatientVisitPkId, AcuteSiteId, ' \
          'AcuteSiteExternalId, EpisodeId, EpisodeStatusTypeId, EpisodeStatus, ProgramPayerName, ProgramPayerName, ' \
          'DischargeDate, EpisodeCreatedDate, VisitCreatedDate, IsHidden, IsVisible, IsOverride, EpisodeSource, ' \
          'EpisodePurpose ' \
          'FROM Config.udf_find_episodes_for_visit(%s,\'%s\')' % (tenant_id, ext_visit_id)

    res = None
    for x in range(1, 20):
        time.sleep(wait)
        res = dh.execute_prince_query(env, sql, None)
        if res[0].__len__() != 0:
            break
    if res[0].__len__() > 0:
        return res
    else:
        return None


def get_udf_find_episodes_for_all(tenant_id, ext_visit_id, wait=1):
    sql = 'SELECT ExternalVisitId, AnchorVisitId, PatientVisitPkId, AnchorPatientVisitPkId, AcuteSiteId, ' \
          'AcuteSiteExternalId, EpisodeStatusTypeId, EpisodeStatus, ProgramPayerName, DischargeDate, IsHidden, ' \
          'IsVisible, IsOverride FROM Config.udf_find_episodes_for_all(%s) ' \
          'WHERE ExternalVisitId =  \'%s\'' % (tenant_id, ext_visit_id)
    res = None
    for x in range(1, 20):
        time.sleep(wait)
        res = dh.execute_prince_query(env, sql, None)
        if res[0].__len__() != 0:
            break
    if res[0].__len__() > 0:
        return res
    else:
        return None


def get_udf_find_episodes_for_site(tenant_id, acute_site_id, ext_visit_id, wait=1):
    sql = 'SELECT * FROM Config.udf_find_episodes_for_site(%s, %s) WHERE ExternalVisitId = \'%s\'' % (tenant_id, acute_site_id, ext_visit_id)
    res = None
    for x in range(1, 20):
        time.sleep(wait)
        res = dh.execute_prince_query(env, sql, None)
        if res[0].__len__() != 0:
            break
    if res[0].__len__() > 0:
        return res
    else:
        return None


def get_program_id(program_name):
    program_id = dict()
    program_id["arbor"] = 4
    program_id["uhc"] = 5
    program_id["wellmed"] = 6
    program_id["prospect"] = 7
    program_id["christus"] = 10
    program_id["mtcarmel"] = 15
    program_id["pcrozer"] = 16
    program_id["humana"] = 17
    program_id["aledade"] = 18
    return program_id


def get_episode_patient_visit_by_episode_id(episode_id, wait=1):
    sql = 'SELECT Id, PatientVisitId, EpisodeId, EpisodeStatusTypeId, Notes, IsHidden ' \
          'FROM Episode.EpisodePatientVisit ' \
          'WHERE EpisodeId = %s' % episode_id
    time.sleep(wait)
    res = dh.execute_prince_query(env, sql, None)
    if res[0].__len__() != 0:
        return res
    else:
        return None


def get_patient_discharge_plan(patient_visit_id, wait=1):
    sql = 'SELECT VisitId,	DischargeDispositionActual, DischargeDispositionProvider, DischargeDispositionCareTeam, ' \
          'DischargeDispositionPatient, DischargeLocationId, ReadmissionRiskScore, AnticipatedDischargeDate, Note, ' \
          'SurpriseQuestion, DateCreated, CreatedBy ' \
          'FROM Patient.DischargePlan ' \
          'WHERE VisitId = %s' % patient_visit_id
    time.sleep(wait)
    res = dh.execute_prince_query(env, sql, None)
    if res[0].__len__() != 0:
        return res
    else:
        return None


def get_avoidable_day(visit_id, wait):
    sql = 'SELECT VisitId, DateOfService, AvoidableDayTypeId, Notes FROM Patient.AvoidableDay WHERE VisitId = %s' % visit_id
    time.sleep(wait)
    res = dh.execute_prince_query(env, sql, None)
    if res[0].__len__() != 0:
        return res
    else:
        return None


def get_episode_patient_visit_status (visit_id, wait):
    sql = 'SELECT PatientVisitId, EpisodeId, EpisodeStatusTypeId, IsHidden FROM Episode.EpisodePatientVisit WHERE PatientVisitId = 1417308' % visit_id
    time.sleep(wait)
    res = dh.execute_prince_query(env, sql, None)
    if res[0].__len__() != 0:
        return res
    else:
        return None

def execute_upd_episode_precedence_uipdate(visit_id):
    sql = 'EXECUTE [Episode].[udp_episode_precedence_update] %s' % visit_id
    dh.execute_prince_query_command(env, sql, None)
