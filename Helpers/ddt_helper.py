import unittest
try:
    import yaml
except ImportError:  # pragma: no cover
    have_yaml_support = False
else:
    have_yaml_support = True
    del yaml

# A good-looking decorator
needs_yaml = unittest.skipUnless(
    have_yaml_support, "Need YAML to run this test"
)


class Mylist(list):
    pass


def annotated(a, b):
    r = Mylist([a, b])
    setattr(r, "__name__", "test_%d_greater_than_%d" % (a, b))
    return r


def annotated2(listIn, name, docstring):
    r = Mylist(listIn)
    setattr(r, "__name__", name)
    setattr(r, "__doc__", docstring)
    return r
