from random import *
from Helpers import utilities as u, episode_management_helper as p


def get_program_info(program):
    payer_id = None
    payer_name = None
    eligibility_engine_id = None
    eligibility_engine_name = None
    if program == 'Manual':
        eligibility_engine_id = None
        eligibility_engine_name = 'UserOverride'
        payer_id = '4E6BA003-016C-4715-88F6-23A297E9E93F'
        payer_name = 'BPCIA'
    elif program == 'Bpcia':
        eligibility_engine_id = '6D818699-8E67-48FB-BF6B-91B27FD5DB32'
        eligibility_engine_name = 'ArborBee'
        payer_id = '4E6BA003-016C-4715-88F6-23A297E9E93F'
        payer_name = 'BPCIA'
    elif program == 'Uhc':
        eligibility_engine_id = '1A47D0E4-6602-42E4-90AF-4D1E123C09AC'
        eligibility_engine_name = 'PHMA - United Health Care'
        payer_id = 'DD4884D0-9099-4D0A-873A-2BB44FD565E3'
        payer_name = 'United Health Care'
    elif program == 'WellMed':
        eligibility_engine_id = '65119393-7CAA-41B1-8FCE-822CFAD83BAA'
        eligibility_engine_name = 'PHMA - WellMed'
        payer_id = 'FCCFAFEA-1E39-4A06-8513-36FC4B80830B'
        payer_name = 'WellMed'
    elif program == 'Prospect':
        eligibility_engine_id = '2867FCC4-F083-4BAE-B1E6-B001D20F0DAF'
        eligibility_engine_name = 'ACO - Prospect'
        payer_id = '1A3C167F-FAC4-4FB2-A468-A1027682922D'
        payer_name = 'Prospect'
    elif program == 'Christus':
        eligibility_engine_id = '185999BC-0E24-410C-98C7-36F0FEB934D1'
        eligibility_engine_name = 'PHACO Christus'
        payer_id = 'DCE2CFEB-1943-40AF-BFF3-412CCD485C7C'
        payer_name = 'Christus'
    elif program == 'MtCarmel':
        eligibility_engine_id = 'BE7CF3D7-5A95-43FA-8FF8-42CB970D671A'
        eligibility_engine_name = 'PHMA - Mt. Carmel'
        payer_id = '5BA61434-04F5-451B-9A22-F21514C362F9'
        payer_name = 'Mt Carmel'
    elif program == 'ProspectCrozer':
        eligibility_engine_id = '41E3AE61-E950-48BF-80A6-0EC3C65A8F85'
        eligibility_engine_name = 'ACO - Prospect Crozer'
        payer_id = 'D77A922E-97C9-44DE-914B-696FA561BD71'
        payer_name = 'Prospect Crozer'
    elif program == 'Humana':
        eligibility_engine_id = '292D96BB-B422-4954-9049-9C8C3AD3F30E'
        eligibility_engine_name = 'PHMA Humana'
        payer_id = '77F4A318-338E-4FCC-B90D-118201005115'
        payer_name = 'Humana'
    elif program == 'Aledade':
        eligibility_engine_id = '5809C09A-B5ED-4B6B-8853-FEF713E69B6E'
        eligibility_engine_name = 'PHACO Aledade'
        payer_id = '260A06C2-CA7D-4623-B2F3-B7E0782150C6'
        payer_name = 'Aledade'
    elif program == 'Carolina':
        eligibility_engine_id = 'DCFD29DA-B3A5-470F-983A-AFA463273B77'
        eligibility_engine_name = 'PHACO Coastal Carolina QC'
        payer_id = 'AB784B69-BE21-48BB-BA65-72322A27C183'
        payer_name = 'Coastal Carolina QC'
    elif program == 'Chenmed':
        eligibility_engine_id = 'DE9EC0E6-4BC4-4369-A6B6-1921F427C139'
        eligibility_engine_name = 'TCMA ChenMed'
        payer_id = '8D900AEB-B382-419D-A1E9-92F6B9EDE9A3'
        payer_name = 'ChenMed'
    elif program == 'Catalyst':
        eligibility_engine_id = '416557BF-1E18-4196-82D1-941A490F2144'
        eligibility_engine_name = 'PHMA Catalyst'
        payer_id = '37509EC8-D567-4001-83F1-74E00D5104CE'
        payer_name = 'Catalyst'
    elif program == 'Iora':
        eligibility_engine_id = '9D47E2A7-EB24-40C4-AB9C-030336522B9B'
        eligibility_engine_name = 'PHMA Iora'
        payer_id = 'E9BAA527-78C1-446A-9365-6E5830F11F3C'
        payer_name = 'Iora'
    return eligibility_engine_id, eligibility_engine_name, payer_id, payer_name


def program_payer_id_and_eligibility_engine_id(episode_type):
    program_payer_id = None
    eligibility_engine_id = None

    if episode_type == 'Manual':
        program_payer_id = 1
        eligibility_engine_id = None

    elif episode_type == 'Bpcia':
        program_payer_id = 1
        eligibility_engine_id = 3

    elif episode_type == 'Uhc':
        program_payer_id = 2
        eligibility_engine_id = 1

    elif episode_type == 'WellMed':
        program_payer_id = 3
        eligibility_engine_id = 5

    elif episode_type == 'Prospect':
        program_payer_id = 4
        eligibility_engine_id = 6

    elif episode_type == 'Christus':
        program_payer_id = 7
        eligibility_engine_id = 7

    elif episode_type == 'MtCarmel':
        program_payer_id = 8
        eligibility_engine_id = 8

    elif episode_type == 'ProspectCrozer':
        program_payer_id = 9
        eligibility_engine_id = 9

    elif episode_type == 'Humana':
        program_payer_id = 10
        eligibility_engine_id = 10

    elif episode_type == 'Aledade':
        program_payer_id = 11
        eligibility_engine_id = 11

    elif episode_type == 'Carolina':
        program_payer_id = 12
        eligibility_engine_id = 12

    elif episode_type == 'Chenmed':
        program_payer_id = 13
        eligibility_engine_id = 13

    return program_payer_id, eligibility_engine_id


def update_patient_visit(payload, visit_number, eligibility_engine_id, eligibility_engine_name, payer_id,
                         payer_name, status, discharge_date=None, pos='Inpatient', pos_code='21',
                         is_onetime_charge=False, admit_date='2020-06-08', hospital_admit_date='2020-06-08',
                         is_patient_expired=False, is_focus_patient=False, bundle_id='0'):
    payload["patientVisit"]["visitNumber"] = visit_number
    payload["patientVisit"]["admitDate"] = admit_date
    payload["patientVisit"]["dischargeDate"] = discharge_date
    payload["patientVisit"]["hospitalAdmitDate"] = hospital_admit_date
    payload["patientVisit"]["pos"] = pos
    payload["patientVisit"]["posCode"] = pos_code
    payload["patientVisit"]["isPatientExpired"] = is_patient_expired
    payload["patientVisit"]["isFocusPatient"] = is_focus_patient
    payload["patientVisit"]["isOneTimeCharge"] = is_onetime_charge
    payload["patientVisit"]["eligibilityEngineId"] = eligibility_engine_id
    payload["patientVisit"]["eligibilityEngineName"] = eligibility_engine_name
    payload["patientVisit"]["payerId"] = payer_id
    payload["patientVisit"]["payerName"] = payer_name
    payload["patientVisit"]["episodeStatus"] = status
    payload["patientVisit"]["bundleId"] = bundle_id


def update_procedure_code(payload, is_none_billable, is_consult, is_one_time):
    if is_none_billable == 1:
        payload["procedures"][0]["procedureCode"] = 'G8427'
        payload["procedures"][0]["isBillable"] = False
        payload["procedures"][1]["procedureCode"] = 'G8452'
        payload["procedures"][1]["isBillable"] = False
    if is_consult == 1:
        payload["procedures"][0]["procedureCode"] = '99221'
        payload["procedures"][0]["isBillable"] = True
        payload["procedures"][1]["procedureCode"] = 'G8430'
        payload["procedures"][1]["isBillable"] = False
    if is_one_time == 1:
        payload["procedures"][0]["procedureCode"] = '99281'
        payload["procedures"][0]["isBillable"] = True
        payload["procedures"][1]["procedureCode"] = 'G8430'
        payload["procedures"][1]["isBillable"] = False


def update_place_of_service(payload, pos, pos_code):
    payload["patientVisit"]["pos"] = pos
    payload["patientVisit"]["posCode"] = str(pos_code)


def update_one_time_charge(payload, is_onetime_charge):
    if is_onetime_charge == 1:
        payload["patientVisit"]["isOneTimeCharge"] = True


def update_patient_info(payload, first_name, last_name, dob, sex='M', zip='98001'):
    payload["patientInformation"]["firstName"] = first_name
    payload["patientInformation"]["lastName"] = last_name
    payload["patientInformation"]["dateOfBirth"] = dob
    payload["patientInformation"]["sex"] = sex
    payload["patientInformation"]["postalCode"] = zip


def update_header(payload, message_type, tenant_id, data_generated='2019-11-18T19:41:14.9056814Z'):
    payload["header"]["correlationId"] = generate_correlation_id()
    payload["header"]["messageType"] = message_type
    payload["header"]["dateGenerated"] = data_generated
    payload["header"]["tenantId"] = tenant_id


def update_diagnoses(payload, node, code, type, codingMethod, relateGroup, priority, sequenceNumber):
    payload["diagnoses"][node]["code"] = code
    payload["diagnoses"][node]["type"] = type
    payload["diagnoses"][node]["codingMethod"] = codingMethod
    payload["diagnoses"][node]["relateGroup"] = relateGroup
    payload["diagnoses"][node]["priority"] = priority
    payload["diagnoses"][node]["sequenceNumber"] = sequenceNumber

def update_acute_site(payload, ccn, acute_site_name, external_id):
    payload["acuteSite"]["ccn"] = ccn
    payload["acuteSite"]["name"] = acute_site_name
    payload["acuteSite"]["externalId"] = external_id  # string


def update_post_acute_site(
        payload, care_type_id, ccn=None, site_name=None, address=None, city=None, state=None, zip=None, phone=None):
    payload["postAcuteSite"]["careTypeId"] = care_type_id
    payload["postAcuteSite"]["ccn"] = ccn
    payload["postAcuteSite"]["name"] = site_name
    payload["postAcuteSite"]["address"] = address
    payload["postAcuteSite"]["city"] = city
    payload["postAcuteSite"]["state"] = state
    payload["postAcuteSite"]["zip"] = zip
    payload["postAcuteSite"]["phone"] = phone


def generate_visit_number(first_digits='1001'):
    visit_id = first_digits + str(randint(10000, 99900))
    return visit_id


def generate_random_name(name):
    random_name = name + str(randint(10000, 99900))
    return random_name


def generate_correlation_id(leading_string='THANNGUY-8AAB-46F4-BD69-E673B'):
    correlation_id = leading_string + str(randint(1000000, 9900000))
    return correlation_id


def episode_status(status_name):
    if status_name == 'Candidate: Anchor Stay' or status_name == 'Candidate: Subsequent Stay':
        return 1
    elif status_name == 'Confirmed: Anchor Stay' or status_name == 'Confirmed: Subsequent Stay':
        return 2
    elif status_name == 'Dismissed':
        return 5
    elif status_name == 'System Dismissed':
        return 8
    elif status_name == 'Completed':
        return 7


def bool_value(value):
    if value == 'true':
        return 1
    elif value == 'false':
        return 0


def verify_patient_visit_info (result, payload):
    first_name = payload["patientInformation"]["firstName"]
    last_name = payload["patientInformation"]["lastName"]
    dob = payload["patientInformation"]["dateOfBirth"]

    u.assert_and_log(result[0][0].ExternalVisitId == payload["patientVisit"]["visitNumber"],
                     'DB Results: ' + result[0][0].ExternalVisitId + ' Expected: ' + payload["patientVisit"]["visitNumber"])

    u.assert_and_log(result[0][0].FirstName == payload["patientInformation"]["firstName"],
                     'DB Results: ' + result[0][0].FirstName + ' Expected: ' + payload["patientInformation"]["firstName"])

    u.assert_and_log(result[0][0].LastName == payload["patientInformation"]["lastName"],
                     'DB Results: ' + result[0][0].LastName + ' Expected: ' + payload["patientInformation"]["lastName"])

    u.assert_and_log(str(result[0][0].DateOfBirth) == payload["patientInformation"]["dateOfBirth"],
                     'DB Results: ' + str(result[0][0].DateOfBirth) + ' Expected: ' + payload["patientInformation"]["dateOfBirth"])

    u.assert_and_log(str(result[0][0].AdmitDate) == payload["patientVisit"]["admitDate"],
                     'DB Results: ' + str(result[0][0].AdmitDate) + ' Expected: ' + payload["patientVisit"]["admitDate"])

    #if payload["patientVisit"]["dischargeDate"] is None:
        #u.assert_and_log(result[0][0].DischargeDate == payload["patientVisit"]["dischargeDate"], 'Discharge date should be null')
    #else:
        #u.assert_and_log(str(result[0][0].DischargeDate) == payload["patientVisit"]["dischargeDate"],
                         #'DB Results: ' + str(result[0][0].DischargeDate) + ' Expected: ' + payload["patientVisit"]["dischargeDate"])

    u.assert_and_log(str(result[0][0].PlaceOfServiceTypeId) == payload["patientVisit"]["posCode"],
                     'DB Results: ' + str(result[0][0].PlaceOfServiceTypeId) + ' Expected: ' + payload["patientVisit"]["posCode"])


def verify_episode_info(result, payload, engine_id, program_payer_id):

    episode_status_id = p.episode_status(payload["patientVisit"]["episodeStatus"])

    u.assert_and_log(result[0][0].ProgramPayerId == program_payer_id,
                     'DB Results: ' + str(result[0][0].ProgramPayerId) + ' Expected: ' + str(program_payer_id))

    u.assert_and_log(result[0][0].EpisodeStatusTypeId == episode_status_id,
                     'DB Results: ' + str(result[0][0].EpisodeStatusTypeId) + ' Expected: ' + str(episode_status_id))

    if payload["patientVisit"]["eligibilityEngineName"] == 'UserOverride':
        u.assert_and_log(result[0][0].IsManualEpisode == 1, 'This should be Manual episode')
        u.assert_and_log(result[0][0].EligibilityEngineId is None, 'Manual episode should not have eligibility engine')
    else:
        u.assert_and_log(result[0][0].IsManualEpisode == 0, 'This should not be Manual episode')
        u.assert_and_log(result[0][0].EligibilityEngineId == engine_id,
                         'DB Results: ' + str(result[0][0].EligibilityEngineId) + ' Expected: ' + str(engine_id))


def verify_vbh_episode_info(result, payload, engine_id, program_payer_id):

    episode_status_id = p.episode_status(payload["patientVisit"]["episodeStatus"])

    u.assert_and_log(result[0][0].ProgramPayerId == program_payer_id,
                     'DB Results: ' + str(result[0][0].ProgramPayerId) + ' Expected: ' + str(program_payer_id))

    u.assert_and_log(result[0][0].EpisodeStatusTypeId == episode_status_id,
                     'DB Results: ' + str(result[0][0].EpisodeStatusTypeId) + ' Expected: ' + str(episode_status_id))

    u.assert_and_log(result[0][0].EligibilityEngineId == engine_id,
                     'DB Results: ' + str(result[0][0].EligibilityEngineId) + ' Expected: ' + str(engine_id))