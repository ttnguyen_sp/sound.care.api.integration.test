import json


def get_payload_data(file_path):
    with open(file_path, "r") as read_file:
        data = json.load(read_file)
    return data
