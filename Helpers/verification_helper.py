import time
from Helpers import utilities as u, episode_management_helper as p


def verify_patient_visit_info (result, payload):
    first_name = payload["patientInformation"]["firstName"]
    last_name = payload["patientInformation"]["lastName"]
    dob = payload["patientInformation"]["dateOfBirth"]


    u.assert_and_log(result[0][0].ExternalVisitId == payload["patientVisit"]["visitNumber"],
                     'DB Results: ' + result[0][0].ExternalVisitId + ' Expected: ' + payload["patientVisit"]["visitNumber"])

    u.assert_and_log(result[0][0].FirstName == payload["patientInformation"]["firstName"],
                     'DB Results: ' + result[0][0].FirstName + ' Expected: ' + payload["patientInformation"]["firstName"])

    u.assert_and_log(result[0][0].LastName == payload["patientInformation"]["lastName"],
                     'DB Results: ' + result[0][0].LastName + ' Expected: ' + payload["patientInformation"]["lastName"])

    u.assert_and_log(str(result[0][0].DateOfBirth) == payload["patientInformation"]["dateOfBirth"],
                     'DB Results: ' + str(result[0][0].DateOfBirth) + ' Expected: ' + payload["patientInformation"]["dateOfBirth"])

    u.assert_and_log(str(result[0][0].AdmitDate) == payload["patientVisit"]["admitDate"],
                     'DB Results: ' + str(result[0][0].AdmitDate) + ' Expected: ' + payload["patientVisit"]["admitDate"])

    if payload["patientVisit"]["dischargeDate"] is None:
        u.assert_and_log(result[0][0].DischargeDate == payload["patientVisit"]["dischargeDate"], 'Discharge date should be null')
    else:
        u.assert_and_log(str(result[0][0].DischargeDate) == payload["patientVisit"]["dischargeDate"],
                         'DB Results: ' + str(result[0][0].DischargeDate) + ' Expected: ' + payload["patientVisit"]["dischargeDate"])

    u.assert_and_log(result[0][0].PlaceOfService == payload["patientVisit"]["pos"],
                     'DB Results: ' + result[0][0].PlaceOfService + ' Expected: ' + payload["patientVisit"]["pos"])

    u.assert_and_log(str(result[0][0].PlaceOfServiceCode) == payload["patientVisit"]["posCode"],
                     'DB Results: ' + str(result[0][0].PlaceOfServiceCode) + ' Expected: ' + payload["patientVisit"]["posCode"])


def verify_episode_info(result, payload, engine_id, program_payer_id):

    episode_status_id = p.episode_status(payload["patientVisit"]["episodeStatus"])

    u.assert_and_log(result[0][0].ProgramPayerId == program_payer_id,
                     'DB Results: ' + str(result[0][0].ProgramPayerId) + ' Expected: ' + str(program_payer_id))

    u.assert_and_log(result[0][0].EpisodeStatusTypeId == episode_status_id,
                     'DB Results: ' + str(result[0][0].EpisodeStatusTypeId) + ' Expected: ' + str(episode_status_id))

    if payload["patientVisit"]["eligibilityEngineName"] == 'UserOverride':
        u.assert_and_log(result[0][0].IsManualEpisode == 1, 'This should be Manual episode')
        u.assert_and_log(result[0][0].EligibilityEngineId is None, 'Manual episode should not have eligibility engine')
    else:
        u.assert_and_log(result[0][0].IsManualEpisode == 0, 'This should not be Manual episode')
        u.assert_and_log(result[0][0].EligibilityEngineId == engine_id,
                         'DB Results: ' + str(result[0][0].EligibilityEngineId) + ' Expected: ' + str(engine_id))


def verify_bpcia_dismissal_answer(result):
    u.assert_and_log(result[0][0].QuestionKey == 'attending', 'Incorrect key. Actual:' + str(result[0][0].QuestionKey))
    u.assert_and_log(result[0][0].Answer == '0', 'Incorrect answer. Actual:' + str(result[0][0].Answer))
    u.assert_and_log(result[0][1].QuestionKey == 'diagnosis', 'Incorrect key. Actual:' + str(result[0][1].QuestionKey))
    u.assert_and_log(result[0][1].Answer == '0', 'Incorrect answer. Actual:' + str(result[0][1].Answer))
    u.assert_and_log(result[0][2].QuestionKey == 'exclusion', 'Incorrect key. Actual:' + str(result[0][2].QuestionKey))
    u.assert_and_log(result[0][2].Answer == '0', 'Incorrect answer. Actual:' + str(result[0][2].Answer))
    u.assert_and_log(result[0][3].QuestionKey == 'inpatient', 'Incorrect key. Actual:' + str(result[0][3].QuestionKey))
    u.assert_and_log(result[0][3].Answer == '0', 'Incorrect answer. Actual:' + str(result[0][3].Answer))
    u.assert_and_log(result[0][4].QuestionKey == 'membership', 'Incorrect key. Actual:' + str(result[0][4].QuestionKey))
    u.assert_and_log(result[0][4].Answer == '0', 'Incorrect answer. Actual:' + str(result[0][4].Answer))
    u.assert_and_log(result[0][5].QuestionKey == 'notes', 'Incorrect key. Actual:' + str(result[0][5].QuestionKey))
    u.assert_and_log(result[0][5].Answer == 'Testing note', 'Incorrect answer. Actual:' + str(result[0][5].Answer))


def verify_readmission_risk_survey_default_answer(j_res, answer_1, answer_2, answer_3, answer_4, answer_5, answer_6):
    element = j_res["pages"][0]["elements"]
    u.assert_and_log(element[0]["name"] == 'schedulePCPFollowUp', 'Incorrect key. Actual:' + element[0]["name"])
    u.assert_and_log(element[0]["defaultValue"] == answer_1, 'Incorrect answer. Actual:' + element[0]["defaultValue"])
    u.assert_and_log(element[1]["name"] == 'communicationToPCP', 'Incorrect key. Actual:' + element[1]["name"])
    u.assert_and_log(element[1]["defaultValue"] == answer_2, 'Incorrect answer. Actual:' + element[1]["defaultValue"])
    u.assert_and_log(element[2]["name"] == 'dischargeSummaryComplete', 'Incorrect key. Actual:' + element[2]["name"])
    u.assert_and_log(element[2]["defaultValue"] == answer_3, 'Incorrect answer. Actual:' + element[2]["defaultValue"])
    u.assert_and_log(element[3]["name"] == 'medReconciliation', 'Incorrect key. Actual:' + element[3]["name"])
    u.assert_and_log(element[3]["defaultValue"] == answer_4, 'Incorrect answer. Actual:' + element[3]["defaultValue"])
    u.assert_and_log(element[4]["name"] == 'medsToBed', 'Incorrect key. Actual:' + element[4]["name"])
    u.assert_and_log(element[4]["defaultValue"] == answer_5, 'Incorrect answer. Actual:' + element[4]["defaultValue"])
    u.assert_and_log(element[5]["name"] == 'teachBackDischargeInstructions', 'Incorrect key. Actual:' + element[5]["name"])
    u.assert_and_log(element[5]["defaultValue"] == answer_6, 'Incorrect answer. Actual:' + element[5]["defaultValue"])


def verify_readmission_risk_survey_questions(j_res):
    element = j_res["pages"][0]["elements"]
    u.assert_and_log(element[0]["title"] == 'A PCP appointment was scheduled within 7 days post discharge', 'Incorrect question. Actual:' + element[0]["title"])
    u.assert_and_log(element[1]["title"] == 'Communication to PCP at time of discharge', 'Incorrect question. Actual:' + element[1]["title"])
    u.assert_and_log(element[2]["title"] == 'Discharge summary complete at time of discharge', 'Incorrect question. Actual:' + element[2]["title"])
    u.assert_and_log(element[3]["title"] == 'Med reconciliation (physicians or pharmacists) with the pt prior to d/c', 'Incorrect question. Actual:' + element[3]["title"])
    u.assert_and_log(element[4]["title"] == 'Meds to Bed prior to discharge (highly encouraged if available)', 'Incorrect question. Actual:' + element[4]["title"])
    u.assert_and_log(element[5]["title"] == 'Teach-back d/c instructions by the provider to the patient/caregiver', 'Incorrect question. Actual:' + element[5]["title"])


def verify_save_readmission_risk_survey_answer(db_res, ans0, ans1, ans2, ans3, ans4, ans5):
    u.assert_and_log(db_res[0][0].QuestionKey == 'schedulePCPFollowUp', 'Incorrect key 0')
    u.assert_and_log(db_res[0][0].Answer == ans0, 'Incorrect answer 0')
    u.assert_and_log(db_res[0][1].QuestionKey == 'communicationToPCP', 'Incorrect key 1')
    u.assert_and_log(db_res[0][1].Answer == ans1, 'Incorrect answer 1')
    u.assert_and_log(db_res[0][2].QuestionKey == 'dischargeSummaryComplete', 'Incorrect key 2')
    u.assert_and_log(db_res[0][2].Answer == ans2, 'Incorrect answer 2')
    u.assert_and_log(db_res[0][3].QuestionKey == 'medReconciliation', 'Incorrect key 3')
    u.assert_and_log(db_res[0][3].Answer == ans3, 'Incorrect answer 3')
    u.assert_and_log(db_res[0][4].QuestionKey == 'medsToBed', 'Incorrect key 4')
    u.assert_and_log(db_res[0][4].Answer == ans4, 'Incorrect answer 4')
    u.assert_and_log(db_res[0][5].QuestionKey == 'teachBackDischargeInstructions', 'Incorrect key 5')
    u.assert_and_log(db_res[0][5].Answer == ans5, 'Incorrect answer 5')


def verify_pcp_appt_list(j_res, ext_visit_id):
    if j_res["items"].__len__() > 0:
        for x in range(0, j_res["items"].__len__()):
            if j_res["items"][x]["externalVisitId"] == str(ext_visit_id):
                return True
    return False


def verify_readmission_risk_survey_list(j_res, ext_visit_id):
    if j_res["items"].__len__() > 0:
        for x in range(0, j_res["items"].__len__()):
            if j_res["items"][x]["externalVisitId"] == str(ext_visit_id):
                return True
    return False


def is_item_existed(j_res, ext_visit_is):
    if j_res["items"].__len__() > 0:
        for x in range(0, j_res["items"].__len__()):
            if j_res["items"][x]["externalVisitId"] == str(ext_visit_is):
                return True, x
    return False, None


def verify_readmission_risk_survey_percent_complete(j_res, ext_visit_id, percent):
    found, index = is_item_existed(j_res, ext_visit_id)
    print(round(j_res["items"][index]["readmissionRiskSurveyPercentComplete"], 2))
    print(percent)
    u.assert_and_log(round(j_res["items"][index]["readmissionRiskSurveyPercentComplete"], 2) == percent, 'Correct percent should be: ' + str(percent))


def verify_hcc_list_detail(j_res, ext_visit_id, res_status, hcc_count, hcc_id, hcc_status_id, pre_diagn):
    found, index = is_item_existed(j_res, ext_visit_id)
    if index is not None:
        u.assert_and_log(j_res["items"][index]["responseStatus"] == res_status, 'Incorrect response status')
        u.assert_and_log(j_res["items"][index]["hccCount"] == hcc_count, 'Incorrect HCC count')
        u.assert_and_log( j_res["items"][index]["hccDetail"][0]["hccId"] == hcc_id, 'Incorrect HCC Id')
        u.assert_and_log(j_res["items"][index]["hccDetail"][0]["hccStatusId"] == hcc_status_id, 'Incorrect Hcc status Id')
        u.assert_and_log(j_res["items"][index]["hccDetail"][0]["previousDiagnosisText"] == pre_diagn, 'Incorrect previous diagnostic')


def verify_hcc_visit(j_res, MemberId, res_status, AttestationFinalized, hccId, hccDescription, hcc_statusId, pre_diagn):
    u.assert_and_log(j_res["data"]["MemberId"] == MemberId, '')
    u.assert_and_log(j_res["data"]["ResponseStatus"] == res_status, '')
    u.assert_and_log(j_res["data"]["AttestationFinalized"] == AttestationFinalized, '')
    u.assert_and_log(j_res["data"]["Hccs"][0]["hccId"] == hccId, '')
    u.assert_and_log(j_res["data"]["Hccs"][0]["hccDescription"] == hccDescription, '')
    u.assert_and_log(j_res["data"]["Hccs"][0]["statusId"] == hcc_statusId, '')
    u.assert_and_log(j_res["data"]["Hccs"][0]["previousDiagnosisText"] == pre_diagn, '')


def verify_key_value(j_res, key, value):

    if j_res["elements"].__len__() > 0:
        for x in range(0, j_res["elements"].__len__()):
            if j_res["elements"][x]["name"] == key and j_res["elements"][x]['defaultValue'] == value:
                return True
    return False


def verify_response_answer(db, question, answer):
    if db[0].__len__() > 0:
        for x in range(0, db[0].__len__()):
            if db[0][x].QuestionKey == question and db[0][x].Answer == answer:
                return True
    return False


def verify_avoidable_day(j_res):
    if j_res["data"].__len__() > 0:
        for x in range(0, j_res["data"].__len__()):
            if j_res["data"][x]["isAvoidable"] is True:
                return True
    return False


def verify_key_existed_in_dictionary(j_res, key):

    if j_res.__len__() > 0:
        if key in j_res:
            return True
    return False


def verify_next_possible_action(j_res, env, ext_visit_id, current_status):
    found = False
    candidate_url = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4cab-b7bd-8d257cf70dff/patientVisits/' + str(ext_visit_id) + '/episodeStatus/update/1?u=jebishop@soundphysicians.com'
    confirmed_url = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4cab-b7bd-8d257cf70dff/patientVisits/' + str(ext_visit_id) + '/episodeStatus/update/2?u=jebishop@soundphysicians.com'
    dismissed_url = 'https://php-' + env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4cab-b7bd-8d257cf70dff/patientVisits/' + str(ext_visit_id) + '/episodeStatus/update/5?u=jebishop@soundphysicians.com'

    found, index = is_item_existed(j_res, ext_visit_id)

    if current_status == 1:
        j_res['items'][index]['validStatusUpdates'][0] = confirmed_url
        j_res['items'][index]['validStatusUpdates'][1] = dismissed_url
    elif current_status == 2:
        j_res['items'][index]['validStatusUpdates'][0] = candidate_url
        j_res['items'][index]['validStatusUpdates'][1] = dismissed_url
    elif current_status == 5:
        verify_key_existed_in_dictionary(j_res['items'][index], 'validStatusUpdates')
    elif current_status == 7:
        verify_key_existed_in_dictionary(j_res['items'][index], 'validStatusUpdates')
    elif current_status == 8:
        verify_key_existed_in_dictionary(j_res['items'][index], 'validStatusUpdates')