from jsonschema import validate, Draft3Validator
from random import randint
import config
from sc_util import api_helper, target_process_helper as tp
import json
import requests as r


def validate_response_schema(response, schema):
    # Run the Draft Validator to capture any errors for reporting
    print('Response: ')
    print(response)
    v = Draft3Validator(schema)
    for error in sorted(v.iter_errors(response), key=str):
        string_error = str(error).replace("'", "")
        config.errors += str(string_error)

    # Run the validate to test the response vs schema
    validate(response, schema)


def execute_api_get(url, header):
    print('Request: ' + url)
    response, message = api_helper.api_get(url, header)
    assert response.status_code == 200, 'ERROR: Response code was ' + str(response.status_code)
    json_response = json.loads(response.content)
    return response, json_response


def execute_api_post_basic_auth(url, body, header):
    print('Request: ' + url)
    response, message = api_helper.api_post(url, body, header)
    json_response = json.loads(response.content)
    return response, json_response


def execute_api_post(url, body, header, auth=None):
    print('Request: ' + url)
    response, message = api_helper.api_post(url, body, header, auth)
    json_response = json.loads(response.content)
    return response, json_response


def execute_api_patch(url, body, header):
    print('Request: ' + url)
    response, message = api_helper.api_patch(url, body, header)
    json_response = json.loads(response.content)
    return response, json_response


def execute_api_delete(url, header):
    print('Request: ' + url)
    response, message = api_helper.api_delete(url, header)
    return response


def assert_and_log(assert_statement, error_message):
    if not assert_statement:
        config.errors += error_message
    assert assert_statement, error_message


def set_test_status(test_cases, test_case_name, child_test_plan_run_id, error, date):
    if error == "" or error is None:
        test_result = 'PASSED'
    else:
        test_result = 'FAILED'

    file = open("C:\\mobile_api_results\\results.txt", "a+")
    file.write(test_case_name + ' ' + test_result + '\n')

    tp.update_target_process(config.test_cases, config.test_case_name, config.child_test_plan_run_id, str(error))

def create_random_patient_name(first_name, last_name):
    rand = str(randint(10000, 99000))
    rand_first = first_name + '[' + rand + ']'
    rand_last = last_name + '[' + rand + ']'
    return rand_first, rand_last
