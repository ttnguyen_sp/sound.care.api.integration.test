import smtplib
import os
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate


def send_mail(send_from, send_to, subject, body, path, server="SEAQA2APP-1"):
    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = send_to
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(body, 'plain'))

    fileslist = os.listdir(path)
    reports = []

    for f in fileslist:
        reports.append(f)

    for f in reports:
        file_path = os.path.join(path, f)
        attachment = MIMEApplication(open(file_path, "rb").read(), _subtype="txt")
        attachment.add_header('Content-Disposition', 'attachment', filename=f)
        msg.attach(attachment)

    smtp = smtplib.SMTP(server)
    smtp.sendmail(msg["From"], msg["To"].split(","), msg.as_string())
    smtp.close()

    filelist = [f for f in os.listdir(path) if f.endswith(".html")]
    for f in filelist:
        os.remove(os.path.join(path, f))
