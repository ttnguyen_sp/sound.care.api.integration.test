from sc_util import api_helper
import json
import requests

token = 'MTM2OkI3NkYxQzhBMzlFQ0RFNEFDRUY0NkIxMkMyMzQyQzkx'
header = {'Content-Type': "application/json"}


def get_child_test_plan_run(test_plan_run_id, suite_name):
    uri = 'https://soundphysicians.tpondemand.com/api/v1/TestPlanRuns?where=(ParentTestPlanRun.Id%20eq%20' + \
          str(test_plan_run_id) + ')%20and%20(Name%20contains%20%27' + suite_name + \
          '%27%20%20)&take=1000&resultFormat=json&resultInclude=[Id,Name]&token=' + token
    api_response = api_helper.api_get(uri, header)
    data = json.loads(api_response[0].text)
    if len(data["Items"]) == 0:
        raise Exception('Invalid test plan run id or invalid suite name')
    else:
        return data["Items"][0]["Id"]


def get_test_case_run_id(test_plan_run_id, test_case_id):
    uri = 'https://soundphysicians.tpondemand.com/api/v1/TestCaseRuns?where=(TestPlanRun.Id%20eq%20' + \
          str(test_plan_run_id) + '%20)%20and%20(TestCase.Id%20eq%20' + \
          str(test_case_id) + ')&take=1000&resultFormat=json&token=' + token
    api_response = api_helper.api_get(uri, header)
    data = json.loads(api_response[0].text)
    if len(data["Items"]) == 0:
        return 0
    else:
        return data["Items"][0]["Id"]


def get_test_cases(tp_tag_name):
    uri = 'https://soundphysicians.tpondemand.com/api/v1/TestCases?Where=(Tags%20eq%20%27' + tp_tag_name + \
          '%27%20%20)&take=1000&resultFormat=json&resultInclude=[Id,Name]&token=' + token
    api_response = api_helper.api_get(uri, header)
    data = json.loads(api_response[0].text)
    if len(data["Items"]) == 0:
        raise Exception('Found no test case for the tag name: %s' % tp_tag_name)
    return data


def post_update_tp_test_case_status(test_case_run_id, test_case_run_status, comment):
    uri = 'https://soundphysicians.tpondemand.com/api/v1/TestCaseRuns/' + str(test_case_run_id) + '?token=' + token
    body = '{\"Status\": "%s", \"Comment\": "%s"}' % (test_case_run_status, comment)
    api_response = api_helper.api_post(uri, body, header, False)
    return api_response


def initialize_target_process(tp_tag_name, tp_run_id, suite_name, run_target_process):
    try:
        if run_target_process is True:
            tc_list = get_test_cases(tp_tag_name)
            child_test_run_id = get_child_test_plan_run(tp_run_id, suite_name)
            return tc_list, child_test_run_id
        else:
            return None, 0
    except Exception as e:
        raise e


def update_target_process(tc_list, tc_name, child_test_run_id, error):
    try:
        tc_id = get_test_case_id(tc_list, tc_name)
        tc_run_id = get_test_case_run_id(child_test_run_id, tc_id)
        if (tc_run_id is not None) and (error is ''):
            post_update_tp_test_case_status(tc_run_id, "Passed", "The automation test PASSED")
        elif (tc_run_id is not None) and ((error is not None) or (error is not '')):
            post_update_tp_test_case_status(tc_run_id, "Failed", error)
    except Exception as e:
        raise e


def get_test_case_id(tc_list, tc_name):
    for key in range(0, len(tc_list["Items"])):
        if tc_list["Items"][key]["Name"] == tc_name:
            return tc_list["Items"][key]["Id"]
    return None
