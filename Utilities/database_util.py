import pyodbc
from pyodbc import Error


def create_connection(connection):
    try:
        conn = pyodbc.connect(connection, autocommit=True, timeout=600)
        return conn
    except Error as e:
        print(e)

    return None


def execute_query(env, query, params):
    results = None
    column_names = None
    try:
        conn_string = get_connection_string(env)
        print(conn_string)
        conn = create_connection(conn_string)
        cursor = conn.cursor()

        if params is None:
            cursor.execute(query)
            results = cursor.fetchall()
        else:
            results = cursor.execute(query, params)
        column_names = [i[0] for i in cursor.description]
        conn.close()
    except Error as e:
        print(e)

    return results, column_names


def execute_query_command(env, query, params):
    try:
        conn_string = get_connection_string(env)
        conn = create_connection(conn_string)
        cursor = conn.cursor()
        if params:
            cursor.execute(query, params)
        else:
            cursor.execute(query)
        conn.close()
    except Error as e:
        print(e)

    return None


def execute_prince_query_command(env, query, params):
    results = None
    column_names = None
    server = 'tcp:php-' + env + '-sqlsvr01.database.windows.net'
    database = 'php-' + env + '-sqldb01'
    username = 'qDpRN85&wH`/uc3.'
    password = '8.Rck`TePXFd?`eE'
    driver = '{ODBC Driver 17 for SQL Server}'
    cnnx = 'DRIVER=' + driver + ';SERVER=' + server + ';PORT=1443;DATABASE=' + database + ';UID=' + username + ';PWD=' + password
    try:
        conn = pyodbc.connect(cnnx)
        cursor = conn.cursor()
        if params:
            cursor.execute(query, params)
        else:
            cursor.execute(query)
        cursor.commit()
        conn.close()
    except Error as e:
        print(e)

    return None


def execute_prince_query(env, query, params):
    results = None
    column_names = None
    server = 'tcp:php-' + env + '-sqlsvr01.database.windows.net'
    database = 'php-' + env + '-sqldb01'
    username = 'qDpRN85&wH`/uc3.'
    password = '8.Rck`TePXFd?`eE'
    driver = '{ODBC Driver 17 for SQL Server}'
    cnnx = 'DRIVER=' + driver + ';SERVER=' + server + ';PORT=1443;DATABASE=' + database + ';UID=' + username + ';PWD=' + password
    print(cnnx)
    try:
        conn = pyodbc.connect(cnnx)
        cursor = conn.cursor()

        if params is None:
            cursor.execute(query)
            results = cursor.fetchall()
        else:
            results = cursor.execute(query, params)
        column_names = [i[0] for i in cursor.description]
        conn.close()
    except Error as e:
        print(e)
    return results, column_names


def execute_mirth_connect_query(env, query, params):
    results = None
    column_names = None
    server = 'tcp:php-' + env + '-sqlsvr01.database.windows.net'
    database = 'gw2-' + env + '-sqldb01'
    username = 'qDpRN85&wH`/uc3.'
    password = '8.Rck`TePXFd?`eE'
    driver = '{ODBC Driver 17 for SQL Server}'
    cnnx = 'DRIVER=' + driver + ';SERVER=' + server + ';PORT=1443;DATABASE=' + database + ';UID=' + username + ';PWD=' + password
    print(cnnx)
    try:
        conn = pyodbc.connect(cnnx)
        cursor = conn.cursor()

        if params is None:
            cursor.execute(query)
            results = cursor.fetchall()
        else:
            results = cursor.execute(query, params)
        column_names = [i[0] for i in cursor.description]
        conn.close()
    except Error as e:
        print(e)
    return results, column_names


def execute_covered_lives_query(env, query, params):
    results = None
    column_names = None
    server = 'tcp:php-' + env + '-sqlsvr-coveredlives.database.windows.net'
    database = 'php-' + env + '-sqldb-coveredlives'
    username = 'qDpRN85&wH`/uc3.'
    password = '8.Rck`TePXFd?`eE'
    driver = '{ODBC Driver 17 for SQL Server}'
    cnnx = 'DRIVER=' + driver + ';SERVER=' + server + ';PORT=1443;DATABASE=' + database + ';UID=' + username + ';PWD=' + password
    print(cnnx)
    try:
        conn = pyodbc.connect(cnnx)
        cursor = conn.cursor()

        if params is None:
            cursor.execute(query)
            results = cursor.fetchall()
        else:
            results = cursor.execute(query, params)
        column_names = [i[0] for i in cursor.description]
        conn.close()
    except Error as e:
        print(e)
    return results, column_names


def get_connection_string(env):
    conn_string = None
    if env == 'qa':
        conn_string = 'DRIVER={SQL Server};SERVER=QA-SC-DB.sound.local;DATABASE=CCIS'
    elif env == 'int':
        conn_string = 'DRIVER={SQL Server};SERVER=intsc.sound.local;DATABASE=CCIS'
    elif env == 'uat':
        conn_string = 'DRIVER={SQL Server};SERVER=dv-wrk-4.sound.local;DATABASE=CCIS'
    elif env == 'business':
        conn_string = 'DRIVER={SQL Server};SERVER=SEAPRODDB-1.sound.local;DATABASE=CCIS'
    elif env == 'prod':
        conn_string = 'DRIVER={SQL Server};SERVER=SEAPRODDB-1.sound.local;DATABASE=CCIS'

    return conn_string


# -------------------------------------------------------------------------------------------------------------------



