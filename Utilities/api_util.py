import requests


def api_post(url, post_body, headers, ssl_verify=True):
    response = None
    message = None
    print(url)
    try:
        response = requests.post(url, data=post_body, headers=headers, verify=ssl_verify)
        # print response.status_code
        if response.status_code != 200:
            message = ' Response from url ' + url + 'was ' + str(response.status_code)
        else:
            print(response.content)
    except requests.RequestException:
        print(' Request failed. Received error code: ', requests.RequestException)
        message = requests.RequestException

    return response, message


def api_get(url,  headers):
    response = None
    message = None
    print(url)
    try:
        response = requests.get(url,  headers=headers)
        # print response.status_code
        if response.status_code != 200:
            message = ' Response from url ' + url + 'was ' + str(response.status_code)
        else:
            print(response.content)
    except requests.RequestException:
        print(' Request failed. Received error code: ', requests.RequestException)
        message = requests.RequestException

    return response, message


def api_patch(url, post_body, headers, ssl_verify=True):
    response = None
    message = None
    try:
        response = requests.patch(url, data=post_body, headers=headers, verify=ssl_verify)
        # print response.status_code
        if response.status_code != 200:
            message = ' Response from url ' + url + 'was ' + str(response.status_code)
    except requests.RequestException:
        print(' Request failed. Received error code: ', requests.RequestException)
        message = requests.RequestException

    return response, message


def api_delete(url, headers, post_body=None, ssl_verify=True):
    response = None
    message = None
    try:
        if post_body:
            response = requests.delete(url, headers=headers, data=post_body, verify=ssl_verify)
        else:
            response = requests.delete(url, headers=headers, verify=ssl_verify)

        if response.status_code != 200:
            message = ' Response from url ' + url + 'was ' + str(response.status_code)
    except requests.RequestException:
        print(' Request failed. Received error code: ', requests.RequestException)
        message = requests.RequestException

    return response, message


def api_put(url, post_body, headers, ssl_verify=True):
    response = None
    message = None
    try:
        response = requests.put(url, data=post_body, headers=headers, verify=ssl_verify)
        # print response.status_code
        if response.status_code != 200:
            message = ' Response from url ' + url + 'was ' + str(response.status_code)
    except requests.RequestException:
        print(' Request failed. Received error code: ', requests.RequestException)
        message = requests.RequestException

    return response, message
