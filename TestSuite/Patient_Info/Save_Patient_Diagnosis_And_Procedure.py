import unittest
import config as c
from EndPoints import soundcare
from datetime import date, timedelta
from Utilities import target_process_util as th
from Helpers import file_helper as f, ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
import json as j
from ddt import ddt, data


c.suite_name = 'Create Episode'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 86482
c.run_target_process = False
acute_site_id = 418

# ('9944916', '9962988', '9957389', '9933635', '9969652', '9920956', '9951344')
@ddt
class SavePatientDiagnosisAndProcedure(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia', 'Patient', '1', 'Admit'], '', """"""),
        d.annotated2(['Bpcia', 'Patient', '1', 'Update'], '', """"""),
        d.annotated2(['Bpcia', 'Patient', '1', 'Discharge'], '', """"""),
        d.annotated2(['Bpcia', 'Patient', '0', 'Admit'], '', """"""),
        d.annotated2(['Bpcia', 'Patient', '0', 'Update'], '', """"""),
        d.annotated2(['Bpcia', 'Patient', '0', 'Discharge'], '', """"""),
        d.annotated2(['WellMed', 'Patient', '1', 'Admit'], '', """"""),
        d.annotated2(['WellMed', 'Patient', '1', 'Update'], '', """"""),
        d.annotated2(['WellMed', 'Patient', '1', 'Discharge'], '', """"""),
        d.annotated2(['WellMed', 'Patient', '0', 'Admit'], '', """"""),
        d.annotated2(['WellMed', 'Patient', '0', 'Update'], '', """"""),
        d.annotated2(['WellMed', 'Patient', '0', 'Discharge'], '', """"""),
        d.annotated2(['Humana', 'Patient', '1', 'Admit'], '', """"""),
        d.annotated2(['Humana', 'Patient', '1', 'Update'], '', """"""),
        d.annotated2(['Humana', 'Patient', '1', 'Discharge'], '', """"""),
        d.annotated2(['Humana', 'Patient', '0', 'Admit'], '', """"""),
        d.annotated2(['Humana', 'Patient', '0', 'Update'], '', """"""),
        d.annotated2(['Humana', 'Patient', '0', 'Discharge'], '', """"""),
    )
    def test_create_episode(self, values):
        program, f_name, is_new_patient, message_type = values

        c.test_case_name = 'Verify if ' + getattr(values, "__name__") + ' episode is successfully created in SoundCare'
        print(c.test_case_name)

        discharge_date = str(date.today() - timedelta(days=6))
        status = 'Confirmed: Anchor Stay'
        payload = f.get_payload_data('.\\..\\..\\PayloadData\\Patient_payload.txt')
        e.update_header(payload, message_type)

        e_id, e_name, p_id, p_name = e.get_program_info(program)
        ext_visit_id = e.generate_visit_number()
        print(ext_visit_id)

        e.update_patient_info(payload, f_name, program, '1951-10-11')

        if is_new_patient == 0:
            p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)

        if message_type == 'Discharge':
            e.update_patient_visit(payload, ext_visit_id, e_id, e_name, p_id, p_name, status, discharge_date)
        elif message_type == 'Admit' or message_type == 'Update':
            e.update_patient_visit(payload, ext_visit_id, e_id, e_name, p_id, p_name, status)


        e.update_acute_site(payload, '', 'Sandbox Hospital 3.0', '383')

        episode_status_id = e.episode_status(payload["patientVisit"]["episodeStatus"])

        body = j.dumps(payload)

        resp = soundcare.send_sc_message(body, c.env)
        u.assert_and_log(resp.status_code == 200, 'Failed to send message - Actual code: ' + str(resp.status_code))
        '''
        result = db.get_episode(visit_id)
        u.assert_and_log(result is not None, 'Failed to create ' + program + ' episode')
        if result is not None:
            e.verify_episode_info(result, payload, engine_id, pr_payer_id)
            e.verify_patient_visit_info(result, payload)
        #db.delete_episode_records(visit_id, result[0][0].EpisodeId)
        '''
# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

