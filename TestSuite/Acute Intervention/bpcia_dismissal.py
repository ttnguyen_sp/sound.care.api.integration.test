import unittest
import config as c
from EndPoints import soundcare
from Utilities import target_process_util as th
#from Helpers import file_helper as f, ddt_helper as d, db_helper as db, utilities as u, episode_management_helper as e, verification_helper as v
from Helpers import ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
import json as j
from ddt import ddt, data, unpack


c.suite_name = 'BPCIa Dismissal'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 91602
c.run_target_process = False

acute_site_id = 418  # Sandbox Hospital 3.0
token = soundcare.request_token(c.env)
header = {'Content-Type': "application/json", 'Authorization': 'Bearer ' + token}

@ddt
class BpciaDismissal(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia', 'Patient', '1951-10-11', 'Bpcia',  3,  1], 'Verify get/save dismissal survey APIs for BPCIA episode are working properly', """"""),
        d.annotated2(['Manual', 'Patient', '1951-10-11', 'Manual', 3, 1], 'Verify get/save dismissal survey APIs for UserOverride episode are working properly', """"""),
        #d.annotated2(['Manual', 'Patient', '1951-10-11', 'Manual', 3, 1], 'Verify once dismissal survey is saved, it is no longer available to edit', """"""),
    )
    def test_bpcia_dismissal(self, values):
        l_name, f_name, dob, episode_type, engine_id, pr_payer_id = values
        ext_visit_id = e.generate_visit_number()

        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)
        db.execute_upd_episode_precedence_uipdate(p_visit_id)
        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)
        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

        get_survey_res = soundcare.get_dismissal_survey(c.env, ext_visit_id, header)
        u.assert_and_log(get_survey_res.status_code == 200, 'Failed to get survey - Actual code: ' + str(get_survey_res.status_code))
        save_survey_res = soundcare.save_dismissal_survey(c.env, ext_visit_id, header)
        u.assert_and_log(save_survey_res.status_code == 200, 'Failed to save survey - Actual code: ' + str(save_survey_res.status_code))

        db_result = db.get_bpcia_dismissal_answer(ext_visit_id)
        v.verify_bpcia_dismissal_answer(db_result)

        #db.delete_intervention_survey_records(p_visit_id, 8)
        #db.delete_episode_records(ext_visit_id, episode_id)


# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)
