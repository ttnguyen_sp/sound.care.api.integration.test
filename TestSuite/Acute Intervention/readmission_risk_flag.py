import unittest
from datetime import date, timedelta
import config as c
from EndPoints import soundcare
from Utilities import target_process_util as th
#from Helpers import ddt_helper as d, db_helper as db, utilities as u, episode_management_helper as e, verification_helper as v
from Helpers import ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
from ddt import ddt, data, unpack


c.suite_name = 'Readmission Risk Flag'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 87643
c.run_target_process = True


acute_site_id = 418  # Sandbox Hospital 3.0
token = soundcare.request_token(c.env)
header = {'Content-Type': "application/json", 'Authorization': 'Bearer ' + token}

@ddt
class ReadmissionRiskFlag(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia', 12, True,  'y'], 'Verify readmission risk flag api return True, if readmission risk score meets the threshold', """"""),
        d.annotated2(['Bpcia', 11, False, 'y'], 'Verify readmission risk flag api return False, if readmission risk score is below the threshold', """"""),
        d.annotated2(['Bpcia', 0 , False, 'y'], 'Verify readmission risk flag api return False, if readmission risk score is NULL', """"""),
        d.annotated2(['Bpcia', 12, False, 'n'], 'Verify readmission risk flag api return "Not Found", for invalid external visit Id', """"""),
        d.annotated2(['Uhc'  , 12, False, 'y'], 'Verify readmission risk flag api return False, if intervention assignment is not enable for a facility' , """"""),
    )
    def test_readmission_risk_flag(self, values):
        episode_type, score, expected, is_valid_id = values

        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)
        db.execute_upd_episode_precedence_uipdate(p_visit_id)
        if score > 0:
            db.update_readmission_risk_score(p_visit_id, score)

        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

        if is_valid_id == 'n':
            ext_visit_id = 1110001

        res, j_res = soundcare.get_readmission_risk_flag(c.env, ext_visit_id, header)

        if is_valid_id == 'n':
            u.assert_and_log(res.status_code == 404, 'Failed to get survey - Actual code: ' + str(res.status_code))
            u.assert_and_log(j_res == 'ExternalVisitId not found', 'Incorrect value return')

        elif is_valid_id == 'y':
            u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))
            u.assert_and_log(j_res is expected, 'Incorrect value return')

        db.delete_intervention_survey_records(p_visit_id, 8)
        db.delete_episode_records(ext_visit_id, episode_id)

# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)
