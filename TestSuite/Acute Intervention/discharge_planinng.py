import unittest
from datetime import date, timedelta
import config as c
from EndPoints import soundcare
from Utilities import target_process_util as th
# from Helpers import ddt_helper as d, db_helper as db, utilities as u, episode_management_helper as e, verification_helper as v
from Helpers import ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
from ddt import ddt, data, unpack


c.suite_name = 'Discharge Planning'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 91602
c.run_target_process = False


acute_site_id = 418  # Sandbox Hospital 3.0
token = soundcare.request_token(c.env)
header = {'Content-Type': "application/json", 'Authorization': 'Bearer ' + token}

@ddt
class DischargePlanning(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    def create_discharge_planning_record(self, patient_visit_id):
        response_id = db.insert_intervention_response(patient_visit_id, 16)
        db.insert_intervention_response_answer(response_id, 'actualNSOC', '30')
        db.insert_intervention_response_answer(response_id, 'caseManagementDesiredNSOC', '41')
        db.insert_intervention_response_answer(response_id, 'goalsOfCareDiscussedOrAdvCarePlanningCompleted', '0')
        db.insert_intervention_response_answer(response_id, 'hospiceConsultOrdered', '0')
        db.insert_intervention_response_answer(response_id, 'isFocusPatient', '1')
        db.insert_intervention_response_answer(response_id, 'noChangeToPlanOfCareReason', '2')
        db.insert_intervention_response_answer(response_id, 'palliativeCareConsultOrdered', '0')
        db.insert_intervention_response_answer(response_id, 'patientDesiredNSOC', '5')
        db.insert_intervention_response_answer(response_id, 'providerRecommendedNSOC', '30')
        db.insert_intervention_response_answer(response_id, 'surpriseQuestion', '0')

    def verify_db_result(self, db_res):
        u.assert_and_log(v.verify_response_answer(db_res, 'actualNSOC', '30') is True, 'error - actualNSOC')
        u.assert_and_log(v.verify_response_answer(db_res, 'caseManagementDesiredNSOC', '41') is True, 'error - caseManagementDesiredNSOC')
        u.assert_and_log(v.verify_response_answer(db_res, 'goalsOfCareDiscussedOrAdvCarePlanningCompleted', '0') is True, 'error - goalsOfCareDiscussedOrAdvCarePlanningCompleted')
        u.assert_and_log(v.verify_response_answer(db_res, 'hospiceConsultOrdered', '0') is True, 'error - hospiceConsultOrdered')
        u.assert_and_log(v.verify_response_answer(db_res, 'isFocusPatient', '1') is True, 'error - isFocusPatient')
        u.assert_and_log(v.verify_response_answer(db_res, 'noChangeToPlanOfCareReason', '2') is True, 'error - noChangeToPlanOfCareReason')
        u.assert_and_log(v.verify_response_answer(db_res, 'palliativeCareConsultOrdered', '0') is True, 'error - palliativeCareConsultOrdered')
        u.assert_and_log(v.verify_response_answer(db_res, 'patientDesiredNSOC', '5') is True, 'error - patientDesiredNSOC')
        u.assert_and_log(v.verify_response_answer(db_res, 'providerRecommendedNSOC', '30') is True, 'error - providerRecommendedNSOC')
        u.assert_and_log(v.verify_response_answer(db_res, 'surpriseQuestion', '0') is True, 'error - surpriseQuestion')

    def verify_default_values(self, items, ans1, ans2, ans3, ans4, ans5, ans6, ans7, ans8, ans9 ):
        u.assert_and_log(v.verify_key_value(items, 'surpriseQuestion', ans1) is True, 'Error - surpriseQuestion')
        u.assert_and_log(v.verify_key_value(items, 'goalsOfCareDiscussedOrAdvCarePlanningCompleted', ans2) is True, 'Error - goalsOfCareDiscussedOrAdvCarePlanningCompleted')
        u.assert_and_log(v.verify_key_value(items, 'palliativeCareConsultOrdered', ans3) is True, 'Error - palliativeCareConsultOrdered')
        u.assert_and_log(v.verify_key_value(items, 'hospiceConsultOrdered', ans4) is True, 'Error - hospiceConsultOrdered')
        u.assert_and_log(v.verify_key_value(items, 'noChangeToPlanOfCareReason', ans5) is True, 'Error - noChangeToPlanOfCareReason')
        u.assert_and_log(v.verify_key_value(items, 'patientDesiredNSOC', ans6) is True, 'Error - patientDesiredNSOC')
        u.assert_and_log(v.verify_key_value(items, 'providerRecommendedNSOC', ans7) is True, 'Error - providerRecommendedNSOC')
        u.assert_and_log(v.verify_key_value(items, 'caseManagementDesiredNSOC', ans8) is True, 'Error - caseManagementDesiredNSOC')
        u.assert_and_log(v.verify_key_value(items, 'actualNSOC', ans9) is True, 'Error - actualNSOC')

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia', 'true', '', '', '', '', '', '', '', ''], 'Verify get visit discharge planning', """"""),
    )
    def test_get_visit_discharge_planning(self, values):
        episode_type, a1, a2, a3, a4, a5, a6, a7, a8, a9 = values
        ext_acute_site_id = 383

        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)
        db.execute_upd_episode_precedence_uipdate(p_visit_id)
        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

        res, j_res = soundcare.get_visit_discharge_planning(c.env, ext_visit_id, header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))
        self.verify_default_values(j_res["pages"][0], a1, a2, a3, a4, a5, a6, a7, a8, a9)

        #db.delete_intervention_survey_records(p_visit_id, 16)
        #db.delete_episode_records(ext_visit_id, episode_id)

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia', 25, 'no', 'valid'], 'Verify save visit discharge planning',  """"""),
    )
    def test_save_discharge_planning(self, values):
        episode_type, score, survey_record_exist, ext_id = values

        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), 'Bpcia')

        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

        res, j_res = soundcare.save_visit_discharge_planning(c.env, ext_visit_id, header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))

        db_res = db.get_intervention_response_answers(p_visit_id, 16)
        self.verify_db_result(db_res)

        #db.delete_intervention_survey_records(p_visit_id, 16)
        #db.delete_episode_records(ext_visit_id, episode_id)


# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)
