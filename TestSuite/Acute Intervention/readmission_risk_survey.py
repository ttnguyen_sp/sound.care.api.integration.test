import unittest
import time
from datetime import date, timedelta
import config as c
from EndPoints import soundcare
from Utilities import target_process_util as th
# from Helpers import ddt_helper as d, db_helper as db, utilities as u, episode_management_helper as e, verification_helper as v
from Helpers import ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
from ddt import ddt, data, unpack


c.suite_name = 'Readmission Risk Survey'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 91602
c.run_target_process = False


acute_site_id = 418  # Sandbox Hospital 3.0
token = soundcare.request_token(c.env)
header = {'Content-Type': "application/json", 'Authorization': 'Bearer ' + token}

@ddt
class ReadmissionRiskSurvey(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia', 25, 'Discharged within 7 days', 1, 'valid'], 'Verify Get Readmission Risk List Api will return BPCIA patients that have been discharge within last 7 days', """"""),
        d.annotated2(['Bpcia', 25, 'Discharged over 7 days', 0, 'valid'], 'Verify Get Readmission Risk List Api will return BPCIA than have been discharge over 7 days', """"""),
        d.annotated2(['Bpcia', 11, 'Active', 0, 'valid'], 'Verify Get Readmission Risk List Api will not return BPCIA patients with readmission score is NULL', """"""),
        d.annotated2(['Bpcia', 12, 'Active', 1, 'valid'], 'Verify Get Readmission Risk List Api will return BPCIA patients with readmission score that meet the threshold', """"""),
        d.annotated2(['Bpcia', 12, 'Active', 0, 'invalid'], 'Verify Get Readmission Risk List Api will return proper response for invalid acute site id', """"""),
        d.annotated2(['Bpcia', 25, 'active', 1, 'valid'], 'Verify Get Readmission Risk List Api, Responses to questionnaire can be re-submitted', """allow re-submitted"""),
    )
    def test_get_readmission_risk_survey_list(self, values):
        episode_type, score, episode_state, include_in_survey_list, acute_site = values
        ext_acute_site_id = 383
        if acute_site == 'invalid':
            ext_acute_site_id = 999999

        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)
        db.execute_upd_episode_precedence_uipdate(p_visit_id)
        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

        if episode_state == 'Discharged within 7 days':
            db.update_discharge_date(ext_visit_id, date.today() - timedelta(days=3))

        elif episode_state == 'Discharged over 7 days':
            db.update_discharge_date(ext_visit_id, date.today() - timedelta(days=10))

        db.update_readmission_risk_score(p_visit_id, score)

        if getattr(values, "__doc__") == 'allow re-submitted':
            db.create_readmission_risk_survey_record(p_visit_id, '1', '1', '0', '0', '1', '1')

        res, j_res = soundcare.get_readmission_risk_survey_list(c.env, ext_acute_site_id, header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))

        found = v.verify_readmission_risk_survey_list(j_res, ext_visit_id)

        if include_in_survey_list == 0 or acute_site == 'invalid':
            u.assert_and_log(found is False, 'Should not be included in the pcp appt list')

        if include_in_survey_list == 1:
            u.assert_and_log(found is True, 'Should be included in the pcp appt list')

        #db.delete_intervention_survey_records(p_visit_id, 8)
        #db.delete_episode_records(ext_visit_id, episode_id)

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia', 25, 'no', 'valid'], 'Verify Get Readmission Risk Survey Api returns correct default values for a patient that has no intervention readmission risk record',  """"""),
        d.annotated2(['Bpcia', 25, 'yes', 'valid'], 'Verify Get Readmission Risk Survey Api returns with correct default values for a patient that already had intervention readmission risk record', """"""),
    )
    def test_get_readmission_risk_survey(self, values):
        episode_type, score, survey_record_exist, ext_id = values

        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)

        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

        db.update_readmission_risk_score(p_visit_id, score)

        if survey_record_exist == 'yes':
            db.create_readmission_risk_survey_record(p_visit_id, '1', '1', '0', '0', '1', '1')

        if ext_id == 'invalid':
            ext_visit_id = 99999999

        time.sleep(5)
        res, j_res = soundcare.get_readmission_risk_survey(c.env, ext_visit_id, header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))

        if survey_record_exist == 'yes':
            v.verify_readmission_risk_survey_default_answer(j_res, '1', 'true', 'false', 'false', '1', 'true')
        else:
            v.verify_readmission_risk_survey_questions(j_res)

        #db.delete_intervention_survey_records(p_visit_id, 8)
        #db.delete_episode_records(ext_visit_id, episode_id)
    
    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia', 25, 'no', 'valid'], 'Verify Save Readmission Risk survey Api will save correct survey info to SoundCare DB for a patient that has no intervention readmission risk record', """"""),
        d.annotated2(['Bpcia', 25, 'yes', 'valid'], 'Verify Save Readmission Risk survey Api will save correct survey info to SoundCare DB for a patient that already had intervention readmission risk record', """"""),
    )
    def test_save_readmission_risk_survey(self, values):
        episode_type, score, survey_record_exist, ext_id = values

        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)

        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

        if survey_record_exist == 'yes':
            db.create_readmission_risk_survey_record(p_visit_id, '1', '1', '0', '0', '1', '1')

        time.sleep(5)
        res, j_res = soundcare.save_readmission_risk_survey(c.env, ext_visit_id, header, '1', 'true', 'true', 'false', '1', 'false')
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))

        time.sleep(5)
        res1, j_res1 = soundcare.get_readmission_risk_survey(c.env, ext_visit_id, header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))

        time.sleep(5)
        v.verify_readmission_risk_survey_default_answer(j_res1, '1', 'true', 'true', 'false', '1', 'false')

        #db.delete_intervention_survey_records(p_visit_id, 8)
        #db.delete_episode_records(ext_visit_id, episode_id)
    '''
    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia', 50, 0], 'Verify Get Readmission Risk List Api will return correct percent if 0 survey question completed', """"""),
        d.annotated2(['Bpcia', 50, 1], 'Verify Get Readmission Risk List Api will return correct percent if 1 survey question completed', """"""),
        d.annotated2(['Bpcia', 50, 2], 'Verify Get Readmission Risk List Api will return correct percent if 2 survey question completed', """"""),
        d.annotated2(['Bpcia', 50, 3], 'Verify Get Readmission Risk List Api will return correct percent if 3 survey question completed', """"""),
        d.annotated2(['Bpcia', 50, 4], 'Verify Get Readmission Risk List Api will return correct percent if 4 survey question completed', """"""),
        d.annotated2(['Bpcia', 50, 5], 'Verify Get Readmission Risk List Api will return correct percent if 5 survey question completed', """"""),
        d.annotated2(['Bpcia', 50, 6], 'Verify Get Readmission Risk List Api will return correct percent if 6 survey question completed', """"""),
    )
    def test_readmission_risk_survey_percent_complete(self, values):
        episode_type, score, question_complete = values
        ext_acute_site_id = 383

        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)

        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

        db.update_readmission_risk_score(p_visit_id, score)

        response_id = db.create_readmission_risk_survey_record(p_visit_id, '1', 'true', 'false', 'false', '1', 'true')
        time.sleep(5)

        if question_complete == 0:
            db.delete_intervention_response_answer(response_id, 'schedulePCPFollowUp')
            db.delete_intervention_response_answer(response_id, 'communicationToPCP')
            db.delete_intervention_response_answer(response_id, 'dischargeSummaryComplete')
            db.delete_intervention_response_answer(response_id, 'medReconciliation')
            db.delete_intervention_response_answer(response_id, 'medsToBed')
            db.delete_intervention_response_answer(response_id, 'teachBackDischargeInstructions')
            res, j_res = soundcare.get_readmission_risk_survey_list(c.env, ext_acute_site_id, header)
            u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))

        if question_complete == 1:
            db.delete_intervention_response_answer(response_id, 'communicationToPCP')
            db.delete_intervention_response_answer(response_id, 'dischargeSummaryComplete')
            db.delete_intervention_response_answer(response_id, 'medReconciliation')
            db.delete_intervention_response_answer(response_id, 'medsToBed')
            db.delete_intervention_response_answer(response_id, 'teachBackDischargeInstructions')
            res, j_res = soundcare.get_readmission_risk_survey_list(c.env, ext_acute_site_id, header)
            u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))
            v.verify_readmission_risk_survey_percent_complete(j_res, ext_visit_id, 16.67)

        if question_complete == 2:
            db.delete_intervention_response_answer(response_id, 'dischargeSummaryComplete')
            db.delete_intervention_response_answer(response_id, 'medReconciliation')
            db.delete_intervention_response_answer(response_id, 'medsToBed')
            db.delete_intervention_response_answer(response_id, 'teachBackDischargeInstructions')
            res, j_res = soundcare.get_readmission_risk_survey_list(c.env, ext_acute_site_id, header)
            u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))
            v.verify_readmission_risk_survey_percent_complete(j_res, ext_visit_id, 33.33)

        if question_complete == 3:
            db.delete_intervention_response_answer(response_id, 'medReconciliation')
            db.delete_intervention_response_answer(response_id, 'medsToBed')
            db.delete_intervention_response_answer(response_id, 'teachBackDischargeInstructions')
            res, j_res = soundcare.get_readmission_risk_survey_list(c.env, ext_acute_site_id, header)
            u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))
            v.verify_readmission_risk_survey_percent_complete(j_res, ext_visit_id, 50.00)

        if question_complete == 4:
            db.delete_intervention_response_answer(response_id, 'medsToBed')
            db.delete_intervention_response_answer(response_id, 'teachBackDischargeInstructions')
            res, j_res = soundcare.get_readmission_risk_survey_list(c.env, ext_acute_site_id, header)
            u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))
            v.verify_readmission_risk_survey_percent_complete(j_res, ext_visit_id, 66.67)

        if question_complete == 5:
            db.delete_intervention_response_answer(response_id, 'teachBackDischargeInstructions')
            res, j_res = soundcare.get_readmission_risk_survey_list(c.env, ext_acute_site_id, header)
            u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))
            v.verify_readmission_risk_survey_percent_complete(j_res, ext_visit_id, 83.33)

        if question_complete == 6:
            res, j_res = soundcare.get_readmission_risk_survey_list(c.env, ext_acute_site_id, header)
            u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))
            v.verify_readmission_risk_survey_percent_complete(j_res, ext_visit_id, 100.00)

        db.delete_intervention_survey_records(p_visit_id, 8)
        db.delete_episode_records(ext_visit_id, episode_id)
    '''

# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)
