import unittest
import time
from datetime import date, timedelta
import config as c
from EndPoints import soundcare
from Utilities import target_process_util as th
# from Helpers import ddt_helper as d, db_helper as db, utilities as u, episode_management_helper as e, verification_helper as v
from Helpers import ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
from ddt import ddt, data, unpack


c.suite_name = 'Focus Patient'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 91602
c.run_target_process = False


ext_acute_site_id = 383  # Sandbox Hospital 3.0
acute_site_id = 418  # Sandbox Hospital 3.0
token = soundcare.request_token(c.env)
header = {'Content-Type': "application/json", 'Authorization': 'Bearer ' + token}

@ddt
class AvoidableDay(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    def create_avoidable_day_record(self, patient_visit_id):
        test = 'test'

    """
    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia', 25, 'Discharged within 7 days', 1, 'valid'], 'Verify get visit focus patient', """"""),
    )
    def test_get_avoidable_day7(self, values):
        episode_type, score, episode_state, include_in_survey_list, acute_site = values

        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)
        db.execute_upd_episode_precedence_uipdate(p_visit_id)
        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)
        day1 = date.today() - timedelta(days=2)
        day2 = date.today() - timedelta(days=1)
        db.create_patient_discharge_plan_record(p_visit_id)
        db.create_patient_avoidable_day_record(p_visit_id, day1, day2)

        res, j_res = soundcare.get_avoidable_day(c.env, ext_visit_id, header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))

        v.verify_avoidable_day(j_res)
        u.assert_and_log(v.verify_avoidable_day(j_res) is True, 'Could not find avoidable day')
        #db.delete_intervention_survey_records(p_visit_id, 32)
        #db.delete_episode_records(ext_visit_id, episode_id)

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia', 25, 'no', 'valid'], 'Verify save visit focus patient',  """"""),
    )
    def test_save_avoidable_day(self, values):
        episode_type, score, survey_record_exist, ext_id = values

        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)

        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)
        date1 = date.today() - timedelta(days=2)
        date2 = date.today() - timedelta(days=1)

        res, j_res = soundcare.save_avoidable_day(c.env, ext_visit_id, str(date1), str(date2), header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))

        db_res = db.get_avoidable_day(p_visit_id, 5)
        u.assert_and_log(db_res[0][0].VisitId == p_visit_id, 'Could not find avoidable record')
        u.assert_and_log(db_res[0][0].DateOfService == date1, 'Could not find avoidable record')
        u.assert_and_log(db_res[0][0].AvoidableDayTypeId == 11, 'Could not find avoidable record')

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia', 25, 'no', 'valid'], 'Verify save visit focus patient',  """"""),
    )
    def test_get_avoidable_day_summary(self, values):
        episode_type, score, survey_record_exist, ext_id = values

        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)

        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)
        date1 = date.today() - timedelta(days=2)
        date2 = date.today() - timedelta(days=1)

        date3 = date.today() - timedelta(days=5)
        date4 = date.today() - timedelta(days=0)

        res, j_res = soundcare.save_avoidable_day(c.env, ext_visit_id, str(date1), str(date2), header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))

        res1, j_res1 = soundcare.get_avoidable_day_summary(c.env, ext_acute_site_id, str(date3), str(date4), header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))
    """
    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia', 25, 'no', 'valid'], 'Verify save visit focus patient',  """"""),
    )
    def test_get_avoidable_day_roll_up(self, values):
        episode_type, score, survey_record_exist, ext_id = values

        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)

        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)
        date1 = date.today() - timedelta(days=2)
        date2 = date.today() - timedelta(days=1)

        date3 = date.today() - timedelta(days=5)
        date4 = date.today() - timedelta(days=0)

        res, j_res = soundcare.save_avoidable_day(c.env, ext_visit_id, str(date1), str(date2), header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))

        res1, j_res1 = soundcare.get_avoidable_day_roll_up(c.env, ext_acute_site_id, str(date3), str(date4), header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))

# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)
