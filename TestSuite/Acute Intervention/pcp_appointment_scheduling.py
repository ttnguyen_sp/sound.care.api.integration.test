import unittest
from datetime import date, timedelta
import config as c
from EndPoints import soundcare
from Utilities import target_process_util as th
#from Helpers import file_helper as f, ddt_helper as d, db_helper as db, utilities as u, episode_management_helper as e, verification_helper as v
from Helpers import ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
import json as j
from ddt import ddt, data, unpack


c.suite_name = 'PCP Appointment Scheduling'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 94252
c.run_target_process = False

acute_site_id = 418  # Sandbox Hospital 3.0
ext_acute_site_id = 383
token = soundcare.request_token(c.env)
header = {'Content-Type': "application/json", 'Authorization': 'Bearer ' + token}

@ddt
class PcpAppointmentScheduling(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))
    """
    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Aledade', 1, 'discharged within 14 days', 1], 'Verify the PCP Appt list will include Aledade patients that have been discharged within 14 days', """"""),
        d.annotated2(['Aledade', 1, 'discharged over 14 days', 0], 'Verify the PCP Appt list will not include Aledade patients that have been discharged more than 14 days', """"""),
        d.annotated2(['Uhc', 1, 'discharged within 7 days', 1], 'Verify the PCP Appt list will include patients that have been discharged within 7 days', """"""),
        d.annotated2(['Uhc', 1, 'discharged over 7 days', 0], 'Verify the PCP Appt list will not include patients that have been discharged more than 7 days', """"""),
        d.annotated2(['Uhc', 0, 'Active', 0], 'Verify the PCP Appt list will not include patients that have no NSOC', """"""),
        d.annotated2(['Aledade', 3, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - SNF - Short Term Care', """"""),
        d.annotated2(['Uhc', 62, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - ReHab', """"""),
        d.annotated2(['Aledade', 41, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - Pt Died', """"""),
        d.annotated2(['Uhc', 50, 'Active', 1], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - Hospice', """"""),
        d.annotated2(['Aledade', 70, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - Other Hospital', """"""),
        d.annotated2(['Uhc', 61, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - Swing', """"""),
        d.annotated2(['Aledade', 70, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - Other', """"""),
        d.annotated2(['Uhc', 7, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - AMA', """"""),
        d.annotated2(['Aledade', 62, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - Inpatient Rehab', """"""),
        d.annotated2(['Uhc', 63, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - Transfer to other Hospital or LTACH', """"""),
        d.annotated2(['Aledade', 7, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - Against Medical Advice', """"""),
        d.annotated2(['Uhc', 41, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - Expired', """"""),
        d.annotated2(['Aledade', 5, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - Another Acute Care Hospital', """"""),
        d.annotated2(['Uhc', 51, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - Hospice - Medical Facility', """"""),
        d.annotated2(['Aledade', 63, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - LTACH', """"""),
        d.annotated2(['Uhc', 63, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - Long Term Care', """"""),
        d.annotated2(['Aledade', 50, 'Active', 1], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - Hospice - Inpatient', """"""),
        d.annotated2(['Uhc', 65, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - Inpatient Psych', """"""),
        d.annotated2(['Aledade', 30, 'Active', 0], 'Verify the PCP Appt list will not include patients that have non-qualifying NSOC - Remains Inpatient/TRF of Service', """"""),
        d.annotated2(['Uhc', 1, 'Active', 1], 'Verify the PCP Appt list will include patients that have qualifying NSOC - Home - PCP', """"""),
        d.annotated2(['Aledade', 1, 'Active', 1], 'Verify the PCP Appt list will include patients that have qualifying NSOC - Home - Private Residence', """"""),
        d.annotated2(['Uhc', 50, 'Active', 1], 'Verify the PCP Appt list will include patients that have qualifying NSOC - Hospice - Private Residence', """"""),
        d.annotated2(['Aledade', 1, 'Active', 1], 'Verify the PCP Appt list will include patients that have qualifying NSOC - HH - Private Residence', """"""),
        d.annotated2(['Uhc', 6, 'Active', 1], 'Verify the PCP Appt list will include patients that have qualifying NSOC - HH - Residential Facility', """"""),
        d.annotated2(['Aledade', 84, 'Active', 1], 'Verify the PCP Appt list will include patients that have qualifying NSOC - Home - Residential Facility', """""")
    )
    def test_get_pcp_appt_list(self, values):
        episode_type, nsoc_type, episode_state, include_in_pcp_appt_list = values

        test = date.today() - timedelta(days=1)
        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)
        db.execute_upd_episode_precedence_uipdate(p_visit_id)

        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

        if episode_state == 'discharged within 7 days':
            db.update_discharge_date(ext_visit_id, date.today() - timedelta(days=5))

        elif episode_state == 'discharged over 7 days':
            db.update_discharge_date(ext_visit_id, date.today() - timedelta(days=7))

        elif episode_state == 'discharged within 14 days':
            db.update_discharge_date(ext_visit_id, date.today() - timedelta(days=13))

        elif episode_state == 'discharged over 14 days':
            db.update_discharge_date(ext_visit_id, date.today() - timedelta(days=15))

        db.update_post_acute_care_type(nsoc_type, p_visit_id)

        res, j_res = soundcare.get_pcp_appointment_list(c.env, ext_acute_site_id, header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))

        found = v.verify_pcp_appt_list(j_res, ext_visit_id)

        if include_in_pcp_appt_list == 0:
            u.assert_and_log(found is False, 'Should not be included in the pcp appt list')

        if include_in_pcp_appt_list == 1:
            u.assert_and_log(found is True, 'Should be included in the pcp appt list')

        db.delete_intervention_survey_records(p_visit_id, 8)
        db.delete_episode_records(ext_visit_id, episode_id)
        #res1, j_res1 = soundcare.get_pcp_survey(c.env, 8144773, header)
        #res2, j_res2 = soundcare.save_pcp_survey(c.env, 8144773, header, 1, '2020-08-25', 'test', 'Test, Pcp')

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Aledade', 1,  1, 'appt before discharged',   99], 'Verify that "Reschedule" is flaged if appt date before discharge date', """"""),
        d.annotated2(['Aledade', 1,  1, 'appt same day discharged', 99], 'Verify that "Reschedule" is flaged if appt was made and patient is still in house same day', """"""),
        d.annotated2(['Aledade', 3,  1, 'appt before discharged',   98], 'Verify that "Cancel status" if appt date before discharge date and patient NSOC changed to non-qualifying - ReHab', """"""),
        d.annotated2(['Aledade', 2,  1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - SNF - Short Term Care', """"""),
        d.annotated2(['Aledade', 3,  1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - ReHab', """"""),
        d.annotated2(['Aledade', 4,  1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - Pt Died', """"""),
        d.annotated2(['Aledade', 5,  1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - Hospice', """"""),
        d.annotated2(['Aledade', 7,  1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - Other Hospital', """"""),
        d.annotated2(['Aledade', 8,  1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - Swing', """"""),
        d.annotated2(['Aledade', 9,  1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - Other', """"""),
        d.annotated2(['Aledade', 10, 1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - AMA', """"""),
        d.annotated2(['Aledade', 13, 1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - Inpatient Rehab', """"""),
        d.annotated2(['Aledade', 14, 1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - Transfer to other Hospital or LTACH', """"""),
        d.annotated2(['Aledade', 15, 1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - Against Medical Advice', """"""),
        d.annotated2(['Aledade', 18, 1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - Expired', """"""),
        d.annotated2(['Aledade', 19, 1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - Another Acute Care Hospital', """"""),
        d.annotated2(['Aledade', 21, 1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - Hospice - Medical Facility', """"""),
        d.annotated2(['Aledade', 22, 1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - LTACH', """"""),
        d.annotated2(['Aledade', 24, 1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - Long Term Care', """"""),
        d.annotated2(['Aledade', 25, 1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - Hospice - Inpatient', """"""),
        d.annotated2(['Aledade', 26, 1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - Inpatient Psych', """"""),
        d.annotated2(['Aledade', 27, 1, 'appt after discharged',    98], 'Verify that "Cancel status" is flaged if appt date after discharge date and patient NSOC changed to non-qualifying - Remains Inpatient/TRF of Service', """"""),
        d.annotated2(['Aledade', 1,  1, 'appt after discharged',     1], 'Verify that proper status returned if Appt after discharge date and patient NSOC changed to qualifying - Home - PCP', """"""),
        d.annotated2(['Aledade', 11, 1, 'appt after discharged',    11], 'Verify that proper status returned if Appt after discharge date and patient NSOC changed to qualifying - Home - Private Residence', """"""),
        d.annotated2(['Aledade', 12, 1, 'appt after discharged',    12], 'Verify that proper status returned if Appt after discharge date and patient NSOC changed to qualifying - Hospice - Private Residence', """"""),
        d.annotated2(['Aledade', 16, 1, 'appt after discharged',    16], 'Verify that proper status returned if Appt after discharge date and patient NSOC changed to qualifying - HH - Private Residence', """"""),
        d.annotated2(['Aledade', 20, 1, 'appt after discharged',    20], 'Verify that proper status returned if Appt after discharge date and patient NSOC changed to qualifying - HH - Residential Facility', """"""),
        d.annotated2(['Aledade', 23, 1, 'appt after discharged',    23], 'Verify that proper status returned if Appt after discharge date and patient NSOC changed to qualifying - Home - Residential Facility', """"""),
    )
    def test_get_pcp_survey(self, values):
        episode_type, nsoc_type, appt_status, condition, expected_status = values

        test = date.today() - timedelta(days=1)
        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)

        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)
        db.execute_upd_episode_precedence_uipdate(p_visit_id)

        db.update_post_acute_care_type(nsoc_type, ext_visit_id)

        if condition == 'appt after discharged' and nsoc_type in (1, 6, 84):
            db.create_pcp_appt_record(p_visit_id, date.today() + timedelta(days=4), appt_status)
            db.update_discharge_date(ext_visit_id, date.today() - timedelta(days=1))

        if condition == 'appt before discharged' and nsoc_type in (1, 6, 84):

            db.create_pcp_appt_record(p_visit_id, date.today() - timedelta(days=5), appt_status)
            db.update_discharge_date(ext_visit_id, date.today() - timedelta(days=1))

        if condition == 'appt same day discharged' and nsoc_type in (1, 6, 84):
            db.create_pcp_appt_record(p_visit_id, date.today() - timedelta(days=1), appt_status)
            db.update_discharge_date(ext_visit_id, date.today() - timedelta(days=1))

        if condition == 'appt before discharged' and nsoc_type not in (1, 6, 84):
            db.create_pcp_appt_record(p_visit_id, date.today() - timedelta(days=5), appt_status)
            db.update_discharge_date(ext_visit_id, date.today() - timedelta(days=1))

        if condition == 'appt after discharged' and nsoc_type not in (1, 6, 84):
            db.create_pcp_appt_record(p_visit_id, date.today() + timedelta(days=5), appt_status)
            db.update_discharge_date(ext_visit_id, date.today() - timedelta(days=1))

        res, j_res = soundcare.get_pcp_survey(c.env, ext_visit_id, header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))
        # Verify the default status
        # Verify pcp/note/appt date
        db.delete_episode_records(ext_visit_id, episode_id)
    """
    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Aledade', 1,  1, 'appt before discharged',   99], 'Verify that pcp Appt Info is successfully saved to SoCare DB', """"""),
    )
    def test_save_pcp_survey(self, values):
        episode_type, nsoc_type, appt_status, condition, expected_status = values

        test = date.today() - timedelta(days=1)
        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)
        db.execute_upd_episode_precedence_uipdate(p_visit_id)
        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

        res, j_res = soundcare.save_pcp_survey(c.env, ext_visit_id, header, appt_status, '2020-08-25', 'note', 'pcp')
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))
        db_res = db.get_intervention_response_answers(p_visit_id, 1)
        u.assert_and_log(db_res is not None, 'Should save intervention record')
        db.delete_episode_records(ext_visit_id, episode_id)

# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)