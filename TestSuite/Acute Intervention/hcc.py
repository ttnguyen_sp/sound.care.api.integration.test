import unittest
from random import *
from datetime import date, timedelta
import config as c
from EndPoints import soundcare
from Utilities import target_process_util as th
from Helpers import ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
from ddt import ddt, data, unpack


c.suite_name = 'PCP Appointment Scheduling'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 79712
c.run_target_process = False

acute_site_id = 418  # Sandbox Hospital 3.0
ext_acute_site_id = 383
token = soundcare.request_token(c.env)
header = {'Content-Type': "application/json", 'Authorization': 'Bearer ' + token}

@ddt
class PcpAppointmentScheduling(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))
    '''
    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2([1, 0,   1, 1, 'B-29', 1, False, 0], 'Verify the HCC will include all active UHC patients for a facility', """"""),
        d.annotated2([0, 0,   2, 1, 'B-29', 1, False, 0], 'Verify the HCC will not include dicharge UHC patients for a facility', """"""),
        d.annotated2([1, 0,  -1, 1, 'B-29', 1, False, 0], 'Verify the HCC will return correct info for new admit patient', """"""),
        d.annotated2([1, 85,  2, 3, 'B-29', 1, False, 1], 'Verify the HCC will return correct info for patient with existing hcc record', """"""),
        d.annotated2([1, 81,  1, 2, 'B-29', 1, True,  1], 'Verify the HCC will return correct info for patient already attestation finalized ', """"""),

    )
    def test_get_hcc_list(self, values):
        is_active, hccid, hcc_status, hcc_resp_status, prev_diagnosis, attestation, attestationfinalized, existing_hcc = values

        attestation_date_modified = date.today() - timedelta(days=1)
        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)
        ext_visit_id = e.generate_visit_number()
        member_id = '7892' + str(randint(10000, 99900))
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), 'Uhc')

        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

        #hccStatus = statusId: - 1 Incomplete / 1 = Recapture / 2 = Suspected / New
        #hccResponseStatus: 1 =	New Admit /	2 =	Response Pending / 3 = Response Received

        if existing_hcc == 1:
            db.create_hcc_record(p_visit_id, hcc_resp_status, member_id, attestationfinalized, hccid, hcc_status,
                                 prev_diagnosis, attestation)

        if is_active == 0:
            db.update_discharge_date(ext_visit_id, date.today() - timedelta(days=1))

        # Verify HCC List Api return correct status for new admit patient
        res, j_res = soundcare.get_hcc_list(c.env, ext_acute_site_id, token)
        found, index = v.is_item_existed(j_res, ext_visit_id)

        if is_active == 1:
            u.assert_and_log(found is True, 'Should be included in the HCC list')
        else:
            u.assert_and_log(found is False, 'Should not be included in the HCC list')

        #res1, j_res1 = soundcare.get_hcc_visit(c.env, 9136835, token)
        #v.verify_hcc_visit(j_res1)
        #v.verify_hcc_list_detail(j_res, 9136835)

        db.delete_episode_records(ext_visit_id, episode_id)

    '''

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Uhc', 1, 'active'   , 1], 'Verify the get hcc visit api will return correct info for newly admit patient', """"""),
        d.annotated2(['Uhc', 1, 'discharge', 0], 'Verify the get hcc visit api will return correct info for the patients that already have hcc details', """"""),
        d.annotated2(['Uhc', 1, 'discharge', 1], 'Verify the HCC will return correct info for new admit patient', """"""),
        d.annotated2(['Uhc', 1, 'discharge', 1], 'Verify the HCC will return correct info for patient with existing hcc record', """"""),
        d.annotated2(['Uhc', 1, 'discharge', 1], 'Verify the HCC will return correct info for patient already attestation finalized ', """"""),
    )
    def test_get_hcc_visit(self, values):
        episode_type, nsoc_type, episode_state, include_in_pcp_appt_list = values

        test = date.today() - timedelta(days=1)
        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)
        ext_visit_id = e.generate_visit_number()

        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), 'Uhc')

        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

        db.create_hcc_intervention_response_answer()

        # Verify HCC List Api return correct status for new admit patient
        res, j_res = soundcare.get_hcc_visit(c.env, 9136835, token)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))


        db.delete_episode_records(ext_visit_id, episode_id)

# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)