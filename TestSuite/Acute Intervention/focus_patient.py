import unittest
import time
from datetime import date, timedelta
import config as c
from EndPoints import soundcare
from Utilities import target_process_util as th
# from Helpers import ddt_helper as d, db_helper as db, utilities as u, episode_management_helper as e, verification_helper as v
from Helpers import ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
from ddt import ddt, data, unpack


c.suite_name = 'Focus Patient'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 91602
c.run_target_process = False


acute_site_id = 418  # Sandbox Hospital 3.0
token = soundcare.request_token(c.env)
header = {'Content-Type': "application/json", 'Authorization': 'Bearer ' + token}

@ddt
class FocusPatient(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    def create_discharge_planning_record(self, patient_visit_id):
        response_id = db.insert_intervention_response(patient_visit_id, 16)
        db.insert_intervention_response_answer(response_id, 'actualNSOC', '30')
        db.insert_intervention_response_answer(response_id, 'caseManagementDesiredNSOC', '41')
        db.insert_intervention_response_answer(response_id, 'goalsOfCareDiscussedOrAdvCarePlanningCompleted', '0')
        db.insert_intervention_response_answer(response_id, 'hospiceConsultOrdered', '0')
        db.insert_intervention_response_answer(response_id, 'isFocusPatient', '1')
        db.insert_intervention_response_answer(response_id, 'noChangeToPlanOfCareReason', '2')
        db.insert_intervention_response_answer(response_id, 'palliativeCareConsultOrdered', '0')
        db.insert_intervention_response_answer(response_id, 'patientDesiredNSOC', '5')
        db.insert_intervention_response_answer(response_id, 'providerRecommendedNSOC', '30')
        db.insert_intervention_response_answer(response_id, 'surpriseQuestion', '0')

    def verify_default_values(self, j_res):
        u.assert_and_log(v.verify_key_value(j_res, 'homeHealthAssessmentCompleted', '') is True, 'Error - homeHealthAssessmentCompleted')
        u.assert_and_log(v.verify_key_value(j_res, 'homeHealthAssessmentNotCompletedReason', '') is True, 'Error - homeHealthAssessmentNotCompletedReason')
        u.assert_and_log(v.verify_key_value(j_res, 'surpriseQuestion', 'true') is True, 'Error - surpriseQuestion')
        u.assert_and_log(v.verify_key_value(j_res, 'goalsOfCareDiscussedOrAdvCarePlanningCompleted', '') is True, 'Error - goalsOfCareDiscussedOrAdvCarePlanningCompleted')
        u.assert_and_log(v.verify_key_value(j_res, 'palliativeCareConsultOrdered', '') is True, 'Error - palliativeCareConsultOrdered')
        u.assert_and_log(v.verify_key_value(j_res, 'hospiceConsultOrdered', '') is True, 'Error - hospiceConsultOrdered')
        u.assert_and_log(v.verify_key_value(j_res, 'noChangeToPlanOfCareReason', '') is True, 'Error - noChangeToPlanOfCareReason')
        u.assert_and_log(v.verify_key_value(j_res, 'patientBaselineFunctionId', '') is True, 'Error - patientBaselineFunctionId')
        u.assert_and_log(v.verify_key_value(j_res, 'hasHelpAvailableHome', '') is True, 'Error - hasHelpAvailableHome')
        u.assert_and_log(v.verify_key_value(j_res, 'cpnRoundingComplete', '') is True, 'Error - cpnRoundingComplete')

    def verify_db_result(self, db_res):
        u.assert_and_log(v.verify_response_answer(db_res, 'cpnRoundingComplete', '0') is True, 'error - cpnRoundingComplete')
        u.assert_and_log(v.verify_response_answer(db_res, 'goalsOfCareDiscussedOrAdvCarePlanningCompleted', '0') is True, 'error - goalsOfCareDiscussedOrAdvCarePlanningCompleted')
        u.assert_and_log(v.verify_response_answer(db_res, 'hasHelpAvailableHome', '1') is True, 'error - hasHelpAvailableHome')
        u.assert_and_log(v.verify_response_answer(db_res, 'homeHealthAssessmentCompleted', '0') is True, 'error - homeHealthAssessmentCompleted')
        u.assert_and_log(v.verify_response_answer(db_res, 'homeHealthAssessmentNotCompletedReason', '9') is True, 'error - homeHealthAssessmentNotCompletedReason')
        u.assert_and_log(v.verify_response_answer(db_res, 'homeHealthAssessmentNotCompletedReasonNote', 'test') is True, 'error - homeHealthAssessmentNotCompletedReasonNote')
        u.assert_and_log(v.verify_response_answer(db_res, 'hospiceConsultOrdered', '0') is True, 'error - hospiceConsultOrdered')
        u.assert_and_log(v.verify_response_answer(db_res, 'noChangeToPlanOfCareReason', '9') is True, 'error - noChangeToPlanOfCareReason')
        u.assert_and_log(v.verify_response_answer(db_res, 'noChangeToPlanOfCareReasonNote', 'test') is True, 'error - noChangeToPlanOfCareReasonNote')
        u.assert_and_log(v.verify_response_answer(db_res, 'palliativeCareConsultOrdered', '0') is True, 'error - palliativeCareConsultOrdered')
        u.assert_and_log(v.verify_response_answer(db_res, 'patientBaselineFunctionId', '4') is True, 'error - patientBaselineFunctionId')
        u.assert_and_log(v.verify_response_answer(db_res, 'surpriseQuestion', '0') is True, 'error - surpriseQuestion')

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia', 25, 'Discharged within 7 days', 1, 'valid'], 'Verify get visit focus patient', """"""),
    )
    def test_get_visit_focus_patient(self, values):
        episode_type, score, episode_state, include_in_survey_list, acute_site = values
        ext_acute_site_id = 383

        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)
        db.execute_upd_episode_precedence_uipdate(p_visit_id)
        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

        self.create_discharge_planning_record(p_visit_id)

        res, j_res = soundcare.get_visit_focus_patient(c.env, ext_visit_id, header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))

        self.verify_default_values(j_res["pages"][0])

        #db.delete_intervention_survey_records(p_visit_id, 32)
        #db.delete_episode_records(ext_visit_id, episode_id)

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia', 25, 'no', 'valid'], 'Verify save visit focus patient',  """"""),
    )
    def test_save_visit_focus_patient(self, values):
        episode_type, score, survey_record_exist, ext_id = values

        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)

        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

        res, j_res = soundcare.save_visit_focus_patient(c.env, ext_visit_id, header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))

        time.sleep(10)
        db_res = db.get_intervention_response_answers(p_visit_id, 32)
        self.verify_db_result(db_res)

        #db.delete_intervention_survey_records(p_visit_id, 16)
        #db.delete_episode_records(ext_visit_id, episode_id)


# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)
