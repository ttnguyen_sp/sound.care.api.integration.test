import unittest
import time
import config as c
from EndPoints import soundcare
from datetime import date, timedelta
from Utilities import target_process_util as th
from Helpers import file_helper as f, ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
import json as j
from ddt import ddt, data, unpack


c.suite_name = 'Udf Find Episode'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 91602
c.run_target_process = True

acute_site_id = 418  # Sandbox Hospital 3.0
token = soundcare.request_token(c.env)
header = {'Content-Type': "application/json", 'Authorization': 'Bearer ' + token}

@ddt
class UdfFindEpisode(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    def create_episode_for_existing_patient_visit(self, patient_visit_id, episode_type, ext_visit_id):
        p_id, e_id = e.program_payer_id_and_eligibility_engine_id(episode_type)
        episode_id = db.insert_episode(p_id, e_id, ext_visit_id)
        db.insert_episode_patient_visit(patient_visit_id, episode_id)
        if episode_type == 'Manual':
            db.update_episode_patient_visit(episode_id, 2)
        return episode_id

    def remove_episode(self, episode_id):
        db.delete_episode_patient_visit(episode_id)
        db.delete_episode(episode_id)
        #db.ca(episode_id)

    def prepare_episode(self, patient_visit_id, ext_visit_id):
        ep_id = dict()
        ep_id["manual"] = self.create_episode_for_existing_patient_visit(patient_visit_id, 'Manual', ext_visit_id)
        ep_id["bpcia"] = self.create_episode_for_existing_patient_visit(patient_visit_id, 'Bpcia', ext_visit_id)
        ep_id["uhc"] = self.create_episode_for_existing_patient_visit(patient_visit_id, 'Uhc', ext_visit_id)
        ep_id["wellmed"] = self.create_episode_for_existing_patient_visit(patient_visit_id, 'WellMed', ext_visit_id)
        ep_id["prospect"] = self.create_episode_for_existing_patient_visit(patient_visit_id, 'Prospect', ext_visit_id)
        ep_id["christus"] = self.create_episode_for_existing_patient_visit(patient_visit_id, 'Christus', ext_visit_id)
        ep_id["mtcarmel"] = self.create_episode_for_existing_patient_visit(patient_visit_id, 'MtCarmel', ext_visit_id)
        ep_id["pcrozer"] = self.create_episode_for_existing_patient_visit(patient_visit_id, 'ProspectCrozer', ext_visit_id)
        ep_id["humana"] = self.create_episode_for_existing_patient_visit(patient_visit_id, 'Humana', ext_visit_id)
        ep_id["aledade"] = self.create_episode_for_existing_patient_visit(patient_visit_id, 'Aledade', ext_visit_id)
        ep_id["carolina"] = self.create_episode_for_existing_patient_visit(patient_visit_id, 'Carolina', ext_visit_id)

        return ep_id

    def delete_priority_episode(self, episode_id):
        db.delete_episode(episode_id)
        db.delete_episode_patient_visit(episode_id)

    def create_subsequent_episode(self, anchor_stay_id, subsequent_stay_id, status):
        first_name = e.generate_random_name('Patient')
        admit_date = date.today() - timedelta(days=6)

        anchor_p_visit_id, episode_id = db.create_episode_using_query_test(
            acute_site_id, anchor_stay_id, 'Bpcia', '1970-10-15', first_name, 'Arbor', admit_date)

        db.update_discharge_date(anchor_stay_id, date.today() - timedelta(days=2))

        subs_p_visit_id = db.insert_patient_visit(acute_site_id, subsequent_stay_id, first_name, 'Arbor', '1970-10-15', admit_date)
        db.insert_episode_patient_visit(subs_p_visit_id, episode_id)
        db.update_episode_status(episode_id, status)
        return anchor_p_visit_id, subs_p_visit_id, episode_id
    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    def test_udf_find_episode_for_site_tc1(self):
        c.test_case_name = 'Verify udf_find_episode_for_site will return the highest order episode with latest visit info'
        ext_visit_id = e.generate_visit_number()
        print(c.test_case_name)
        p_visit_id = db.insert_patient_visit(
            acute_site_id, ext_visit_id, e.generate_random_name('Patient'), 'Test', '1970-10-15', date.today() - timedelta(days=6))

        e_id = self.prepare_episode(p_visit_id, ext_visit_id)
        print(ext_visit_id)
        print(p_visit_id)
        print(e_id)

        time.sleep(5)
        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)

        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC', 'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["uhc"], 5)

        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA Humana',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["humana"], 8)

        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA WellMed',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["wellmed"], 5)

        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHACO Aledade',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["aledade"], 8)

        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'ACO Prospect',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["prospect"], 5)

        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'ACO - Prospect Crozer',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["pcrozer"], 8)

        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA Mt. Carmel',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["mtcarmel"], 5)

        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHACO Christus',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["christus"], 8)

        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHACO Coastal Carolina QC',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["carolina"], 5)

        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'BPCIA',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["manual"], 8)

        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'BPCIA',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["bpcia"], 5)

    def test_udf_find_episode_for_site_tc2(self):
        c.test_case_name = 'Verify udf_find_episode_for_site will not change priority order if hiding the episode'
        ext_visit_id = e.generate_visit_number()
        print(c.test_case_name)
        p_visit_id = db.insert_patient_visit(
            acute_site_id, ext_visit_id, e.generate_random_name('Patient'), 'Test', '1970-10-15', date.today() - timedelta(days=6))

        e_id = self.prepare_episode(p_visit_id, ext_visit_id)
        print(ext_visit_id)
        print(p_visit_id)
        print(e_id)

        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC', 'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_is_hidden_status(e_id["uhc"], 1)

        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)

    def test_udf_find_episode_for_site_tc3(self):
        c.test_case_name = 'Verify udf_find_episode_for_site will not change priority order if updating episode status to Candidate/Confirmed/Complete'
        ext_visit_id = e.generate_visit_number()
        print(c.test_case_name)
        p_visit_id = db.insert_patient_visit(
            acute_site_id, ext_visit_id, e.generate_random_name('Patient'), 'Test', '1970-10-15', date.today() - timedelta(days=6))

        e_id = self.prepare_episode(p_visit_id, ext_visit_id)
        print(ext_visit_id)
        print(p_visit_id)
        print(e_id)

        db.update_episode_status(e_id["uhc"], 1)
        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)

        db.update_episode_status(e_id["uhc"], 2)
        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)

        db.update_episode_status(e_id["uhc"], 7)
        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)

    def test_udf_find_episode_for_site_tc4(self):
        c.test_case_name = 'Verify udf_find_episode_for_site will slide precedence order to the next if updating episode status to dismissed'
        ext_visit_id = e.generate_visit_number()
        print(c.test_case_name)
        p_visit_id = db.insert_patient_visit(
            acute_site_id, ext_visit_id, e.generate_random_name('Patient'), 'Test', '1970-10-15', date.today() - timedelta(days=6))

        e_id = self.prepare_episode(p_visit_id, ext_visit_id)
        print(ext_visit_id)
        print(p_visit_id)
        print(e_id)

        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC', 'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["uhc"], 5)
        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA Humana',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)

    def test_udf_find_episode_for_site_tc5(self):
        c.test_case_name = 'Verify udf_find_episode_for_site will slide precedence order to the next if updating episode status to system dismissed'
        ext_visit_id = e.generate_visit_number()
        print(c.test_case_name)
        p_visit_id = db.insert_patient_visit(
            acute_site_id, ext_visit_id, e.generate_random_name('Patient'), 'Test', '1970-10-15', date.today() - timedelta(days=6))

        e_id = self.prepare_episode(p_visit_id, ext_visit_id)
        print(ext_visit_id)
        print(p_visit_id)
        print(e_id)

        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC', 'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["uhc"], 8)

        result = db.get_udf_find_episodes_for_site(1, 418, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA Humana',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    def test_udf_find_episode_for_visit_tc1(self):
        c.test_case_name = 'Verify udf_find_episode_for_visit will return the highest order episode with latest visit info'
        ext_visit_id = e.generate_visit_number()
        print(c.test_case_name)
        p_visit_id = db.insert_patient_visit(
            acute_site_id, ext_visit_id, e.generate_random_name('Patient'), 'Test', '1970-10-15', date.today() - timedelta(days=6))

        e_id = self.prepare_episode(p_visit_id, ext_visit_id)
        print(ext_visit_id)
        print(p_visit_id)
        print(e_id)

        time.sleep(5)
        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)

        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC', 'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["uhc"], 5)

        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA Humana',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["humana"], 8)

        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA WellMed',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["wellmed"], 5)

        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHACO Aledade',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["aledade"], 8)

        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'ACO Prospect',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["prospect"], 5)

        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'ACO - Prospect Crozer',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["pcrozer"], 8)

        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA Mt. Carmel',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["mtcarmel"], 5)

        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHACO Christus',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["christus"], 8)

        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHACO Coastal Carolina QC',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["carolina"], 5)

        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'BPCIA',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["manual"], 8)

        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'BPCIA',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["bpcia"], 5)

    def test_udf_find_episode_for_visit_tc2(self):
        c.test_case_name = 'Verify udf_find_episode_for_visit will not change priority order if hiding the episode'
        ext_visit_id = e.generate_visit_number()
        print(c.test_case_name)
        p_visit_id = db.insert_patient_visit(
            acute_site_id, ext_visit_id, e.generate_random_name('Patient'), 'Test', '1970-10-15', date.today() - timedelta(days=6))

        e_id = self.prepare_episode(p_visit_id, ext_visit_id)
        print(ext_visit_id)
        print(p_visit_id)
        print(e_id)

        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC', 'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_is_hidden_status(e_id["uhc"], 1)

        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)

    def test_udf_find_episode_for_visit_tc3(self):
        c.test_case_name = 'Verify udf_find_episode_for_visit will not change priority order if updating episode status to Candidate/Confirmed/Complete'
        ext_visit_id = e.generate_visit_number()
        print(c.test_case_name)
        p_visit_id = db.insert_patient_visit(
            acute_site_id, ext_visit_id, e.generate_random_name('Patient'), 'Test', '1970-10-15', date.today() - timedelta(days=6))

        e_id = self.prepare_episode(p_visit_id, ext_visit_id)
        print(ext_visit_id)
        print(p_visit_id)
        print(e_id)

        db.update_episode_status(e_id["uhc"], 1)
        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)

        db.update_episode_status(e_id["uhc"], 2)
        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)

        db.update_episode_status(e_id["uhc"], 7)
        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)

    def test_udf_find_episode_for_visit_tc4(self):
        c.test_case_name = 'Verify udf_find_episode_for_visit will slide precedence order to the next if updating episode status to dismissed'
        ext_visit_id = e.generate_visit_number()
        print(c.test_case_name)
        p_visit_id = db.insert_patient_visit(
            acute_site_id, ext_visit_id, e.generate_random_name('Patient'), 'Test', '1970-10-15', date.today() - timedelta(days=6))

        e_id = self.prepare_episode(p_visit_id, ext_visit_id)
        print(ext_visit_id)
        print(p_visit_id)
        print(e_id)

        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC', 'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["uhc"], 5)
        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA Humana',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)

    def test_udf_find_episode_for_visit_tc5(self):
        c.test_case_name = 'Verify udf_find_episode_for_visit will slide precedence order to the next if updating episode status to system dismissed'
        ext_visit_id = e.generate_visit_number()
        print(c.test_case_name)
        p_visit_id = db.insert_patient_visit(
            acute_site_id, ext_visit_id, e.generate_random_name('Patient'), 'Test', '1970-10-15', date.today() - timedelta(days=6))

        e_id = self.prepare_episode(p_visit_id, ext_visit_id)
        print(ext_visit_id)
        print(p_visit_id)
        print(e_id)

        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC', 'Error  - DB Results: ' + result[0][0].ProgramPayerName)
        db.update_episode_status(e_id["uhc"], 8)

        result = db.get_udf_find_episodes_for_visit(1, ext_visit_id, 10)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA Humana',  'Error  - DB Results: ' + result[0][0].ProgramPayerName)
    '''
    def test_udf_find_episode_for_all(self):
        c.test_case_name = 'Verify udf_find_episode_for_all will return correct episode info for the patient without error'
        ext_visit_id = e.generate_visit_number()
        print(c.test_case_name)
        p_visit_id = db.insert_patient_visit(
            acute_site_id, ext_visit_id, e.generate_random_name('Patient'), 'Test', '1970-10-15', date.today() - timedelta(days=6))

        e_id = self.prepare_episode(p_visit_id, ext_visit_id)
        print(ext_visit_id)
        print(p_visit_id)
        print(e_id)

        result = db.get_udf_find_episodes_for_all(1, ext_visit_id, 300)
        u.assert_and_log(result[0][0].ProgramPayerName == 'PHMA UHC', 'Error  - DB Results: ' + result[0][0].ProgramPayerName)
    '''

# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)
