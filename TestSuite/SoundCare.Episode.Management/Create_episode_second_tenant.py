import unittest
import config as c
from EndPoints import soundcare
from Utilities import target_process_util as th
from Helpers import file_helper as f, ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
import json as j
from ddt import ddt, data


c.suite_name = 'Create Episode'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 81017
c.run_target_process = False


@ddt
class CreateEpisode(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['BPCIA', 'Patient', '1951-10-11', 'Bpcia',  3,  1], 'Bpcia', """"""),
        d.annotated2(['UserOverride', 'Patient', '1951-10-11', 'Manual', 3, 1], 'UserOverride', """"""),
        d.annotated2(['GALLAHER', 'RITA', '1953-03-31', 'Uhc', 1, 2], 'Uhc', """"""),
        d.annotated2(['Patient', 'Wellmed', '1968-01-01', 'WellMed', 5, 3], 'WellMed' , """"""),
        d.annotated2(['Patient', 'Christus', '1968-01-01', 'Christus', 7, 7], 'Christus', """"""),
        d.annotated2(['Patient', 'Aledade', '1968-01-01', 'Aledade', 11, 11], 'Aledade', """""")
        #d.annotated2(['Patient', 'MtCarmel', '1968-01-01', 'MtCarmel', 8, 8], 'MtCarmel', """"""),
        #d.annotated2(['Patient', 'ProspectCrozer', '1968-01-01', 'ProspectCrozer', 9, 9], 'ProspectCrozer', """"""),
        #d.annotated2(['Patient', 'Prospect', '1968-01-01', 'Prospec', 6, 4], 'Prospect', """"""),
        #d.annotated2(['Patient', 'Humana', '1951-10-11', 'Humana', 10, 10], 'Humana', """""")
    )
    def test_create_episode(self, values):
        l_name, f_name, dob, program, engine_id, pr_payer_id = values

        c.test_case_name = 'Verify if ' + getattr(values, "__name__") + ' episode is successfully created in SoundCare'
        print(c.test_case_name)
        payload = f.get_payload_data('.\\..\\..\\PayloadData\\SendToSoundCare.txt')
        e_id, e_name, p_id, p_name = e.get_program_info(program)
        visit_id = e.generate_visit_number()
        print(visit_id)
        e.update_header(payload, 'Admit')
        e.update_patient_info(payload, f_name, l_name, dob)
        e.update_patient_visit(payload, visit_id, e_id, e_name, p_id, p_name, 'Confirmed: Anchor Stay')
        e.update_acute_site(payload, '', 'Sandbox Hospital 3.0', '383')

        episode_status_id = e.episode_status(payload["patientVisit"]["episodeStatus"])

        body = j.dumps(payload)

        resp = soundcare.send_sc_message_second_tenant(body, 'tst')
        u.assert_and_log(resp.status_code == 202, 'Failed to send message - Actual code: ' + str(resp.status_code))

        result = db.get_episode(visit_id)
        u.assert_and_log(result is not None, 'Failed to create ' + program + ' episode')
        if result is not None:
            e.verify_episode_info(result, payload, engine_id, pr_payer_id)
            e.verify_patient_visit_info(result, payload)
        db.delete_episode_records(visit_id, result[0][0].EpisodeId)


# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

