import unittest
import config as c
from EndPoints import soundcare
from Utilities import target_process_util as th
from Helpers import file_helper as f, ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
import json as j
from ddt import ddt, data


c.suite_name = 'Next Possible Actions'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 104403
c.run_target_process = True

acute_site_id = 418  # Sandbox Hospital 3.0
ext_acute_site_id = 383
token = soundcare.request_token(c.env)
header = {'Content-Type': "application/json", 'Authorization': 'Bearer ' + token}


@ddt
class NextPossibleActions(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''
        db.update_episode_status(13376, 2)
        db.update_episode_patient_visit_status(13376, 2)

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            db.update_episode_status(13376, 2)
            db.update_episode_patient_visit_status(13376, 2)
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2([7592834, 13376, 1,  0, 1], 'Current Status is Candidate', """next possible API actions will be Confirmed and Dismissed"""),
        d.annotated2([7592834, 13376, 2,  0, 2], 'Current Status is Confirmed', """next possible API actions will be Candidate and Dismissed"""),
        d.annotated2([7592834, 13376, 5,  0, 5], 'Current Status is Dismissed', """next possible API actions will be None"""),
        d.annotated2([7592834, 13376, 7,  0, 7], 'Current Status is Completed', """next possible API actions will be None"""),
        d.annotated2([7592834, 13376, 8,  0, 8], 'Current Status is System Dismisses', """next possible API actions will be None"""),
        d.annotated2([7592834, 13376, 8,  1, 1], 'Current Status is Candidate', """next possible API actions will be Confirmed and Dismissed - EpisodePatientVisit status takes precedent"""),
        d.annotated2([7592834, 13376, 7,  2, 2], 'Current Status is Confirmed', """next possible API actions will be Candidate and Dismissed - EpisodePatientVisit status takes precedent"""),
        d.annotated2([7592834, 13376, 2,  5, 5], 'Current Status is Dismissed', """next possible API actions will be None - EpisodePatientVisit status takes precedent"""),
        d.annotated2([7592834, 13376, 5,  7, 7], 'Current Status is Completed', """next possible API actions will be None - EpisodePatientVisit status takes precedent"""),
        d.annotated2([7592834, 13376, 1,  8, 8], 'Current Status is System Dismissed', """next possible API actions will be None - EpisodePatientVisit status takes precedent""")
    )
    def test_get_episode_sync(self, values):
        ext_visit_id, episode_id, episode_status, episode_patient_visit_status, current_status = values
        candidate_url = 'https://php-' + c.env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4cab-b7bd-8d257cf70dff/patientVisits/' + str(ext_visit_id) + '/episodeStatus/update/1?u=jebishop@soundphysicians.com'
        confirmed_url = 'https://php-' + c.env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4cab-b7bd-8d257cf70dff/patientVisits/' + str(ext_visit_id) + '/episodeStatus/update/2?u=jebishop@soundphysicians.com'
        dismissed_url = 'https://php-' + c.env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4cab-b7bd-8d257cf70dff/patientVisits/' + str(ext_visit_id) + '/episodeStatus/update/5?surveyConfigured=1&surveyCompleted=0&u=jebishop@soundphysicians.com'

        c.test_case_name = 'Get Episode Sync Api - Verify if ' + getattr(values, "__name__") + ', ' + getattr(values, "__doc__")
        print(c.test_case_name)

        db.update_episode_status(episode_id, episode_status)
        db.update_episode_patient_visit_status(episode_id, episode_patient_visit_status)

        res, j_res = soundcare.get_episodes_sync(c.env, ext_acute_site_id, header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))

        found, index = v.is_item_existed(j_res, ext_visit_id)

        if current_status in (1,2):
            print(j_res['items'][index]['validStatusUpdates'][0])
            print(j_res['items'][index]['validStatusUpdates'][1])

        if current_status == 1:
            u.assert_and_log(j_res['items'][index]['validStatusUpdates'][0] == confirmed_url, 'Incorrect status')
            u.assert_and_log(j_res['items'][index]['validStatusUpdates'][1] == dismissed_url, 'Incorrect status')
        elif current_status == 2:
            u.assert_and_log(j_res['items'][index]['validStatusUpdates'][0] == candidate_url, 'Incorrect status')
            u.assert_and_log(j_res['items'][index]['validStatusUpdates'][1] == dismissed_url, 'Incorrect status')
        elif current_status == 5:
            u.assert_and_log(v.verify_key_existed_in_dictionary(j_res['items'][index], 'validStatusUpdates') is False, 'Should have no option')
        elif current_status == 7:
            u.assert_and_log(v.verify_key_existed_in_dictionary(j_res['items'][index], 'validStatusUpdates') is False, 'Should have no option')
        elif current_status == 8:
            u.assert_and_log(v.verify_key_existed_in_dictionary(j_res['items'][index], 'validStatusUpdates') is False, 'Should have no option')

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------

    @data(
        d.annotated2([7592834, 13376, 1,  0, 1], 'Current Status is Candidate', """next possible API actions will be Confirmed and Dismissed"""),
        d.annotated2([7592834, 13376, 2,  0, 2], 'Current Status is Confirmed', """next possible API actions will be Candidate and Dismissed"""),
        d.annotated2([7592834, 13376, 8,  1, 1], 'Current Status is Candidate', """next possible API actions will be Confirmed and Dismissed - EpisodePatientVisit status takes precedence"""),
        d.annotated2([7592834, 13376, 7,  2, 2], 'Current Status is Confirmed', """next possible API actions will be Candidate and Dismissed - EpisodePatientVisit status takes precedence"""),
    )
    def test_get_episode(self, values):
        ext_visit_id, episode_id, episode_status, episode_patient_visit_status, current_status = values
        candidate_url = 'https://php-' + c.env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4cab-b7bd-8d257cf70dff/patientVisits/' + str(ext_visit_id) + '/episodeStatus/update/1?u=jebishop@soundphysicians.com'
        confirmed_url = 'https://php-' + c.env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4cab-b7bd-8d257cf70dff/patientVisits/' + str(ext_visit_id) + '/episodeStatus/update/2?u=jebishop@soundphysicians.com'
        dismissed_url = 'https://php-' + c.env + '-api-episodemanagement.azurewebsites.net/88592232-0585-4cab-b7bd-8d257cf70dff/patientVisits/' + str(ext_visit_id) + '/episodeStatus/update/5?surveyConfigured=1&surveyCompleted=0&u=jebishop@soundphysicians.com'

        c.test_case_name = 'Get Episode Api - Verify if ' + getattr(values, "__name__") + ', ' + getattr(values, "__doc__")
        print(c.test_case_name)

        db.update_episode_status(episode_id, episode_status)
        db.update_episode_patient_visit_status(episode_id, episode_patient_visit_status)

        res, j_res = soundcare.get_episodes(c.env, ext_acute_site_id, header)
        u.assert_and_log(res.status_code == 200, 'Failed to get survey - Actual code: ' + str(res.status_code))

        found, index = v.is_item_existed(j_res, ext_visit_id)

        if current_status == 1:
            u.assert_and_log(j_res['items'][index]['validStatusUpdates'][0] == confirmed_url, 'Incorrect status')
            u.assert_and_log(j_res['items'][index]['validStatusUpdates'][1] == dismissed_url, 'Incorrect status')
        elif current_status == 2:
            u.assert_and_log(j_res['items'][index]['validStatusUpdates'][0] == candidate_url, 'Incorrect status')
            u.assert_and_log(j_res['items'][index]['validStatusUpdates'][1] == dismissed_url, 'Incorrect status')
        elif current_status == 5:
            u.assert_and_log(v.verify_key_existed_in_dictionary(j_res['items'][index], 'validStatusUpdates') is False, 'Should have no option')
        elif current_status == 7:
            u.assert_and_log(v.verify_key_existed_in_dictionary(j_res['items'][index], 'validStatusUpdates') is False, 'Should have no option')
        elif current_status == 8:
            u.assert_and_log(v.verify_key_existed_in_dictionary(j_res['items'][index], 'validStatusUpdates') is False, 'Should have no option')


# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

