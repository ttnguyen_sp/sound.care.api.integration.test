import unittest
import config as c
from EndPoints import soundcare
from Utilities import target_process_util as th
from Helpers import file_helper as f, ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
import json as j
from ddt import ddt, data


c.suite_name = 'Create Episode'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 91602
c.run_target_process = False

# ('9944916', '9962988', '9957389', '9933635', '9969652', '9920956', '9951344')
@ddt
class CreateEpisode(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['BPCIA', 'Patient', '1951-10-11', 'Bpcia',  3,  1], 'Bpcia', """"""),

    )
    def test_create_episode(self, values):
        l_name, f_name, dob, program, engine_id, pr_payer_id = values

        c.test_case_name = 'Verify if ' + getattr(values, "__name__") + ' episode is successfully created in SoundCare'
        print(c.test_case_name)

        tenant = 1
        tenant_guid = None
        if tenant == 1:
            tenant_guid = '88592232-0585-4CAB-B7BD-8D257CF70DFF'  # First Tenant GUID
        elif tenant == 2:
            tenant_guid = '756DFB9C-7CD7-430C-824C-1FCDE6B72207'  # Second Tenant GUID

        payload = f.get_payload_data('.\\..\\..\\PayloadData\\SendToSoundCare.txt')
        e_id, e_name, p_id, p_name = e.get_program_info('Bpcia')

        e.update_diagnoses(0, seq0)

        payload["diagnoses"][0]["code"]
        payload["diagnoses"][0]["type"]
        payload["diagnoses"][0]["codingMethod"]
        payload["diagnoses"][0]["relateGroup"]
        payload["diagnoses"][0]["priority"]
        payload["diagnoses"][0]["sequenceNumber"]

        visit_id = e.generate_visit_number()
        print(visit_id)
        e.update_header(payload, 'Admit', tenant_guid)
        e.update_patient_info(payload, 'Patient', 'BPCIA', '1951-10-11')
        e.update_patient_visit(payload, visit_id, e_id, e_name, p_id, p_name, 'Confirmed: Anchor Stay')
        e.update_acute_site(payload, '', 'Sandbox Hospital 3.0', '383')

        episode_status_id = e.episode_status(payload["patientVisit"]["episodeStatus"])

        body = j.dumps(payload)
        resp = soundcare.send_sc_message(body, c.env, tenant)
        u.assert_and_log(resp.status_code == 200, 'Failed to send message - Actual code: ' + str(resp.status_code))


# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

