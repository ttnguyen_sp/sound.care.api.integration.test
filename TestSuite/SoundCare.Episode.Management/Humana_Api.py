import unittest
import config as c
from EndPoints import soundcare
from datetime import date, timedelta
from Utilities import target_process_util as th
from Helpers import file_helper as f, ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
import json as j
from ddt import ddt, data

c.suite_name = 'Humana API - [SoCare API]'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 84673
c.run_target_process = False

@ddt
class HumanaApi(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    def create_humana_episode(self, anchor_stay_id, l_name, f_name, dob):
        p_visit_id = db.insert_patient_visit(418, anchor_stay_id, f_name, l_name, dob, date.today() - timedelta(days=10))
        episode_id = db.insert_episode(10, 10, anchor_stay_id)
        db.insert_episode_patient_visit(p_visit_id, episode_id)
        return episode_id, p_visit_id

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        #d.annotated2(['dalby', 'fake', '1938-02-17', 'M',  '98662', 'N'], 'Verify the patient is not Humana participant if unmatched first name', """"""),
        #d.annotated2(['fakes', 'earl', '1938-02-17', 'M',  '98662', 'N'], 'Verify the patient is not Humana participant if unmatched last name', """"""),
        #d.annotated2(['dalby', 'earl', '1938-02-18', 'M',  '98662', 'N'], 'Verify the patient is not Humana participant if unmatched dob', """"""),
        #d.annotated2(['dalby', 'earl', '1938-02-17', 'F',  '98662', 'N'], 'Verify the patient is not Humana participant if unmatched sex', """"""),
        #d.annotated2(['dalby', 'earl', '1938-02-17', 'M',  '98663', 'N'], 'Verify the patient is not Humana participant if unmatched zipcode', """"""),
        d.annotated2(['dalby', 'earl', '1938-02-17', 'M',  '98662', 'y'], 'Verify the patient is Humana participant if match all firstname/lastname/dob/sex/zipcode', """"""),
        #d.annotated2(['dalby', 'earl', '1938-02-17', 'M',  '98662', 'I'], 'Verify the message will be ignored if already Humana patient with the same patient visit id', """"""),
        #d.annotated2(['dalby', 'earl', '1938-02-17', 'M',  '98662', 'S'], 'Verify subsequent episode will be create if already Humana patient but different patient visit id', """"""),
        #d.annotated2(['dalby', 'earl  ', '1938-02-17', 'M', '98662', 'Y'], 'Verify firstname string is normalized before hashing/sending to Humana api', """"""),
        #d.annotated2(['dalby  ', 'earl', '1938-02-17', 'M',  '98662', 'Y'], 'Verify lastname string is normalized before hashing/sending to Humana api', """"""),
        #d.annotated2(['dalby', 'earl', '1938-02-17', 'm',  '98662', 'Y'], 'Verify sex string is normalized before hashing/sending to Humana api', """"""),
        #$d.annotated2(['dalby', 'earl', '1938-02-17', 'M',  '98662  ', 'Y'], 'Verify zipcode string is normalized before hashing/sending to Humana api', """""")
    )
    def test_create_episode(self, values):
        l_name, f_name, dob, sex, zip, participant = values
        admit_date = date.today() - timedelta(days=5)
        anchor_stay_id = None
        anchor_episode_id = None
        anchor_p_visit_id = None
        episode_id = None
        current_p_visit_id = None

        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        tenant = 1
        tenant_guid = None
        if tenant == 1:
            tenant_guid = '88592232-0585-4CAB-B7BD-8D257CF70DFF'  # First Tenant GUID
        elif tenant == 2:
            tenant_guid = '756DFB9C-7CD7-430C-824C-1FCDE6B72207'  # Second Tenant GUID

        payload = f.get_payload_data('.\\..\\..\\PayloadData\\SendToSoundCare.txt')
        e_id, e_name, p_id, p_name = e.get_program_info('Humana')
        ext_visit_id = e.generate_visit_number()

        print('External Visit ID: ' + str(ext_visit_id))

        if participant == 'S':
            anchor_stay_id = e.generate_visit_number()
            anchor_p_visit_id, episode_id = db.create_episode_using_query_test(418, anchor_stay_id, 'Humana', dob, f_name, l_name, admit_date)

        if participant == 'I':
            current_p_visit_id, episode_id = db.create_episode_using_query_test(418, ext_visit_id, 'Humana', dob, f_name, l_name, admit_date)

        print('Anchor Stay ID: ' + str(anchor_stay_id))
        print('Anchor Episode ID: ' + str(anchor_episode_id))
        print('Anchor Patient Visit ID: ' + str(anchor_p_visit_id))
        print('CurrentPatient Visit ID: ' + str(current_p_visit_id))
        print('Episode ID: ' + str(episode_id))

        e.update_header(payload, 'Admit', tenant_guid)
        e.update_patient_info(payload, f_name, l_name, dob)
        e.update_patient_visit(payload, ext_visit_id, e_id, e_name, p_id, p_name, 'Confirmed: Anchor Stay')
        e.update_acute_site(payload, '', 'Sandbox Hospital 3.0', '383')

        episode_status_id = e.episode_status(payload["patientVisit"]["episodeStatus"])

        body = j.dumps(payload)

        resp = soundcare.send_sc_message(body, c.env)
        u.assert_and_log(resp.status_code == 200, 'Failed to send message - Actual code: ' + str(resp.status_code))

        result = db.get_episode(ext_visit_id, 30)

        if participant == 'N':
            u.assert_and_log(result is None, 'Should not create Humana episode')

        elif participant == 'Y':
            e.verify_episode_info(result, payload, 10, 10)

        elif participant == 'S':
            result2 = db.get_episode(anchor_stay_id, 30)
            e.verify_episode_info(result2, payload, 10, 10)
            u.assert_and_log(result[0][0].EpisodeId == result[0][0].EpisodeId, 'Subsequent episode should have the same episode id')

        elif participant == 'I':
            u.assert_and_log(result[0].__len__() == 1, 'The message should be ignore')

        #db.delete_episode_records(ext_visit_id, result[0][0].EpisodeId)
        #db.delete_episode_records(anchor_stay_id, result[0][0].EpisodeId)


# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

