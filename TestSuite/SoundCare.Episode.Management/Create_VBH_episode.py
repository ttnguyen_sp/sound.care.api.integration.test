import unittest
import config as c
from EndPoints import soundcare
from Utilities import target_process_util as th
from Helpers import file_helper as f, ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
import json as j
from ddt import ddt, data


c.suite_name = 'Create VBH Episode'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 107600
c.run_target_process = True

# ('9944916', '9962988', '9957389', '9933635', '9969652', '9920956', '9951344')
@ddt
class CreateEpisode(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Long Term Care',			  32, 0, 0, 0, 0, 'Manual'  ], '', """"""),
        d.annotated2(['Nursing Facility',		  32, 0, 0, 0, 0, 'Bpcia'   ], '', """"""),
        d.annotated2(['Skilled Nursing Facility', 31, 0, 0, 0, 0, 'Uhc'     ], '', """"""),
        d.annotated2(['Home',					  12, 0, 0, 0, 0, 'WellMed' ], '', """"""),
        d.annotated2(['Inpatient',				  21, 0, 0, 0, 0, 'Prospect'], '', """"""),
        d.annotated2(['Inpatient',				  21, 1, 0, 0, 0, 'Prospect'], '', """"""),
        d.annotated2(['Inpatient',				  21, 0, 1, 0, 0, 'Prospect'], '', """"""),
        d.annotated2(['Inpatient',				  21, 0, 0, 1, 0, 'Prospect'], '', """"""),
        d.annotated2(['Inpatient',				  21, 0, 0, 0, 1, 'Prospect'], '', """"""),
        d.annotated2(['Observation',			  22, 0, 0, 0, 0, 'Christus'], '', """"""),
        d.annotated2(['Outpatient',				  22, 0, 0, 0, 0, 'MtCarmel'], '', """"""),
        d.annotated2(['ER',						  23, 0, 0, 0, 0, 'Crozer'  ], '', """"""),
        d.annotated2(['IRU',					  61, 0, 0, 0, 0, 'Humana'  ], '', """"""),
        d.annotated2(['Inpt Rehab Facility',	  61, 0, 0, 0, 0, 'Aledade' ], '', """"""),
        d.annotated2(['Inpt Psych Facility',	  51, 0, 0, 0, 0, 'Carolina'], '', """"""),
        d.annotated2(['Behavioral Health',		  56, 0, 0, 0, 0, 'Chenmed' ], '', """"""),
        d.annotated2(['Office',					  11, 0, 0, 0, 0, 'Catalyst'], '', """"""),
        d.annotated2(['Telehealth',				  2,  0, 0, 0, 0, 'Iora'    ], '', """"""),
        d.annotated2(['Ambulatory',				  24, 0, 0, 0, 0, 'Manual'  ], '', """"""),
        d.annotated2(['Comprehensive Outpatient Rehab Facility', 62, 0, 0, 0, 0, 'Uhc'], '', """""")
    )
    def test_vbh_episode(self, values):
        pos, pos_code, one_time_charge, none_billable, consult, one_time_code, program = values
        l_name = 'Patient'
        f_name = program
        dob = '1968-01-01'
        engine_id = 14
        pr_payer_id = 5
        #engine_id = 21
        #pr_payer_id = 5
        code = ''
        if none_billable == 1:
            code = ' None Billable code'
        if consult == 1:
            code = ' Consult code'
        if one_time_code == 1:
            code = ' One time code'
        if one_time_charge == 1:
            code = ' One time charge'

        c.test_case_name = 'Verify VBH episode is successfully created in SoundCare with' + code + ' - POS = ' + pos
        print(c.test_case_name)

        tenant = 1
        tenant_guid = None
        if tenant == 1:
            tenant_guid = '88592232-0585-4CAB-B7BD-8D257CF70DFF'  # First Tenant GUID
        elif tenant == 2:
            tenant_guid = '756DFB9C-7CD7-430C-824C-1FCDE6B72207'  # Second Tenant GUID

        payload = f.get_payload_data('.\\..\\..\\PayloadData\\SendToSoundCare.txt')
        e_id, e_name, p_id, p_name = e.get_program_info(program)
        
        visit_id = e.generate_visit_number()
        print(visit_id)
        #e.update_header(payload, 'Admit', tenant_guid)
        e.update_patient_info(payload, f_name, l_name, dob)
        e.update_patient_visit(payload, visit_id, e_id, e_name, p_id, p_name, 'Confirmed: Anchor Stay')
        e.update_place_of_service(payload, pos, pos_code)
        e.update_one_time_charge(payload, one_time_charge)
        e.update_procedure_code(payload, none_billable, consult, one_time_code)

        e.update_acute_site(payload, '', 'Sandbox Hospital 3.0', '383')

        #episode_status_id = e.episode_status(payload["patientVisit"]["episodeStatus"])

        body = j.dumps(payload)
        resp = soundcare.send_sc_message(body, c.env, tenant)
        u.assert_and_log(resp.status_code == 200, 'Failed to send message - Actual code: ' + str(resp.status_code))

        result = db.get_episode(visit_id)
        u.assert_and_log(result is not None, 'Failed to create ' + program + ' episode')
        if result is not None:
            e.verify_vbh_episode_info(result, payload, engine_id, pr_payer_id)
            e.verify_patient_visit_info(result, payload)
        #db.delete_episode_records(visit_id, result[0][0].EpisodeId)


# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

