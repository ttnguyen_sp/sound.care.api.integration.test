import unittest
import time
from datetime import date, timedelta
import config as c
from EndPoints import soundcare
from Utilities import target_process_util as th
# from Helpers import ddt_helper as d, db_helper as db, utilities as u, episode_management_helper as e, verification_helper as v
from Helpers import ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
from ddt import ddt, data, unpack


c.suite_name = 'Episode Patient Visit Status Update'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 94252
c.run_target_process = False


ext_acute_site_id = 383  # Sandbox Hospital 3.0
acute_site_id = 418  # Sandbox Hospital 3.0
token = soundcare.request_token(c.env)
header = {'Content-Type': "application/json", 'Authorization': 'Bearer ' + token}

@ddt
class UpdateEpisodeStatus(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    def create_avoidable_day_record(self, patient_visit_id):
        test = 'test'

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia',  1,  0, 1, 0, 409], '', """EPV_Status = NULL, E_Status = 1, UpdateTo = 1"""),
        d.annotated2(['Bpcia',  1,  0, 2, 1, 200], '', """EPV_Status = NULL, E_Status = 2, UpdateTo = 1"""),
        d.annotated2(['Bpcia',  1,  0, 5, 0, 409], '', """EPV_Status = NULL, E_Status = 5, UpdateTo = 1"""),
        d.annotated2(['Bpcia',  1,  0, 7, 0, 409], '', """EPV_Status = NULL, E_Status = 7, UpdateTo = 1"""),
        d.annotated2(['Bpcia',  1,  0, 8, 0, 409], '', """EPV_Status = NULL, E_Status = 8, UpdateTo = 1"""),

        d.annotated2(['Bpcia',  1,  1, 1, 1, 409], '', """EPV_Status = 1, E_Status = 1, UpdateTo = 1"""),
        d.annotated2(['Bpcia',  1,  1, 2, 1, 409], '', """EPV_Status = 1, E_Status = 2, UpdateTo = 1"""),
        d.annotated2(['Bpcia',  1,  1, 5, 1, 409], '', """EPV_Status = 1, E_Status = 5, UpdateTo = 1"""),
        d.annotated2(['Bpcia',  1,  1, 7, 1, 409], '', """EPV_Status = 1, E_Status = 7, UpdateTo = 1"""),
        d.annotated2(['Bpcia',  1,  1, 8, 1, 409], '', """EPV_Status = 1, E_Status = 8, UpdateTo = 1"""),

        d.annotated2(['Bpcia',  1,  2, 1, 1, 200], '', """EPV_Status = 2, E_Status = 1, UpdateTo = 1"""),
        d.annotated2(['Bpcia',  1,  2, 2, 1, 200], '', """EPV_Status = 2, E_Status = 2, UpdateTo = 1"""),
        d.annotated2(['Bpcia',  1,  2, 5, 1, 200], '', """EPV_Status = 2, E_Status = 5, UpdateTo = 1"""),
        d.annotated2(['Bpcia',  1,  2, 7, 1, 200], '', """EPV_Status = 2, E_Status = 7, UpdateTo = 1"""),
        d.annotated2(['Bpcia',  1,  2, 8, 1, 200], '', """EPV_Status = 2, E_Status = 8, UpdateTo = 1"""),

        d.annotated2(['Bpcia',  1,  5, 1, 5, 409], '', """EPV_Status = 5, E_Status = 1, UpdateTo = 1"""),
        d.annotated2(['Bpcia',  1,  5, 2, 5, 409], '', """EPV_Status = 5, E_Status = 2, UpdateTo = 1"""),
        d.annotated2(['Bpcia',  1,  5, 5, 5, 409], '', """EPV_Status = 5, E_Status = 5, UpdateTo = 1"""),
        d.annotated2(['Bpcia',  1,  5, 7, 5, 409], '', """EPV_Status = 5, E_Status = 7, UpdateTo = 1"""),
        d.annotated2(['Bpcia',  1,  5, 8, 5, 409], '', """EPV_Status = 5, E_Status = 8, UpdateTo = 1"""),

        d.annotated2(['Bpcia',  2,  0, 1, 2, 200], '', """EPV_Status = NULL, E_Status = 1, UpdateTo = 2"""),
        d.annotated2(['Bpcia',  2,  0, 2, 0, 409], '', """EPV_Status = NULL, E_Status = 2, UpdateTo = 2"""),
        d.annotated2(['Bpcia',  2,  0, 5, 0, 409], '', """EPV_Status = NULL, E_Status = 5, UpdateTo = 2"""),
        d.annotated2(['Bpcia',  2,  0, 7, 0, 409], '', """EPV_Status = NULL, E_Status = 7, UpdateTo = 2"""),
        d.annotated2(['Bpcia',  2,  0, 8, 0, 409], '', """EPV_Status = NULL, E_Status = 8, UpdateTo = 2"""),

        d.annotated2(['Bpcia',  2,  1, 1, 2, 200], '', """EPV_Status = 1, E_Status = 1, UpdateTo = 2"""),
        d.annotated2(['Bpcia',  2,  1, 2, 2, 200], '', """EPV_Status = 1, E_Status = 2, UpdateTo = 2"""),
        d.annotated2(['Bpcia',  2,  1, 5, 2, 200], '', """EPV_Status = 1, E_Status = 5, UpdateTo = 2"""),
        d.annotated2(['Bpcia',  2,  1, 7, 2, 200], '', """EPV_Status = 1, E_Status = 7, UpdateTo = 2"""),
        d.annotated2(['Bpcia',  2,  1, 8, 2, 200], '', """EPV_Status = 1, E_Status = 8, UpdateTo = 2"""),

        d.annotated2(['Bpcia',  2,  2, 1, 2, 409], '', """EPV_Status = 2, E_Status = 1, UpdateTo = 2"""),
        d.annotated2(['Bpcia',  2,  2, 2, 2, 409], '', """EPV_Status = 2, E_Status = 2, UpdateTo = 2"""),
        d.annotated2(['Bpcia',  2,  2, 5, 2, 409], '', """EPV_Status = 2, E_Status = 5, UpdateTo = 2"""),
        d.annotated2(['Bpcia',  2,  2, 7, 2, 409], '', """EPV_Status = 2, E_Status = 7, UpdateTo = 2"""),
        d.annotated2(['Bpcia',  2,  2, 8, 2, 409], '', """EPV_Status = 2, E_Status = 8, UpdateTo = 2"""),

        d.annotated2(['Bpcia',  2,  5, 1, 5, 409], '', """EPV_Status = 5, E_Status = 1, UpdateTo = 2"""),
        d.annotated2(['Bpcia',  2,  5, 2, 5, 409], '', """EPV_Status = 5, E_Status = 2, UpdateTo = 2"""),
        d.annotated2(['Bpcia',  2,  5, 5, 5, 409], '', """EPV_Status = 5, E_Status = 5, UpdateTo = 2"""),
        d.annotated2(['Bpcia',  2,  5, 7, 5, 409], '', """EPV_Status = 5, E_Status = 7, UpdateTo = 2"""),
        d.annotated2(['Bpcia',  2,  5, 8, 5, 409], '', """EPV_Status = 5, E_Status = 8, UpdateTo = 2"""),

        d.annotated2(['Bpcia',  5,  0, 1, 5, 200], '', """EPV_Status = NULL, E_Status = 1, UpdateTo = 5"""),
        d.annotated2(['Bpcia',  5,  0, 2, 5, 200], '', """EPV_Status = NULL, E_Status = 1, UpdateTo = 5"""),
        d.annotated2(['Bpcia',  5,  0, 5, 0, 404], '', """EPV_Status = NULL, E_Status = 1, UpdateTo = 5"""),
        d.annotated2(['Bpcia',  5,  0, 7, 0, 200], '', """EPV_Status = NULL, E_Status = 1, UpdateTo = 5"""),
        d.annotated2(['Bpcia',  5,  0, 8, 0, 404], '', """EPV_Status = NULL, E_Status = 1, UpdateTo = 5"""),

        d.annotated2(['Bpcia',  5,  1, 1, 5, 200], '', """EPV_Status = 1, E_Status = 1, UpdateTo = 5"""),
        d.annotated2(['Bpcia',  5,  1, 2, 5, 200], '', """EPV_Status = 1, E_Status = 2, UpdateTo = 5"""),
        d.annotated2(['Bpcia',  5,  1, 5, 5, 200], '', """EPV_Status = 1, E_Status = 5, UpdateTo = 5"""),
        d.annotated2(['Bpcia',  5,  1, 7, 5, 200], '', """EPV_Status = 1, E_Status = 7, UpdateTo = 5"""),
        d.annotated2(['Bpcia',  5,  1, 8, 5, 200], '', """EPV_Status = 1, E_Status = 8, UpdateTo = 5"""),

        d.annotated2(['Bpcia',  5,  2, 1, 5, 200], '', """EPV_Status = 2, E_Status = 1, UpdateTo = 5"""),
        d.annotated2(['Bpcia',  5,  2, 2, 5, 200], '', """EPV_Status = 2, E_Status = 2, UpdateTo = 5"""),
        d.annotated2(['Bpcia',  5,  2, 5, 5, 200], '', """EPV_Status = 2, E_Status = 5, UpdateTo = 5"""),
        d.annotated2(['Bpcia',  5,  2, 7, 5, 200], '', """EPV_Status = 2, E_Status = 7, UpdateTo = 5"""),
        d.annotated2(['Bpcia',  5,  2, 8, 5, 200], '', """EPV_Status = 2, E_Status = 8, UpdateTo = 5"""),

        d.annotated2(['Bpcia',  5,  5, 1, 5, 404], '', """EPV_Status = NULL, E_Status = 1, UpdateTo = 5"""),
        d.annotated2(['Bpcia',  5,  5, 2, 5, 404], '', """EPV_Status = NULL, E_Status = 2, UpdateTo = 5"""),
        d.annotated2(['Bpcia',  5,  5, 5, 5, 404], '', """EPV_Status = NULL, E_Status = 5, UpdateTo = 5"""),
        d.annotated2(['Bpcia',  5,  5, 7, 5, 404], '', """EPV_Status = NULL, E_Status = 7, UpdateTo = 5"""),
        d.annotated2(['Bpcia',  5,  5, 8, 5, 404], '', """EPV_Status = NULL, E_Status = 8, UpdateTo = 5""")
    )
    def test_create_episode(self, values):
        episode_type, u_status, ep_status, e_status, expect_status, return_code = values

        c.test_case_name = 'Verify if setting status to ' + str(u_status) + \
                           ' where existing EpisodePatientVisitStatus is ' + str(ep_status) + \
                           ' and EpisodeStatus is ' + str(e_status) + ' The return code should be ' + str(return_code)
        if ep_status == 0:
            c.test_case_name = 'Verify if setting status to ' + str(u_status) + \
                           ' where existing EpisodePatientVisitStatus is NULL and EpisodeStatus is ' + str(e_status) + \
                           ' The return code should be ' + str(return_code)

        print(c.test_case_name)

        tenant = 1
        tenant_guid = None
        if tenant == 1:
            tenant_guid = '88592232-0585-4CAB-B7BD-8D257CF70DFF'  # First Tenant GUID
        elif tenant == 2:
            tenant_guid = '756DFB9C-7CD7-430C-824C-1FCDE6B72207'  # Second Tenant GUID

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), episode_type)

        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

        db.update_episode_status(episode_id, e_status)
        db.update_episode_patient_visit_status(episode_id, ep_status)
        time.sleep(5)
        res = soundcare.update_episode_status(tenant_guid, c.env, ext_visit_id, u_status, header)
        u.assert_and_log(res.status_code == return_code, 'Actual return code: ' + str(res.status_code) + ', expect: ' + str(return_code))
        if u_status == 5 and res.status_code != 404:
            res = soundcare.save_dismissal_survey(c.env, ext_visit_id, header)

        db_res = db.get_episode_patient_visit_by_episode_id(episode_id, 3)
        actual_status = db_res[0][0].EpisodeStatusTypeId
        if actual_status is None:
            actual_status = 0

        u.assert_and_log(actual_status == expect_status, 'Actual status: ' + str(actual_status) + ', expect: ' + str(expect_status))
        test = 'test'

        #db.delete_episode_records(visit_id, result[0][0].EpisodeId)

# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)
