import unittest
import config as c
from EndPoints import soundcare
from datetime import date, timedelta
from Utilities import target_process_util as th
from Helpers import file_helper as f, ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
import json as j
from ddt import ddt, data


c.suite_name = 'Create Episode'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 82810
c.run_target_process = False


@ddt
class NextSiteOfCareMappingEpisode(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))
    '''
    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Bpcia', 1 , '383C975B-3009-4279-A95B-B2848AADB54F'], 'Home - PCP Follow Up', """Admit"""),
        d.annotated2(['Bpcia', 3 , 'E0359F09-491F-42DE-8B80-8B351FC703E8'], 'SNF - Short Term Care', """Update"""),
        d.annotated2(['Bpcia', 62, 'A127E1A8-4E1F-40D2-9ADB-57F27D3ABE47'], 'ReHab', """Discharge"""),
        d.annotated2(['Bpcia', 41, 'A8154CDC-88E9-4E36-BD21-B5D801056681'], 'Pt Died', """Admit"""),
        d.annotated2(['Bpcia', 42, '8E520A90-9A09-4FDF-AA79-3CF2EA058365'], 'Hospice', """Update"""),
        d.annotated2(['Bpcia', 70, 'C026E027-2892-4028-8AF3-BC09E9181D95'], 'Other Hospital', """Discharge"""),
        d.annotated2(['Bpcia', 61, 'E04BFB43-9EF0-453B-B1C6-7D8F95D76476'], 'Swing', """Admit"""),
        d.annotated2(['Bpcia', 70, 'C026E027-2892-4028-8AF3-BC09E9181D95'], 'Other', """Update"""),
        d.annotated2(['Bpcia', 7 , 'A901965E-A5CE-44E6-B4F1-89D750CD3F8B'], 'AMA', """Discharge"""),
        d.annotated2(['Bpcia', 1 , '383C975B-3009-4279-A95B-B2848AADB54F'], 'Home - Private Residence', """Admit"""),
        d.annotated2(['Bpcia', 50, '8E520A90-9A09-4FDF-AA79-3CF2EA058365'], 'Hospice - Private Residence',"""Update"""),
        d.annotated2(['Bpcia', 62, 'A127E1A8-4E1F-40D2-9ADB-57F27D3ABE47'], 'Inpatient Rehab', """Discharge"""),
        d.annotated2(['Bpcia', 63, '9E0838AC-ED72-4042-A174-55E46B33BAA6'], 'Transfer to other Hospital or LTACH', """Admit"""),
        d.annotated2(['Bpcia', 7 , 'A901965E-A5CE-44E6-B4F1-89D750CD3F8B'], 'Against Medical Advice', """Update"""),
        d.annotated2(['Bpcia', 1 , '383C975B-3009-4279-A95B-B2848AADB54F'], 'HH - Private Residence', """Discharge"""),
        d.annotated2(['Bpcia', 41, 'A8154CDC-88E9-4E36-BD21-B5D801056681'], 'Expired', """Admit"""),
        d.annotated2(['Bpcia', 5 , '7B4517F5-AA6A-4B6C-9309-51FCE1B3D344'], 'Another Acute Care Hospital', """Update"""),
        d.annotated2(['Bpcia', 6 , '5F772053-0D26-4DE2-AC72-4F6EA5F59984'], 'HH - Residential Facility', """Discharge"""),
        d.annotated2(['Bpcia', 51, '602240AC-04A8-4244-A69C-C26444DB5326'], 'Hospice - Medical Facility', """Admit"""),
        d.annotated2(['Bpcia', 63, '9E0838AC-ED72-4042-A174-55E46B33BAA6'], 'LTACH', """Update"""),
        d.annotated2(['Bpcia', 84, '7CA2EC2D-C7DC-4325-BC05-562F76D40D6E'], 'Home - Residential Facility', """Discharge"""),
        d.annotated2(['Bpcia', 63, '9E0838AC-ED72-4042-A174-55E46B33BAA6'], 'Long Term Care', """Admit"""),
        d.annotated2(['Bpcia', 50, '8E520A90-9A09-4FDF-AA79-3CF2EA058365'], 'Hospice - Inpatient', """Update"""),
        d.annotated2(['Bpcia', 65, '53395D99-C73C-463D-83E2-6FE57223E961'], 'Inpatient Psych', """Discharge"""),
        d.annotated2(['Bpcia', 30, '8DE59E6E-FDFF-42EB-B203-B5E9FF2785D7'], 'Remains Inpatient/TRF of Service', """Admit""")
    )
    def test_nsoc_mapping_bpcia(self, values):
        program, sound_care_id, care_type_id = values

        c.test_case_name = 'Verify correct NSOC mapping for ' + getattr(values, "__name__") + ' on ' + getattr(values, "__doc__") + ' message for ' + program
        print(c.test_case_name)
        payload = f.get_payload_data('.\\..\\..\\PayloadData\\SendToSoundCare.txt')
        e_id, e_name, p_id, p_name = e.get_program_info(program)
        visit_id = e.generate_visit_number()
        print(visit_id)
        e.update_header(payload, getattr(values, "__doc__"))
        e.update_patient_info(payload, 'Bpcia', 'Patient', '1951-10-11')

        if getattr(values, "__doc__") == 'Discharge':
            e.update_patient_visit(payload, visit_id, e_id, e_name, p_id, p_name, 'Confirmed: Anchor Stay', str(date.today() - timedelta(days=1)))
        else:
            e.update_patient_visit(payload, visit_id, e_id, e_name, p_id, p_name, 'Confirmed: Anchor Stay')

        e.update_acute_site(payload, '', 'Sandbox Hospital 3.0', '383')
        e.update_post_acute_site(payload, care_type_id)

        body = j.dumps(payload)

        resp = soundcare.send_sc_message(body, c.env)
        u.assert_and_log(resp.status_code == 200, 'Failed to send message - Actual code: ' + str(resp.status_code))

        result = db.get_episode(visit_id, 5)
        u.assert_and_log(result is not None, 'Failed to create ' + program + ' episode')

        discharge_plan = db.get_patient_discharge_plan(result[0][0].PatientVisitId)
        u.assert_and_log(discharge_plan is not None, 'Failed to create patient discharge plan record')
        u.assert_and_log(discharge_plan[0][0].DischargeDispositionActual == sound_care_id, 'Incorrect mapping for - ' + getattr(values, "__name__"))
        #db.delete_episode_records(visit_id, result[0][0].EpisodeId)

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Uhc', 1 , '383C975B-3009-4279-A95B-B2848AADB54F'], 'Home - PCP Follow Up', """Admit"""),
        d.annotated2(['Uhc', 3 , 'E0359F09-491F-42DE-8B80-8B351FC703E8'], 'SNF - Short Term Care', """Update"""),
        d.annotated2(['Uhc', 62, 'A127E1A8-4E1F-40D2-9ADB-57F27D3ABE47'], 'ReHab', """Discharge"""),
        d.annotated2(['Uhc', 41, 'A8154CDC-88E9-4E36-BD21-B5D801056681'], 'Pt Died', """Admit"""),
        d.annotated2(['Uhc', 42, '8E520A90-9A09-4FDF-AA79-3CF2EA058365'], 'Hospice', """Update"""),
        d.annotated2(['Uhc', 70, 'C026E027-2892-4028-8AF3-BC09E9181D95'], 'Other Hospital', """Discharge"""),
        d.annotated2(['Uhc', 61, 'E04BFB43-9EF0-453B-B1C6-7D8F95D76476'], 'Swing', """Admit"""),
        d.annotated2(['Uhc', 70, 'C026E027-2892-4028-8AF3-BC09E9181D95'], 'Other', """Update"""),
        d.annotated2(['Uhc', 7 , 'A901965E-A5CE-44E6-B4F1-89D750CD3F8B'], 'AMA', """Discharge"""),
        d.annotated2(['Uhc', 1 , '383C975B-3009-4279-A95B-B2848AADB54F'], 'Home - Private Residence', """Admit"""),
        d.annotated2(['Uhc', 50, '8E520A90-9A09-4FDF-AA79-3CF2EA058365'], 'Hospice - Private Residence',"""Update"""),
        d.annotated2(['Uhc', 62, 'A127E1A8-4E1F-40D2-9ADB-57F27D3ABE47'], 'Inpatient Rehab', """Discharge"""),
        d.annotated2(['Uhc', 63, '9E0838AC-ED72-4042-A174-55E46B33BAA6'], 'Transfer to other Hospital or LTACH', """Admit"""),
        d.annotated2(['Uhc', 7 , 'A901965E-A5CE-44E6-B4F1-89D750CD3F8B'], 'Against Medical Advice', """Update"""),
        d.annotated2(['Uhc', 1 , '383C975B-3009-4279-A95B-B2848AADB54F'], 'HH - Private Residence', """Discharge"""),
        d.annotated2(['Uhc', 41, 'A8154CDC-88E9-4E36-BD21-B5D801056681'], 'Expired', """Admit"""),
        d.annotated2(['Uhc', 5 , '7B4517F5-AA6A-4B6C-9309-51FCE1B3D344'], 'Another Acute Care Hospital', """Update"""),
        d.annotated2(['Uhc', 6 , '5F772053-0D26-4DE2-AC72-4F6EA5F59984'], 'HH - Residential Facility', """Discharge"""),
        d.annotated2(['Uhc', 51, '602240AC-04A8-4244-A69C-C26444DB5326'], 'Hospice - Medical Facility', """Admit"""),
        d.annotated2(['Uhc', 63, '9E0838AC-ED72-4042-A174-55E46B33BAA6'], 'LTACH', """Update"""),
        d.annotated2(['Uhc', 84, '7CA2EC2D-C7DC-4325-BC05-562F76D40D6E'], 'Home - Residential Facility', """Discharge"""),
        d.annotated2(['Uhc', 63, '9E0838AC-ED72-4042-A174-55E46B33BAA6'], 'Long Term Care', """Admit"""),
        d.annotated2(['Uhc', 50, '8E520A90-9A09-4FDF-AA79-3CF2EA058365'], 'Hospice - Inpatient', """Update"""),
        d.annotated2(['Uhc', 65, '53395D99-C73C-463D-83E2-6FE57223E961'], 'Inpatient Psych', """Discharge"""),
        d.annotated2(['Uhc', 30, '8DE59E6E-FDFF-42EB-B203-B5E9FF2785D7'], 'Remains Inpatient/TRF of Service', """Admit""")
    )
    def test_nsoc_mapping_uhc(self, values):
        program, sound_care_id, care_type_id = values

        c.test_case_name = 'Verify correct NSOC mapping for ' + getattr(values, "__name__") + ' on ' + getattr(values, "__doc__") + ' message for ' + program
        print(c.test_case_name)
        payload = f.get_payload_data('.\\..\\..\\PayloadData\\SendToSoundCare.txt')
        e_id, e_name, p_id, p_name = e.get_program_info(program)
        visit_id = e.generate_visit_number()
        print(visit_id)
        e.update_header(payload, getattr(values, "__doc__"))
        e.update_patient_info(payload, 'RITA', 'GALLAHER', '1953-03-31')

        if getattr(values, "__doc__") == 'Discharge':
            e.update_patient_visit(payload, visit_id, e_id, e_name, p_id, p_name, 'Confirmed: Anchor Stay', str(date.today() - timedelta(days=1)))
        else:
            e.update_patient_visit(payload, visit_id, e_id, e_name, p_id, p_name, 'Confirmed: Anchor Stay')

        e.update_acute_site(payload, '', 'Sandbox Hospital 3.0', '383')
        e.update_post_acute_site(payload, care_type_id)

        body = j.dumps(payload)

        resp = soundcare.send_sc_message(body, c.env)
        u.assert_and_log(resp.status_code == 200, 'Failed to send message - Actual code: ' + str(resp.status_code))

        result = db.get_episode(visit_id, 20)
        u.assert_and_log(result is not None, 'Failed to create ' + program + ' episode')

        discharge_plan = db.get_patient_discharge_plan(result[0][0].PatientVisitId)
        u.assert_and_log(discharge_plan is not None, 'Failed to create patient discharge plan record')
        u.assert_and_log(discharge_plan[0][0].DischargeDispositionActual == sound_care_id, 'Incorrect mapping for - ' + getattr(values, "__name__"))
        db.delete_episode_records(visit_id, result[0][0].EpisodeId)
    '''

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['WellMed', 1 , '383C975B-3009-4279-A95B-B2848AADB54F'], 'Home - PCP Follow Up', """Admit"""),
        d.annotated2(['WellMed', 3 , 'E0359F09-491F-42DE-8B80-8B351FC703E8'], 'SNF - Short Term Care', """Update"""),
        d.annotated2(['WellMed', 62, 'A127E1A8-4E1F-40D2-9ADB-57F27D3ABE47'], 'ReHab', """Discharge"""),
        d.annotated2(['WellMed', 41, 'A8154CDC-88E9-4E36-BD21-B5D801056681'], 'Pt Died', """Admit"""),
        d.annotated2(['WellMed', 42, '8E520A90-9A09-4FDF-AA79-3CF2EA058365'], 'Hospice', """Update"""),
        d.annotated2(['WellMed', 70, 'C026E027-2892-4028-8AF3-BC09E9181D95'], 'Other Hospital', """Discharge"""),
        d.annotated2(['WellMed', 61, 'E04BFB43-9EF0-453B-B1C6-7D8F95D76476'], 'Swing', """Admit"""),
        d.annotated2(['WellMed', 70, 'C026E027-2892-4028-8AF3-BC09E9181D95'], 'Other', """Update"""),
        d.annotated2(['WellMed', 7 , 'A901965E-A5CE-44E6-B4F1-89D750CD3F8B'], 'AMA', """Discharge"""),
        d.annotated2(['WellMed', 1 , '383C975B-3009-4279-A95B-B2848AADB54F'], 'Home - Private Residence', """Admit"""),
        d.annotated2(['WellMed', 50, '8E520A90-9A09-4FDF-AA79-3CF2EA058365'], 'Hospice - Private Residence',"""Update"""),
        d.annotated2(['WellMed', 62, 'A127E1A8-4E1F-40D2-9ADB-57F27D3ABE47'], 'Inpatient Rehab', """Discharge"""),
        d.annotated2(['WellMed', 63, '9E0838AC-ED72-4042-A174-55E46B33BAA6'], 'Transfer to other Hospital or LTACH', """Admit"""),
        d.annotated2(['WellMed', 7 , 'A901965E-A5CE-44E6-B4F1-89D750CD3F8B'], 'Against Medical Advice', """Update"""),
        d.annotated2(['WellMed', 1 , '383C975B-3009-4279-A95B-B2848AADB54F'], 'HH - Private Residence', """Discharge"""),
        d.annotated2(['WellMed', 41, 'A8154CDC-88E9-4E36-BD21-B5D801056681'], 'Expired', """Admit"""),
        d.annotated2(['WellMed', 5 , '7B4517F5-AA6A-4B6C-9309-51FCE1B3D344'], 'Another Acute Care Hospital', """Update"""),
        d.annotated2(['WellMed', 6 , '5F772053-0D26-4DE2-AC72-4F6EA5F59984'], 'HH - Residential Facility', """Discharge"""),
        d.annotated2(['WellMed', 51, '602240AC-04A8-4244-A69C-C26444DB5326'], 'Hospice - Medical Facility', """Admit"""),
        d.annotated2(['WellMed', 63, '9E0838AC-ED72-4042-A174-55E46B33BAA6'], 'LTACH', """Update"""),
        d.annotated2(['WellMed', 84, '7CA2EC2D-C7DC-4325-BC05-562F76D40D6E'], 'Home - Residential Facility', """Discharge"""),
        d.annotated2(['WellMed', 63, '9E0838AC-ED72-4042-A174-55E46B33BAA6'], 'Long Term Care', """Admit"""),
        d.annotated2(['WellMed', 50, '8E520A90-9A09-4FDF-AA79-3CF2EA058365'], 'Hospice - Inpatient', """Update"""),
        d.annotated2(['WellMed', 65, '53395D99-C73C-463D-83E2-6FE57223E961'], 'Inpatient Psych', """Discharge"""),
        d.annotated2(['WellMed', 30, '8DE59E6E-FDFF-42EB-B203-B5E9FF2785D7'], 'Remains Inpatient/TRF of Service', """Admit""")
    )
    def test_nsoc_mapping_covered_live(self, values):
        program, sound_care_id, care_type_id = values

        c.test_case_name = 'Verify correct NSOC mapping for ' + getattr(values, "__name__") + ' on ' + getattr(values, "__doc__") + ' message for ' + program
        print(c.test_case_name)
        payload = f.get_payload_data('.\\..\\..\\PayloadData\\SendToSoundCare.txt')
        e_id, e_name, p_id, p_name = e.get_program_info(program)
        visit_id = e.generate_visit_number()
        print(visit_id)
        e.update_header(payload, getattr(values, "__doc__"))
        e.update_patient_info(payload, 'Wellmed', 'Patient', '1968-01-01')

        if getattr(values, "__doc__") == 'Discharge':
            e.update_patient_visit(payload, visit_id, e_id, e_name, p_id, p_name, 'Confirmed: Anchor Stay', str(date.today() - timedelta(days=1)))
        else:
            e.update_patient_visit(payload, visit_id, e_id, e_name, p_id, p_name, 'Confirmed: Anchor Stay')

        e.update_acute_site(payload, '', 'Sandbox Hospital 3.0', '383')
        e.update_post_acute_site(payload, care_type_id)

        body = j.dumps(payload)

        resp = soundcare.send_sc_message(body, c.env)
        u.assert_and_log(resp.status_code == 200, 'Failed to send message - Actual code: ' + str(resp.status_code))

        result = db.get_episode(visit_id, 20)
        u.assert_and_log(result is not None, 'Failed to create ' + program + ' episode')

        discharge_plan = db.get_patient_discharge_plan(result[0][0].PatientVisitId)
        u.assert_and_log(discharge_plan is not None, 'Failed to create patient discharge plan record')
        u.assert_and_log(discharge_plan[0][0].DischargeDispositionActual == sound_care_id, 'Incorrect mapping for - ' + getattr(values, "__name__"))
        #db.delete_episode_records(visit_id, result[0][0].EpisodeId)


    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Humana', 1 , '383C975B-3009-4279-A95B-B2848AADB54F'], 'Home - PCP Follow Up', """Admit"""),
        d.annotated2(['Humana', 3 , 'E0359F09-491F-42DE-8B80-8B351FC703E8'], 'SNF - Short Term Care', """Update"""),
        d.annotated2(['Humana', 62, 'A127E1A8-4E1F-40D2-9ADB-57F27D3ABE47'], 'ReHab', """Discharge"""),
        d.annotated2(['Humana', 41, 'A8154CDC-88E9-4E36-BD21-B5D801056681'], 'Pt Died', """Admit"""),
        d.annotated2(['Humana', 42, '8E520A90-9A09-4FDF-AA79-3CF2EA058365'], 'Hospice', """Update"""),
        d.annotated2(['Humana', 70, 'C026E027-2892-4028-8AF3-BC09E9181D95'], 'Other Hospital', """Discharge"""),
        d.annotated2(['Humana', 61, 'E04BFB43-9EF0-453B-B1C6-7D8F95D76476'], 'Swing', """Admit"""),
        d.annotated2(['Humana', 70, 'C026E027-2892-4028-8AF3-BC09E9181D95'], 'Other', """Update"""),
        d.annotated2(['Humana', 7 , 'A901965E-A5CE-44E6-B4F1-89D750CD3F8B'], 'AMA', """Discharge"""),
        d.annotated2(['Humana', 1 , '383C975B-3009-4279-A95B-B2848AADB54F'], 'Home - Private Residence', """Admit"""),
        d.annotated2(['Humana', 50, '8E520A90-9A09-4FDF-AA79-3CF2EA058365'], 'Hospice - Private Residence',"""Update"""),
        d.annotated2(['Humana', 62, 'A127E1A8-4E1F-40D2-9ADB-57F27D3ABE47'], 'Inpatient Rehab', """Discharge"""),
        d.annotated2(['Humana', 63, '9E0838AC-ED72-4042-A174-55E46B33BAA6'], 'Transfer to other Hospital or LTACH', """Admit"""),
        d.annotated2(['Humana', 7 , 'A901965E-A5CE-44E6-B4F1-89D750CD3F8B'], 'Against Medical Advice', """Update"""),
        d.annotated2(['Humana', 1 , '383C975B-3009-4279-A95B-B2848AADB54F'], 'HH - Private Residence', """Discharge"""),
        d.annotated2(['Humana', 41, 'A8154CDC-88E9-4E36-BD21-B5D801056681'], 'Expired', """Admit"""),
        d.annotated2(['Humana', 5 , '7B4517F5-AA6A-4B6C-9309-51FCE1B3D344'], 'Another Acute Care Hospital', """Update"""),
        d.annotated2(['Humana', 6 , '5F772053-0D26-4DE2-AC72-4F6EA5F59984'], 'HH - Residential Facility', """Discharge"""),
        d.annotated2(['Humana', 51, '602240AC-04A8-4244-A69C-C26444DB5326'], 'Hospice - Medical Facility', """Admit"""),
        d.annotated2(['Humana', 63, '9E0838AC-ED72-4042-A174-55E46B33BAA6'], 'LTACH', """Update"""),
        d.annotated2(['Humana', 84, '7CA2EC2D-C7DC-4325-BC05-562F76D40D6E'], 'Home - Residential Facility', """Discharge"""),
        d.annotated2(['Humana', 63, '9E0838AC-ED72-4042-A174-55E46B33BAA6'], 'Long Term Care', """Admit"""),
        d.annotated2(['Humana', 50, '8E520A90-9A09-4FDF-AA79-3CF2EA058365'], 'Hospice - Inpatient', """Update"""),
        d.annotated2(['Humana', 65, '53395D99-C73C-463D-83E2-6FE57223E961'], 'Inpatient Psych', """Discharge"""),
        d.annotated2(['Humana', 30, '8DE59E6E-FDFF-42EB-B203-B5E9FF2785D7'], 'Remains Inpatient/TRF of Service', """Admit""")
    )
    def test_nsoc_mapping_humana(self, values):
        program, sound_care_id, care_type_id = values

        c.test_case_name = 'Verify correct NSOC mapping for ' + getattr(values, "__name__") + ' on ' + getattr(values, "__doc__") + ' message for ' + program
        print(c.test_case_name)
        payload = f.get_payload_data('.\\..\\..\\PayloadData\\SendToSoundCare.txt')
        e_id, e_name, p_id, p_name = e.get_program_info(program)
        visit_id = e.generate_visit_number()
        print(visit_id)
        e.update_header(payload, getattr(values, "__doc__"))
        e.update_patient_info(payload, 'earl', 'dalby', '1938-02-17', 'M', '98662')

        if getattr(values, "__doc__") == 'Discharge':
            e.update_patient_visit(payload, visit_id, e_id, e_name, p_id, p_name, 'Confirmed: Anchor Stay', str(date.today() - timedelta(days=1)))
        else:
            e.update_patient_visit(payload, visit_id, e_id, e_name, p_id, p_name, 'Confirmed: Anchor Stay')

        e.update_acute_site(payload, '', 'Sandbox Hospital 3.0', '383')
        e.update_post_acute_site(payload, care_type_id)

        body = j.dumps(payload)

        resp = soundcare.send_sc_message(body, c.env)
        u.assert_and_log(resp.status_code == 200, 'Failed to send message - Actual code: ' + str(resp.status_code))

        result = db.get_episode(visit_id, 20)
        u.assert_and_log(result is not None, 'Failed to create ' + program + ' episode')

        discharge_plan = db.get_patient_discharge_plan(result[0][0].PatientVisitId)
        u.assert_and_log(discharge_plan is not None, 'Failed to create patient discharge plan record')
        u.assert_and_log(discharge_plan[0][0].DischargeDispositionActual == sound_care_id, 'Incorrect mapping for - ' + getattr(values, "__name__"))
        #db.delete_episode_records(visit_id, result[0][0].EpisodeId)
# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

