import unittest
import config as c
from EndPoints import soundcare
from Utilities import target_process_util as th
from Helpers import file_helper as f, utilities as u, episode_management_helper as e
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
import json as j


class CreateEpisode(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')

    def test_create_episode_load_test(self):
        for i in range (0, 1000):
            visit_id = e.generate_visit_number()
            last_name = 'Patient_'+visit_id

            payload = f.get_payload_data('.\\..\\..\\PayloadData\\SendToSoundCare.txt')
            e_id, e_name, p_id, p_name = e.get_program_info('Bpcia')

            print(visit_id)
            e.update_header(payload, 'Admit', c.tenant1_guid)
            e.update_patient_info(payload, 'BPCIA', 'Patient_'+visit_id, '1951-10-11')
            e.update_patient_visit(payload, visit_id, 3, e_name, 1, p_name, 'Confirmed: Anchor Stay')
            e.update_acute_site(payload, '', 'Sandbox Hospital 3.0', '383')

            body = j.dumps(payload)
            resp = soundcare.send_sc_message(body, c.env, c.tenant1)
            u.assert_and_log(resp.status_code == 200, 'Failed to send message - Actual code: ' + str(resp.status_code))


# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

