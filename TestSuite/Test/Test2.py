import requests
import unittest
import config as c
from Helpers import file_helper as f
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
from EndPoints import soundcare as s
import json as j

env = 'tst'
username = 'sound.caller'
password = 'v2b@$10m29jdfr'
auth_str = '%s:%s' % (username, password)
header = {'Authorization': 'Basic %s' % auth_str}

class Test2(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')

# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
    def test_prince_db_connection(self):
        url = 'https://sound' + env + '.appiancloud.com/suite/webapi/post-event'
        payload = f.get_payload_data('.\\..\\..\\PayloadData\\SendToSoundCare.txt')
        body = j.dumps(payload)
        resp = requests.post(url, body, header, auth=(username, password))
        test = 'test'

# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------

if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

