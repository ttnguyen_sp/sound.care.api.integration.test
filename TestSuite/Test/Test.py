import unittest
from datetime import date, timedelta
import config as c
from EndPoints import soundcare
from Utilities import target_process_util as th
from Helpers import  ddt_helper as d, db_helper as db, utilities as u, episode_management_helper as e, verification_helper as v
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
from ddt import ddt, data, unpack


c.suite_name = 'PCP Appointment Scheduling'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 79712
c.run_target_process = False

acute_site_id = 418  # Sandbox Hospital 3.0
ext_acute_site_id = 383
token = soundcare.request_token()
header = {'Content-Type': "application/json", 'Authorization': 'Bearer ' + token}

@ddt
class PcpAppointmentScheduling(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2(['Uhc', 1, 347, '9142147', 'Patient005', 'Test', '1980-01-01', '2020-09-02'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140549', 'itna', 'ilawms', '1972-03-29', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140534', 'ailwm', 'oenjs', '1930-05-05', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140395', 'robet', 'wiest', '1921-11-23', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140394', 'rashon', 'ary', '1967-05-04', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140374', 'harosn', 'orthn', '1960-12-12', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140373', 'srnhae', 'khaney', '1960-07-10', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140366', 'liby', 'srobcy', '1942-10-30', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140363', 'cirpta', 'brdant', '1930-12-27', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140353', 'craie', 'healy', '1978-05-19', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140317', 'jiakce', 'unrbet', '1971-07-16', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140227', 'soje', 'repz', '1980-06-04', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140216', 'dmoan', 'tiaknws', '1963-10-12', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140197', 'jcak', 'simon', '1957-11-22', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140182', 'anwld', 'ohrsew', '1958-05-08', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140173', 'lisa', 'khuen', '1965-10-15', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140165', 'mtoy', 'luectr', '1940-03-03', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140080', 'arpmd', 'dtorae', '1941-11-25', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9140008', 'ialwm', 'rlebto', '1957-11-20', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9139984', 'liwe', 'nikjes', '1959-03-13', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9139948', 'wialm', 'olcier', '1973-11-24', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9139928', 'ilnda', 'vtiech', '1944-05-26', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9139918', 'harosn', 'ewb', '1958-08-25', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9139764', 'emnaul', 'fesodnrl', '1970-07-24', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9138707', 'fedil', 'sjinoha', '1963-02-07', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9138696', 'nafrk', 'arohisn', '1938-08-13', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9138327', 'cerkiy', 'ray', '1976-08-15', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9138170', 'secjia', 'naudmjo', '1990-05-02', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9138165', 'ntihcya', 'reatg', '1948-09-17', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9138161', 'awnedy', 'ekarp', '1958-11-10', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9138140', 'tevsn', 'bonris', '1967-06-26', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9138138', 'kenhisa', 'rsobin', '1985-04-10', '2020-08-29'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9138134', 'tsve', 'repas', '1951-10-28', '2020-08-29'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9138133', 'nwyade', 'ordbgn', '1961-12-27', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9138132', 'jan', 'slma', '1960-12-09', '2020-08-29'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9138130', 'loirna', 'hnolwad', '1964-05-15', '2020-08-29'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9138129', 'nita', 'neurt', '1977-07-22', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9137999', 'mhtoas', 'daivs', '1952-03-09', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9137990', 'mije', 'keabr', '1951-12-21', '2020-08-30'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9137977', 'emajs', 'icolwx', '1934-12-28', '2020-08-29'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9137964', 'swnah', 'nbet', '1978-11-25', '2020-08-29'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9137957', 'swpiehr', 'owd', '1992-07-18', '2020-08-29'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9137490', 'nlida', 'zrole', '1950-03-12', '2020-08-29'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9137256', 'adlgous', 'raedmhn', '1946-06-15', '2020-08-29'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9137249', 'tios', 'lehtfcr', '1933-10-16', '2020-08-29'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9136965', 'iboe', 'thlcime', '1952-05-06', '2020-08-29'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9136859', 'rdoa', 'yla', '1949-01-25', '2020-08-29'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9136835', 'slinahry', 'acrney', '1963-07-20', '2020-08-29'], '', """"""),
        d.annotated2(['Uhc', 1, 347, '9136784', 'eajms', 'ofestr', '1935-04-06', '2020-08-29'], '', """"""),

    )
    def test_get_pcp_appt_list(self, values):
        episode_type, nsoc_type, acute_site_id, ext_visit_id, fname, lname, dob, admit_date = values

        test = date.today() - timedelta(days=1)
        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        #ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query_test(acute_site_id, ext_visit_id, episode_type, dob, fname, lname, admit_date, nsoc=1)

        #def create_episode_using_query_test(acute_site_id, ext_visit_id, episode_type, dob, fname, lname, admit_date, nsoc=1):

        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)