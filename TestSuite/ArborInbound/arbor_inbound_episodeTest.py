import unittest
import uuid
import config as c
from datetime import date, timedelta
from EndPoints import soundcare
from Utilities import target_process_util as th
#from Helpers import file_helper as f, ddt_helper as d, db_helper as db, utilities as u, episode_management_helper as e, verification_helper as v
from Helpers import ddt_helper as d, db_helper_new as db, utilities as u, episode_management_helper as e, verification_helper as v, file_helper as f
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
import json as j
from ddt import ddt, data
from decimal import Decimal


c.suite_name = 'Arbor Inbound Episode'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 107458
c.run_target_process = False

candidate_anchor_score_range = caar = 4.49   # candidate anchor range
confirmed_anchor_score_range = coar = 100.00 # confirmed anchor range
dismissed_anchor_range_range = diar = 4.48   # dismissed anchor range
candidate_subseq_score_range = casr = 12.00  # candidate subsequent range
confirmed_subseq_score_range = cosr = 100.00 # confirmed subsequent range
dismissed_subseq_score_range = disr = 11.99  # dismissed subsequent range

acute_site_id = 418  # Sandbox Hospital 3.3
token = soundcare.request_token_inbound_message(c.env)
header = {'Content-Type': "application/json", 'Authorization': 'Bearer ' + token}

@ddt
class ArborInboundEpisodeTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    def update_payload(self, anch_stay_id, current_stay_id, a_score, s_score):
        payload = f.get_payload_data('.\\..\\..\\PayloadData\\ArborInboundEpisode.txt')
        payload["id"] = str(uuid.uuid4())
        payload["patientIdentifier"][0]["value"] = str(current_stay_id)
        payload["anchorBundleProbability"]["bundleScore"] = a_score
        payload["anchorBundleProbability"]["bundleProbabilityDetail"][0]["anchorStayId"] = anch_stay_id
        payload["subsequentBundleProbability"]["bundleScore"] = s_score
        payload["subsequentBundleProbability"]["bundleProbabilityDetail"][0]["anchorStayId"] = anch_stay_id
        body = j.dumps(payload)

        return body

    def create_anchor_episode(self, current_stay_id, status):
        first_name = e.generate_random_name('Patient')
        admit_date = date.today() - timedelta(days=6)

        anch_p_visit_id, episode_id = db.create_episode_using_query_test(
            acute_site_id, current_stay_id, 'Bpcia', '1970-10-15', first_name, 'Arbor', admit_date)

        db.update_episode_status(episode_id, status)
        return anch_p_visit_id, episode_id

    def create_subsequent_episode(self, anchor_stay_id, subsequent_stay_id, status):
        first_name = e.generate_random_name('Patient')
        admit_date = date.today() - timedelta(days=6)

        anchor_p_visit_id, episode_id = db.create_episode_using_query_test(
            acute_site_id, anchor_stay_id, 'Bpcia', '1970-10-15', first_name, 'Arbor', admit_date)

        db.update_discharge_date(anchor_stay_id, date.today() - timedelta(days=2))

        subs_p_visit_id = db.insert_patient_visit(acute_site_id, subsequent_stay_id, first_name, 'Arbor', '1970-10-15', admit_date)
        db.insert_patient_discharge_plan(subs_p_visit_id)
        db.insert_episode_patient_visit(subs_p_visit_id, episode_id)
        db.update_episode_status(episode_id, status)

        return anchor_p_visit_id, subs_p_visit_id, episode_id

    def check_expectation(self, episode, status, a_score, s_score, anch_stay_id, current_stay_id, episode_id):
        result1 = db.get_episode(anch_stay_id)
        result2 = db.get_episode(current_stay_id)

        if episode == 'none' and a_score < 44 and s_score < 44:
            print('ignore the message')
            u.assert_and_log(result1 is None, 'arbor message should be ignored')
            u.assert_and_log(result2 is None, 'arbor message should be ignored')

        elif episode == 'none' and ((44 <= s_score < 100) or ((44 <= a_score < 100) and s_score < 44)):
            print('create candidate anchor episode')
            # print(result2[0][0].AnchorBundleScore)
            # print(result2[0][0].SubsequentBundleScore)
            u.assert_and_log(result2 is not None, 'Should create candidate anchor episode')
            u.assert_and_log(result2[0][0].EpisodeStatusTypeId == 1, 'The anchor episode status should be 1')
            # u.assert_and_log(result2[0][0].AnchorBundleScore == round(Decimal(a_score), 2), 'Save incorrect anchor score')
            # u.assert_and_log(result2[0][0].SubsequentBundleScore == round(Decimal(s_score), 2), 'Save incorrect subsequent score')

        elif episode == 'none'and a_score == 100 or s_score == 100:
            print('created confirmed anchor episode')

        elif episode == 'anch' and status in (1, 2, 5, 8) and a_score < 44 and s_score < 44:
            print('update to system dismissed')

        elif episode == 'anch' and status in (1, 2, 5, 8) and (44 <= a_score < 100) and s_score < 44:
            print('update to candidate')

        elif episode == 'anch' and status in (1, 2, 5, 8) and 44 <= s_score < 100:
            print('create candidate subsequent episode')

        elif episode == 'anch' and status in (1, 2, 5, 8) and s_score == 100:
            print('create confirmed subsequent episode')

        elif episode == 'subs' and status in (1, 2, 5, 8) and s_score < 44:
            print('update to system dismissed')

        elif episode == 'subs' and status in (1, 2, 5, 8) and 44 <= s_score < 100:
            print('update to candidate')

        elif episode == 'subs' and status in (1, 2, 5, 8) and s_score == 100:
            print('update to confirmed')

        elif episode == 'subs' and 'anch' and status == 7:
            print('keep the same completed status')

    @data(
        d.annotated2(['subs', 1, diar, disr, 'upd'], 'Arbor upd message will update current ep to system dismissed if - anchor/subseq score in dismissed range', """Candidate subsequent episode existed"""),
        d.annotated2(['subs', 1, diar, disr, 'new'], 'Arbor new message will update current ep to system dismissed if - anchor/subseq score in dismissed range', """Candidate subsequent episode existed"""),
        d.annotated2(['subs', 1, caar, casr, 'upd'], 'Arbor upd message will update current ep to candidate if both anchor/subseq scores in cand range', """Candidate subsequent episode existed"""),
        d.annotated2(['subs', 1, caar, casr, 'new'], 'Arbor new message will create candidate subsequent if both anchor/subseq scores in cand range', """Candidate subsequent episode existed"""),
        d.annotated2(['subs', 1, coar, cosr, 'upd'], 'Arbor upd message will update current ep to confirm if both anchor/subseq scores in conf range', """Candidate subsequent episode existed"""),
        d.annotated2(['subs', 1, coar, cosr, 'new'], 'Arbor new message will create confirmed subsequent ep if both anchor/subseq scores in conf range', """Candidate subsequent episode existed"""),
        d.annotated2(['subs', 1, caar, disr, 'upd'], 'Arbor upd message will update current ep to candidate if anchor score in cand range and subseq score in dism range', """Candidate subsequent episode existed"""),
        d.annotated2(['subs', 1, caar, disr, 'new'], 'Arbor new message will update current ep to candidate if anchor score in cand range and subseq score in dism range', """Candidate subsequent episode existed"""),
        d.annotated2(['subs', 1, diar, casr, 'upd'], 'Arbor upd message will update current ep to candidate if anchor score in dism range and subseq score in cand range', """Candidate subsequent episode existed"""),
        d.annotated2(['subs', 1, diar, casr, 'new'], 'Arbor new message will create candidate subsequent ep if anchor score in dism range and subseq score in cand range', """Candidate subsequent episode existed"""),
        d.annotated2(['subs', 1, coar, casr, 'upd'], 'Arbor upd message will update current ep to candidate if anchor score in conf range and subseq score in cand range', """Candidate subsequent episode existed"""),
        d.annotated2(['subs', 1, coar, casr, 'new'], 'Arbor new message will create candidate subsequent ep if anchor score in conf range and subseq score in cand range', """Candidate subsequent episode existed"""),
        d.annotated2(['subs', 1, caar, cosr, 'upd'], 'Arbor upd message will update current ep to confirmed if anchor score in cand range and subseq score in conf range', """Candidate subsequent episode existed"""),
        d.annotated2(['subs', 1, caar, cosr, 'new'], 'Arbor new message will create confirmed subsequent ep if anchor score in cand range and subseq score in conf range', """Candidate subsequent episode existed"""),
        d.annotated2(['subs', 2, diar, disr, 'upd'], 'Arbor upd message will update current ep to system dismissed if - anchor/subseq scores in dismissed range', """Confirmed subsequent episode existed"""),
        d.annotated2(['subs', 2, diar, disr, 'new'], 'Arbor new message will be ignored if - anchor/subseq scores in dismissed range', """Confirmed subsequent episode existed"""),
        d.annotated2(['subs', 2, caar, casr, 'upd'], 'Arbor upd message will update current ep to candidate if both anchor/subseq scores in cand range', """Confirmed subsequent episode existed"""),
        d.annotated2(['subs', 2, caar, casr, 'new'], 'Arbor new message will create candidate subsequent if both anchor/subseq scores in cand range', """Confirmed subsequent episode existed"""),
        d.annotated2(['subs', 2, coar, cosr, 'upd'], 'Arbor upd message will update current ep to confirm if both anchor/subseq scores in conf range', """Confirmed subsequent episode existed"""),
        d.annotated2(['subs', 2, coar, cosr, 'new'], 'Arbor new message will create confirmed subsequent ep if both anchor/subseq scores in conf range', """Confirmed subsequent episode existed"""),
        d.annotated2(['subs', 2, caar, disr, 'upd'], 'Arbor upd message will update current ep to candidate if anchor score in cand range and subseq score in dism range', """Confirmed subsequent episode existed"""),
        d.annotated2(['subs', 2, caar, disr, 'new'], 'Arbor new message will update current ep to candidate if anchor score in cand range and subseq score in dism range', """Confirmed subsequent episode existed"""),
        d.annotated2(['subs', 2, diar, casr, 'upd'], 'Arbor upd message will update current ep to candidate if anchor score in dism range and subseq score in cand range', """Confirmed subsequent episode existed"""),
        d.annotated2(['subs', 2, diar, casr, 'new'], 'Arbor new message will create candidate subsequent ep if anchor score in dism range and subseq score in cand range', """Confirmed subsequent episode existed"""),
        d.annotated2(['subs', 2, coar, casr, 'upd'], 'Arbor upd message will update current ep to candidate if anchor score in conf range and subseq score in cand range', """Confirmed subsequent episode existed"""),
        d.annotated2(['subs', 2, coar, casr, 'new'], 'Arbor new message will create candidate subsequent ep if anchor score in conf range and subseq score in cand range', """Confirmed subsequent episode existed"""),
        d.annotated2(['subs', 2, caar, cosr, 'upd'], 'Arbor upd message will update current ep to confirmed if anchor score in cand range and subseq score in conf range', """Confirmed subsequent episode existed"""),
        d.annotated2(['subs', 2, caar, cosr, 'new'], 'Arbor new message will create confirmed subsequent ep if anchor score in cand range and subseq score in conf range', """Confirmed subsequent episode existed"""),
        d.annotated2(['subs', 5, diar, disr, 'upd'], 'Arbor upd message will update current ep to system dismissed if - anchor/subseq scores in dismissed range', """Dismissed subsequent episode existed"""),
        d.annotated2(['subs', 5, diar, disr, 'new'], 'Arbor new message will be ignore if - anchor/subseq scores in dismissed range', """Dismissed subsequent episode existed"""),
        d.annotated2(['subs', 5, caar, casr, 'upd'], 'Arbor upd message will update current ep to candidate if both anchor/subseq scores in cand range', """Dismissed subsequent episode existed"""),
        d.annotated2(['subs', 5, caar, casr, 'new'], 'Arbor new message will create candidate subsequent if both anchor/subseq scores in cand range', """Dismissed subsequent episode existed"""),
        d.annotated2(['subs', 5, coar, cosr, 'upd'], 'Arbor upd message will update current ep to confirm if both anchor/subseq scores in conf range', """Dismissed subsequent episode existed"""),
        d.annotated2(['subs', 5, coar, cosr, 'new'], 'Arbor new message will create confirmed subsequent ep if both anchor/subseq scores in conf range', """Dismissed subsequent episode existed"""),
        d.annotated2(['subs', 5, caar, disr, 'upd'], 'Arbor upd message will update current ep to candidate if anchor score in cand range and subseq score in dism range', """Dismissed subsequent episode existed"""),
        d.annotated2(['subs', 5, caar, disr, 'new'], 'Arbor new message will update current ep to candidate if anchor score in cand range and subseq score in dism range', """Dismissed subsequent episode existed"""),
        d.annotated2(['subs', 5, diar, casr, 'upd'], 'Arbor upd message will update current ep to candidate if anchor score in dism range and subseq score in cand range', """Dismissed subsequent episode existed"""),
        d.annotated2(['subs', 5, diar, casr, 'new'], 'Arbor new message will create candidate subsequent ep if anchor score in dism range and subseq score in cand range', """Dismissed subsequent episode existed"""),
        d.annotated2(['subs', 5, coar, casr, 'upd'], 'Arbor upd message will update current ep to candidate if anchor score in conf range and subseq score in cand range', """Dismissed subsequent episode existed"""),
        d.annotated2(['subs', 5, coar, casr, 'new'], 'Arbor new message will create candidate subsequent ep if anchor score in conf range and subseq score in cand range', """Dismissed subsequent episode existed"""),
        d.annotated2(['subs', 5, caar, cosr, 'upd'], 'Arbor upd message will update current ep to confirmed if anchor score in cand range and subseq score in conf range', """Dismissed subsequent episode existed"""),
        d.annotated2(['subs', 5, caar, cosr, 'new'], 'Arbor new message will create confirmed subsequent ep if anchor score in cand range and subseq score in conf range', """Dismissed subsequent episode existed"""),
        #d.annotated2(['subs', 7, diar, disr, 'upd'], 'Arbor upd message will be ignored if - anchor/subseq scores in dismissed range', """Complete subsequent episode existed"""),
        #d.annotated2(['subs', 7, diar, disr, 'new'], 'Arbor new message will be ignored if - anchor/subseq scores in dismissed range', """Complete subsequent episode existed"""),
        #d.annotated2(['subs', 7, caar, casr, 'upd'], 'Arbor upd message will be ignored if both anchor/subseq scores in cand range', """Complete subsequent episode existed"""),
        #d.annotated2(['subs', 7, caar, casr, 'new'], 'Arbor new message will be ignored if both anchor/subseq scores in cand range', """Complete subsequent episode existed"""),
        #d.annotated2(['subs', 7, coar, cosr, 'upd'], 'Arbor upd message will be ignored if both anchor/subseq scores in conf range', """Complete subsequent episode existed"""),
        #d.annotated2(['subs', 7, coar, cosr, 'new'], 'Arbor new message will be ignored if both anchor/subseq scores in conf range', """Complete subsequent episode existed"""),
        #d.annotated2(['subs', 7, caar, disr, 'upd'], 'Arbor upd message will be ignored if anchor score in cand range and subseq score in dism range', """Complete subsequent episode existed"""),
        #d.annotated2(['subs', 7, caar, disr, 'new'], 'Arbor new message will be ignored if anchor score in cand range and subseq score in dism range', """Complete subsequent episode existed"""),
        #d.annotated2(['subs', 7, diar, casr, 'upd'], 'Arbor upd message will be ignored if anchor score in dism range and subseq score in cand range', """Complete subsequent episode existed"""),
        #d.annotated2(['subs', 7, diar, casr, 'new'], 'Arbor new message will be ignored if anchor score in dism range and subseq score in cand range', """Complete subsequent episode existed"""),
        #d.annotated2(['subs', 7, coar, casr, 'upd'], 'Arbor upd message will be ignored if anchor score in conf range and subseq score in cand range', """Complete subsequent episode existed"""),
        #d.annotated2(['subs', 7, coar, casr, 'new'], 'Arbor new message will be ignored if anchor score in conf range and subseq score in cand range', """Complete subsequent episode existed"""),
        #d.annotated2(['subs', 7, caar, cosr, 'upd'], 'Arbor upd message will be ignored if anchor score in cand range and subseq score in conf range', """Complete subsequent episode existed"""),
        #d.annotated2(['subs', 7, caar, cosr, 'new'], 'Arbor new message will be ignored if anchor score in cand range and subseq score in conf range', """Complete subsequent episode existed"""),
        d.annotated2(['subs', 8, diar, disr, 'upd'], 'Arbor upd message will update current ep to system dismissed if - anchor/subseq score in dismissed range', """System dismissed subsequent episode existed"""),
        d.annotated2(['subs', 8, diar, disr, 'new'], 'Arbor new message will be ignore if - anchor/subseq scores in dismissed range', """System dismissed subsequent episode existed"""),
        d.annotated2(['subs', 8, caar, casr, 'upd'], 'Arbor upd message will update current ep to candidate if both anchor/subseq scores in cand range', """System dismissed subsequent episode existed"""),
        d.annotated2(['subs', 8, caar, casr, 'new'], 'Arbor new message will create candidate subsequent if both anchor/subseq scores in cand range', """System dismissed subsequent episode existed"""),
        d.annotated2(['subs', 8, coar, cosr, 'upd'], 'Arbor upd message will update current ep to confirm if both anchor/subseq scores in conf range', """System dismissed subsequent episode existed"""),
        d.annotated2(['subs', 8, coar, cosr, 'new'], 'Arbor new message will create confirmed subsequent ep if both anchor/subseq scores in conf range', """System dismissed subsequent episode existed"""),
        d.annotated2(['subs', 8, caar, disr, 'upd'], 'Arbor upd message will update current ep to candidate if anchor score in cand range and subseq score in dism range', """System dismissed subsequent episode existed"""),
        d.annotated2(['subs', 8, caar, disr, 'new'], 'Arbor new message will update current ep to candidate if anchor score in cand range and subseq score in dism range', """System dismissed subsequent episode existed"""),
        d.annotated2(['subs', 8, diar, casr, 'upd'], 'Arbor upd message will update current ep to candidate if anchor score in dism range and subseq score in cand range', """System dismissed subsequent episode existed"""),
        d.annotated2(['subs', 8, diar, casr, 'new'], 'Arbor new message will create candidate subsequent ep if anchor score in dism range and subseq score in cand range', """System dismissed subsequent episode existed"""),
        d.annotated2(['subs', 8, coar, casr, 'upd'], 'Arbor upd message will update current ep to candidate if anchor score in conf range and subseq score in cand range', """System dismissed subsequent episode existed"""),
        d.annotated2(['subs', 8, coar, casr, 'new'], 'Arbor new message will create candidate subsequent ep if anchor score in conf range and subseq score in cand range', """System dismissed subsequent episode existed"""),
        d.annotated2(['subs', 8, caar, cosr, 'upd'], 'Arbor upd message will update current ep to confirmed if anchor score in cand range and subseq score in conf range', """System dismissed subsequent episode existed"""),
        d.annotated2(['subs', 8, caar, cosr, 'new'], 'Arbor new message will create confirmed subsequent ep if anchor score in cand range and subseq score in conf range', """System dismissed subsequent episode existed"""),
    )
    def test_arbor_inbound_existing_subsequent_episode(self, values):
        existing_ep, status, anchor_score, subseq_score, mssg_type = values
        c.test_case_name = 'Verify that when ' + getattr(values, "__doc__") + ', ' + getattr(values, "__name__")
        print(c.test_case_name)

        body = None
        anchor_stay_id = e.generate_visit_number()
        subseq_stay_id = e.generate_visit_number()
        current_stay_id = e.generate_visit_number()

        a_p_visit_id, s_p_visit_id, episode_id = self.create_subsequent_episode(anchor_stay_id, subseq_stay_id, status)

        print('Anchor stay id: ' + str(anchor_stay_id))
        print('subseq stay id: ' + str(subseq_stay_id))
        print('Current stay id: ' + str(current_stay_id))
        print('Anchor Patient Visit id: ' + str(a_p_visit_id))
        print('Subseq Patient Visit id: ' + str(s_p_visit_id))

        if mssg_type == 'upd':
            body = self.update_payload(subseq_stay_id, subseq_stay_id, anchor_score, subseq_score)

        if mssg_type == 'new':
            p_visit_id = db.insert_patient_visit(acute_site_id, current_stay_id, e.generate_random_name('Patient'), 'Arbor', '1970-10-15', date.today() - timedelta(days=6))
            body = self.update_payload(subseq_stay_id, current_stay_id, anchor_score, subseq_score)

        print(body)
        resp = soundcare.arbor_inbound_episode(body, c.env, header)

        u.assert_and_log(resp.status_code == 202, 'Failed to send message - Actual code: ' + str(resp.status_code))
        #s_result = db.get_episode(current_stay_id)
        a_result = db.get_episode(subseq_stay_id, 10)

        if (anchor_score < caar and subseq_score < caar) and mssg_type == 'new':
            print('Anchor status: ' + str(a_result[0][0].EpisodeStatusTypeId))
            # print('Anchor score: ' + str(a_result[0][0].AnchorBundleScore))
            # print('Subsequent score: ' + str(a_result[0][0].SubsequentBundleScore))
            u.assert_and_log(a_result[0][0].EpisodeStatusTypeId == status, 'Arbor message should be ignored')

        elif (anchor_score < caar and subseq_score < caar) and mssg_type == 'upd':
            print('Anchor status: ' + str(a_result[0][0].EpisodeStatusTypeId))
            # print('Anchor score: ' + str(a_result[0][0].AnchorBundleScore))
            # print('Subsequent score: ' + str(a_result[0][0].SubsequentBundleScore))
            u.assert_and_log(a_result[0][0].EpisodeStatusTypeId == 8, 'Status should be candidate, actual: ' + str(a_result[0][0].EpisodeStatusTypeId))
            # u.assert_and_log(a_result[0][0].AnchorBundleScore == round(Decimal(anchor_score), 2), 'Incorrect anchor bundle score, Actual: ' + str(a_result[0][0].AnchorBundleScore))
            # u.assert_and_log(a_result[0][0].SubsequentBundleScore == round(Decimal(subseq_score), 2), 'Incorrect subsequenr bundle score: ' + str(a_result[0][0].SubsequentBundleScore))

        elif caar < anchor_score < coar and subseq_score < casr:
            print('Anchor status: ' + str(a_result[0][0].EpisodeStatusTypeId))
            # print('Anchor score: ' + str(a_result[0][0].AnchorBundleScore))
            # print('Subsequent score: ' + str(a_result[0][0].SubsequentBundleScore))
            u.assert_and_log(a_result[0][0].EpisodeStatusTypeId == 1, 'Status should be candidate, actual: ' + str(a_result[0][0].EpisodeStatusTypeId))
            # u.assert_and_log(a_result[0][0].AnchorBundleScore == round(Decimal(anchor_score), 2), 'Incorrect anchor bundle score, Actua: ' + str(a_result[0][0].AnchorBundleScore))
            # u.assert_and_log(a_result[0][0].SubsequentBundleScore == round(Decimal(subseq_score), 2), 'Incorrect subsequenr bundle score: ' + str(a_result[0][0].SubsequentBundleScore))

        elif casr <= subseq_score < cosr and mssg_type == 'new':
            s_result = db.get_episode(current_stay_id, 20)
            print('Subsequent status: ' + str(s_result[0][0].EpisodeStatusTypeId))
            # print('Anchor score: ' + str(s_result[0][0].AnchorBundleScore))
            # print('Subsequent score: ' + str(s_result[0][0].SubsequentBundleScore))
            u.assert_and_log(s_result[0][0].EpisodeId == episode_id, 'Episode Id should be matched: ' + str(s_result[0][0].EpisodeStatusTypeId))
            u.assert_and_log(s_result[0][0].EpisodeStatusTypeId == 1, 'Status should be system candidate, actual: ' + str(s_result[0][0].EpisodeStatusTypeId))
            # u.assert_and_log(s_result[0][0].AnchorBundleScore == round(Decimal(anchor_score), 2), 'Incorrect anchor bundle score, Actua: ' + str(s_result[0][0].AnchorBundleScore))
            # u.assert_and_log(s_result[0][0].SubsequentBundleScore == round(Decimal(subseq_score), 2), 'Incorrect subsequenr bundle score: ' + str(s_result[0][0].SubsequentBundleScore))

        elif casr <= subseq_score < cosr and mssg_type == 'upd':
            print('Anchor status: ' + str(a_result[0][0].EpisodeStatusTypeId))
            # print('Anchor score: ' + str(a_result[0][0].AnchorBundleScore))
            # print('Subsequent score: ' + str(a_result[0][0].SubsequentBundleScore))
            u.assert_and_log(a_result[0][0].EpisodeStatusTypeId == 1, 'Status should be system candidate, actual: ' + str(a_result[0][0].EpisodeStatusTypeId))
            # u.assert_and_log(a_result[0][0].AnchorBundleScore == round(Decimal(anchor_score), 2), 'Incorrect anchor bundle score, Actua: ' + str(a_result[0][0].AnchorBundleScore))
            # u.assert_and_log(a_result[0][0].SubsequentBundleScore == round(Decimal(subseq_score), 2), 'Incorrect subsequenr bundle score: ' + str(a_result[0][0].SubsequentBundleScore))

        elif subseq_score == cosr and mssg_type == 'new':
            s_result = db.get_episode(current_stay_id, 20)
            print('Subsequent status: ' + str(s_result[0][0].EpisodeStatusTypeId))
            # print('Anchor score: ' + str(s_result[0][0].AnchorBundleScore))
            # print('Subsequent score: ' + str(s_result[0][0].SubsequentBundleScore))
            u.assert_and_log(s_result[0][0].EpisodeId == episode_id, 'Episode Id should be matched: ' + str(s_result[0][0].EpisodeStatusTypeId))
            u.assert_and_log(s_result[0][0].EpisodeStatusTypeId == 2, 'Status should be system candidate, actual: ' + str(s_result[0][0].EpisodeStatusTypeId))
            # u.assert_and_log(s_result[0][0].AnchorBundleScore == round(Decimal(anchor_score), 2), 'Incorrect anchor bundle score, Actua: ' + str(s_result[0][0].AnchorBundleScore))
            # u.assert_and_log(s_result[0][0].SubsequentBundleScore == round(Decimal(subseq_score), 2), 'Incorrect subsequenr bundle score: ' + str(s_result[0][0].SubsequentBundleScore))

        elif subseq_score == cosr and mssg_type == 'upd':
            print('Anchor status: ' + str(a_result[0][0].EpisodeStatusTypeId))
            # print('Anchor score: ' + str(a_result[0][0].AnchorBundleScore))
            # print('Subsequent score: ' + str(a_result[0][0].SubsequentBundleScore))
            u.assert_and_log(a_result[0][0].EpisodeStatusTypeId == 2, 'Status should be system candidate, actual: ' + str(a_result[0][0].EpisodeStatusTypeId))
            # u.assert_and_log(a_result[0][0].AnchorBundleScore == round(Decimal(anchor_score), 2), 'Incorrect anchor bundle score, Actua: ' + str(a_result[0][0].AnchorBundleScore))
            # u.assert_and_log(a_result[0][0].SubsequentBundleScore == round(Decimal(subseq_score), 2), 'Incorrect subsequenr bundle score: ' + str(a_result[0][0].SubsequentBundleScore))

        #db.delete_episode_records(anchor_stay_id, episode_id)
        #db.delete_episode_records(current_stay_id, episode_id)
        #db.delete_episode_records(subseq_stay_id, episode_id)
    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------


if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

