import unittest
import config as c
from EndPoints import soundcare
from Utilities import target_process_util as th
#from Helpers import file_helper as f, ddt_helper as d, db_helper as db, utilities as u, episode_management_helper as e, verification_helper as v
from Helpers import ddt_helper as d, db_helper_new as db, utilities as u, file_helper as f, episode_management_helper as e
from teamcity import is_running_under_teamcity
from teamcity.unittestpy import TeamcityTestRunner
import json as j
from ddt import ddt, data


c.suite_name = 'Readmission Risk Score'
c.tp_tag = 'SoundCare-API'
c.test_plan_run_id = 107458
c.run_target_process = True


@ddt
class ArborReadmissionRisk(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass')
        if c.run_target_process:
            c.test_cases, c.child_test_plan_run_id = th.initialize_target_process(
                c.tp_tag, c.test_plan_run_id, c.suite_name, c.run_target_process)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass')

    def setUp(self):
        print('setUpTest')
        c.errors = ''

    def tearDown(self):
        print('tearDownTest')
        if c.run_target_process:
            # Remove carriage returns from error list as this causes the error text to be truncated at the first carriage return when updating TP
            error = str(c.errors).replace('\n', '-')
            th.update_target_process(c.test_cases, c.test_case_name, c.child_test_plan_run_id, str(error))

    # -----------------------------------------------------------------------------------------------------------------
    # -----------------------------------------------------------------------------------------------------------------
    @data(
        d.annotated2([0.09, 0 , 'guid', 'yes' ], 'Verify any risk including below 12 percent will be persisted if existing risk score is NULL or below 12 percent', """"""),
        d.annotated2([0.12, 11, 'guid', 'yes' ], 'Verify risk score equal or above 12 percent will be persisted', """"""),
        d.annotated2([0.11, 12, 'guid', 'yes' ], 'Verify risk score below 12 percent will be ignored  if existing risk score is 12 percent or above', """"""),
        #d.annotated2([0.11, 12, 'null', 'yes' ], 'Verify the API return appropriated error if Id GUI is NULL', """"""),
        #d.annotated2([0.12, 12, 'empt', 'yes' ], 'Verify the API return appropriated error if Id GUI is empty', """"""),
        #d.annotated2([0.12, 12, 'guid', 'null'], 'Verify the API return appropriated error if stay id is NULL', """"""),
        #d.annotated2([0.12, 12, 'guid', 'empt'], 'Verify the API return appropriated error if stay id is epmty', """"""),
        #d.annotated2([0.12, None, 'guid', 'null'], 'Verify the API return appropriated error if score is NULL', """"""),
        #d.annotated2([0.12, '', 'guid', 'empt'], 'Verify the API return appropriated error if score is epmty', """"""),
    )
    def test_arbor_readmission_risk_score(self, values):
        score, existing_score, guid, stay_id = values
        acute_site_id = 418

        c.test_case_name = getattr(values, "__name__")
        print(c.test_case_name)

        ext_visit_id = e.generate_visit_number()
        p_visit_id, episode_id = db.create_episode_using_query(acute_site_id, str(ext_visit_id), 'Bpcia')
        db.update_readmission_risk_score(p_visit_id, existing_score)

        print(ext_visit_id)
        print(p_visit_id)
        print(episode_id)

        payload = f.get_payload_data('.\\..\\..\\PayloadData\\RiskScorePayload_new.txt')
        payload["subject"]["identifier"][1]["value"] = ext_visit_id
        payload["group"][0]["measureScore"]["value"] = score

        body = j.dumps(payload)

        resp = soundcare.arbor_readmission_risk(body, c.env)
        u.assert_and_log(resp.status_code == 200, 'Failed to send message - Actual code: ' + str(resp.status_code))

        db_res = db.get_readmission_score(p_visit_id, 20)

        if (existing_score >= 12) and (score < 12):
            u.assert_and_log(existing_score == db_res[0][0].ReadmissionRiskScore,
                             'Readmission risk score should be ' + str(existing_score) +
                             ' Actual score: ' + str(db_res[0][0].ReadmissionRiskScore))

        else:
            u.assert_and_log(score * 100 == db_res[0][0].ReadmissionRiskScore,
                             'Readmission risk score should be ' + str(score) +
                             ' Actual score: ' + str(db_res[0][0].ReadmissionRiskScore))


# -----------------------------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        unittest.TestLoader.sortTestMethodsUsing = None
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)

